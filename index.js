/** @format */

import { AppRegistry} from 'react-native'
import {name as appName} from './app.json';
import MainApp from './App/Navigation/RootNav'
import './shim.js'
import PushNotification from './App/Libs/PushNotification'
import NotificationWorker from './App/Workers/NotificationWorker'


PushNotification.setup();
AppRegistry.registerComponent(appName, () => MainApp);
// New task registration
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => NotificationWorker);
