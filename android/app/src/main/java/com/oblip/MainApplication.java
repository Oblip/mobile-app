package com.oblip;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;

// import com.reactnativecommunity.geolocation.GeolocationPackage;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import com.horcrux.svg.SvgPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.microsoft.appcenter.reactnative.crashes.AppCenterReactNativeCrashesPackage;
import com.microsoft.appcenter.reactnative.analytics.AppCenterReactNativeAnalyticsPackage;
import com.microsoft.appcenter.reactnative.appcenter.AppCenterReactNativePackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.microsoft.codepush.react.CodePush;
import org.reactnative.camera.RNCameraPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.imagepicker.ImagePickerPackage;
import com.rnfingerprint.FingerprintAuthPackage;
import com.reactnativecomponent.splashscreen.RCTSplashScreenPackage;
import com.tradle.react.UdpSocketsModule;
import com.peel.react.TcpSocketsModule;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.oblador.shimmer.RNShimmerPackage;
import com.reactlibrary.securekeystore.RNSecureKeyStorePackage;
import com.bitgo.randombytes.RandomBytesPackage;
import com.peel.react.rnos.RNOSModule;
// import com.bhavan.RNNavBarColor.RNNavBarColor;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import com.rnfs.RNFSPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.projectseptember.RNGL.RNGLPackage;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.ianlin.RNCarrierInfo.RNCarrierInfoPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
        return CodePush.getJSBundleFile();
        }
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new AsyncStoragePackage(),
            // new GeolocationPackage(),
            new ReactNativeRestartPackage(),
            new RNGLPackage(),
            new AppCenterReactNativeCrashesPackage(MainApplication.this, getResources().getString(R.string.appCenterCrashes_whenToSendCrashes)),
            new AppCenterReactNativeAnalyticsPackage(MainApplication.this, getResources().getString(R.string.appCenterAnalytics_whenToEnableAnalytics)),
            new AppCenterReactNativePackage(MainApplication.this),
            new MapsPackage(),
            new LottiePackage(),
            new CodePush(BuildConfig.CODEPUSH_KEY, MainApplication.this, BuildConfig.DEBUG),
            new RNCameraPackage(),
            new RNFirebasePackage(),
            new RNFusedLocationPackage(),
            new ImagePickerPackage(),
            new FingerprintAuthPackage(),
            new RCTSplashScreenPackage(),
            new UdpSocketsModule(),
            new TcpSocketsModule(),
            new SvgPackage(),
            new RNShimmerPackage(),
            new RNSecureKeyStorePackage(),
            new RandomBytesPackage(),
            new RNOSModule(),
            // new RNNavBarColor(),
            new ImageResizerPackage(),
            new RNFSPackage(),
            new RNDeviceInfo(),
            new ReactNativeConfigPackage(),
            new RNCarrierInfoPackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage()
            
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
