# Changelog

## Version 0.1.0

*June 28, 2019*

- New Design
- Simple and fast on-boarding
- New QR code design
- Improved performance
