#!/usr/bin/env bash -e

# Add changelog
# git add CHANGELOG.md

git fetch --tags

# create temp branch
git checkout -b release/temp_$(git rev-parse --short HEAD)