# Oblip React Native App

This is the main repository for the android and ios app for Oblip

## Install

1. Clone this project
2. run the command `npm install`.

#### Environments

There are three known environments [development, staging, production].

Each will require a .env.[env_name] file. See .env.development.example file to see what are the requirements parameters.


## How to Run it

#### Android 

Development

```
$ npm run android-development
```

Staging

```
$ npm run android-staging
```


#### iOS

Development (Device)

```
$ npm run ios-development-device
```


## Updating Version

To update the version, create a tag and upload a release to gitlab, run the following format command: `npm version major|minor|patch`.

You can add a message by using the `-m "Your message"` parameter eg:

`npm version patch -m "Fixed navigation bug"`