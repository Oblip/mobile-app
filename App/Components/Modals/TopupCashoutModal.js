import React, { Component } from 'react';
import {  
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Platform,
  Vibration,
} from 'react-native'
// import Text from '../globalTextStyle';
import Modal from 'react-native-modalbox';
import QRCode from 'react-native-qrcode-svg';
import CloseIconButton from '../CloseIconButton'
import Text from '../globalTextStyle'
import cryptoRandomString from 'crypto-random-string'
import SecureDataStorage from '../../Libs/SecureDataStorage'
import FayeService from '../../Services/FayeService'

export default class TopupCashoutModal extends Component {  
  identifier;

  constructor (props) {
    super(props)
    this.state = {
      isOpen: this.props.isOpen,
      type: this.props.type || 'topup',
      amount: null,
      modalTitle: '',
      asset: 'BZD',
      showEnterAmountView: true,
      showQrCodeView: false,
      showCompleteView: false,
      currentAccount: null,
      scannedUsername: this.props.scannedUsername
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen,
      type: nextProps.type || 'topup',
      scannedUsername: nextProps.scannedUsername
    });
  }

  componentWillMount () {
    this.identifier = cryptoRandomString({length: 32});
    this.identifier = this.identifier.replace(/\(.+?\)/g, '-');

    SecureDataStorage.getAuthAccount()
    .then(authAccount => {
      this.setState({currentAccount: authAccount.account})
    }).catch(error => {
      // TODO: handle
    })

    let channel = `/${this.identifier}`;
    try {
      FayeService.getClient().subscribe(channel, (msgData) => {
        if(!msgData.http_status) {
  
          Vibration.vibrate(500);
  
          this.setState({
            showQrCodeView: false,
            showCompleteView: true,
            amount: msgData.amount
          });
  
          FayeService.getClient().unsubscribe(`/${this.identifier}`)
        } 
      });
    }

    catch (e) {
      // TODO: do something?
    }
   
  }

  componentWillUnmount () {
    try {
      FayeService.getClient().unsubscribe(`/${this.identifier}`)
    } catch(e) {
      // TODO: do something?
    }
  }

  onGenerateQrButtonTap = () => {
    this.setState({
      showEnterAmountView: false,
      showQrCodeView: true
    })
  }

  getDataType = (type) => {
    switch(type) {
      case 'topup':
        return 'partner_topup'
      case 'cashout':
        return 'partner_cashout'
    }
  }

  generateUrl = () => {
    let mainUrl = 'https://oblip.me/';
    
    const type = this.getDataType(this.state.type);
    const amount = this.state.amount;
    const asset = this.state.asset;
    const account = this.state.currentAccount
    const username = account.username;
    

    let isPartner = false;

    if(account.type == 'business' && account.is_agent) {
      isPartner = true;
    }

    let url = `${mainUrl}${username}?action=request_review`
    url = `${url}&partner=${isPartner}&amount=${amount}&asset=${asset}`
    url = `${url}&type=${type}&identifier=${this.identifier}`

    return url;
  }

  renderHeader = (title, subtitle, showCloseBtn = true) => {
    return(
      <View>
       {showCloseBtn && <CloseIconButton
          iconStyle={{tintColor: 'black'}}
          style={{position: 'absolute', right: -25, zIndex: 50}}
          onPressed={() => { this.props.onClosed() }} /> }
        
        <View style={styles.header}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}>{subtitle}</Text>
        </View>
      </View>
    )
  }

  renderEnterAmountView = (data) => {
    let title = data.title;
    let subtitle = data.subtitle;

    return(
      <View style={styles.container}>
        <View style={styles.content}>
          { this.renderHeader(title, subtitle) }
          <View style={styles.centerBody}>
            <View style={styles.fieldContainer}>
              <Text style={styles.inputLabel}>Amount</Text>
              <View style={styles.customInputContainer}>
                <TextInput
                    style={styles.input}
                    autoCorrect={false}
                    autoFocus={true}
                    keyboardType={'numeric'}
                    onChangeText={(text) => this.setState({amount: text})}
                    underlineColorAndroid='transparent'
                    value={this.state.amount}
                  />
              </View>    
            </View>
            {/* Todo: Work on this for cashout */}
            {this.state.type == 'cashout' && <View style={styles.reviewContainer}>
              <View style={styles.reviewKeypairContainer}>
                <Text style={styles.reviewKeypairKey}>Amount entered</Text>
                <Text style={styles.reviewKeypairValue}>{this.state.amount || '0'} BZD</Text>
              </View>
              <View style={styles.reviewKeypairContainer}>
                <Text style={styles.reviewKeypairKey}>You'll recieve (in cash)</Text>
                <Text style={styles.reviewKeypairValue}>99 BZD</Text>
              </View>
            </View>}
          </View>
        </View>
        <View style={styles.buttonContainer}> 
          <TouchableOpacity
            onPress={() => this.onGenerateQrButtonTap()}
            style={[styles.generalButton, styles.generalButtonPrimary]}>
            <Text style={[styles.generalButtonTitle]}>Generate QR Code</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderQRCodeView = (data) => {
    let title = data.title;
    let subtitle = data.subtitle;

    return(
      <View style={styles.container}>
        <View style={styles.content}>
          { this.renderHeader(title, subtitle) }
          <View style={styles.qrCodeContainer}>
            <View style={styles.qrCode}>
              <QRCode
                  value={this.generateUrl()}
                  logo={require('../../Assets/Images/qr_code_logo_circle.png')}
                  logoSize={30}
                  logoBackgroundColor='transparent'
                  size={130}
                />
            </View>
          </View>
          {this.state.type == 'cashout' && <View style={[styles.reviewContainer, { paddingTop: 10 } ]}>
              <View style={styles.reviewKeypairContainer}>
                <Text style={styles.reviewKeypairKey}>Amount entered</Text>
                <Text style={styles.reviewKeypairValue}>{this.state.amount || '0'} BZD</Text>
              </View>
              <View style={styles.reviewKeypairContainer}>
                <Text style={styles.reviewKeypairKey}>You'll recieve (in cash)</Text>
                <Text style={styles.reviewKeypairValue}>99 BZD</Text>
              </View>
            </View>}
          {this.state.type == 'topup' && <View style={styles.amountContainer}>
            <Text style={styles.amountTitle}>Amount</Text>
            <Text style={styles.amountText}>{this.state.amount || '0'} BZD</Text>
          </View>}
        </View>
      </View>
    )
  }

  renderCompleteView = (data) => {
    let title = data.title;
    let subtitle = data.subtitle;

    let actionText = this.state.type == 'topup' ? 'You recieved' : 'You sent'
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          { this.renderHeader(title, subtitle, false) }
          <View style={styles.successContainer}>
            <Image
                source={require('../../Assets/Images/success_large_icon.png')}
                resizeMode={'cover'}
                style={styles.successIcon}
              />
          </View>
          <View style={styles.amountContainer}>
            <Text style={styles.amountTitle}>{actionText}</Text>
            <Text style={styles.amountText}>{ this.state.amount || 0 } BZD</Text>
          </View>
        </View>
        <View style={styles.buttonContainer}> 
          <TouchableOpacity
            onPress={() => {
              FayeService.getClient().unsubscribe(`/${this.identifier}`)
              this.props.onClosed();
            }}
            style={[styles.generalButton, styles.generalButtonPrimary]}>
            <Text style={[styles.generalButtonTitle]}>Done</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderViews = (data) => {
    if(this.state.showEnterAmountView) {
      return this.renderEnterAmountView (data.amountView)
    } else if(this.state.showQrCodeView) {
      return this.renderQRCodeView(data.qrView)
    } else if (this.state.showCompleteView) {
      return this.renderCompleteView(data.completeView)
    }
  }

  renderTopupViews = () => {
    data = {
      amountView: {
        title: 'Topup Request',
        subtitle: `Enter the amount you would like to topup at @${this.state.scannedUsername}.`
      },
      qrView: {
        title: 'Topup Request',
        subtitle: `Show this QR code to @${this.state.scannedUsername} to finish the transaction.`
      },
      completeView: {
        title: 'Topup Completed',
        subtitle: `Your topup order with @${this.state.scannedUsername} was successfully completed.`
      }
    }

    return this.renderViews(data)
  }

  renderCashoutViews = () => {
    data = {
      amountView: {
        title: 'Cash Out',
        subtitle: 'Enter the amount you would like to cash-out at this agent.'
      },
      qrView: {
        title: 'Cash out',
        subtitle: `Show this QR code to @${this.state.scannedUsername} to finish the transaction.`
      },
      completeView: {
        title: 'Cash out',
        subtitle: `Your cash-out order with @${this.state.scannedUsername} was successfully completed.`
      }
    }

    return this.renderViews(data)
  }

  renderViewByType = () => {
    switch(this.state.type) {
      case 'topup':
        return this.renderTopupViews();
      case 'cashout':
        return this.renderCashoutViews();
      default:
        this.setState({type: 'topup'});
    }
  }

  render () {
    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        position={'center'}
        backdropOpacity={0.67}
        backdrop={true}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => {
          this.setState({
            amount: null,
            showEnterAmountView: true,
            showQrCodeView: false
          });

          this.props.onClosed();
        }}
        onOpened={() => { 
      
        }}>
            
        { this.renderViewByType() }
              
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    margin:0,
    height: 400,
    width: '85%',
    borderRadius: 13,
    backgroundColor: 'white',
  },
  reviewContainer: {
    marginTop: 15,
    marginBottom: 10
  },
  reviewKeypairContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15
  },

  reviewKeypairKey: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#0084FF'
  },

  reviewKeypairValue: {
    fontSize: 12,
    color: '#000000',
  },

  keypairContainer: {
    flexDirection: 'row',
    marginBottom: 15
  },

  successIcon: {
    width: 62,
    height: 62
  },
  successContainer: {
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },

  amountContainer: {
    alignContent: 'center', alignItems:'center',  
    marginTop: 10
  },
  centerBody: {
    width: '100%',
    height: 155,
    // borderColor: 'black', borderWidth: 1,
    alignContent: 'center',  justifyContent: 'center',
  },
  amountTitle: {
    color: '#D7D5DB',
    marginBottom: 1,
    fontSize: 12
  },

  amountText: {
    // fontFamily: 'sans-serif',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#030303'
  },

  container: {
    paddingTop: 25, paddingBottom: 40,
    paddingLeft: 40, paddingRight: 40,
    flex: 1
  },

  input: {
    flex: 1, fontSize: 14, color: 'black',
    padding: 5
  },

  customInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    paddingLeft: 10, 
    paddingRight: 10,
    paddingTop: 6,
    paddingBottom: 6,
    flexDirection: 'row',
    // height: 50
  },

  qrCode: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 6,
    // shadowRadius: 3.84,
    elevation: 2
  },

  qrCodeContainer: {
    marginTop: 5,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    // marginBottom: 50,
   
  },

  inputLabel: {
    // color: '#D7D5DB',
    color: 'black',
    marginBottom: 3,
    fontSize: 14,
    fontWeight: '500',
  },

  fieldContainer: {
    marginBottom: 10,
  },

  header: {
    marginTop: 30,
    marginBottom: 8
  },

  title: {
    fontFamily: Platform.OS == 'ios' ? 'Avenir-Black' : 'sans-serif-black',
    textAlign: 'center',
    fontSize: 27,
    marginBottom: 10,
    color: '#212121'
  },
  subtitle: {
    color: 'rgba(0,0,0,0.5)',
    textAlign: 'center',
    fontSize: 12
  },

  content: {
    flex: 1,
    marginBottom: 50,
    
  },
  buttonContainer: {
    flexDirection:'column',
    alignItems: 'center', 
    backgroundColor: 'white',
    marginTop: 5,
    // borderColor: 'red', borderWidth: 1,
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButton: {
    alignItems: 'center',
    height: 50,
    width: 250, 
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    // marginBottom: 30,
    marginTop: 10,
    borderRadius: 27,
    borderWidth: 4,
    borderColor: '#0084FF',
    // paddingRight: 15,
    // paddingLeft: 10,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
  },
  generalButtonTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#fff',
    paddingTop: Platform.OS == 'ios'? 0 : 10,
    paddingBottom: Platform.OS == 'ios'? 0 : 10,
  },
  buttonLabel: {
    color: '#D7D5DB',
    fontSize: 12
  },
})