import React, { PureComponent } from 'react';
import {
  View,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
  Platform
} from 'react-native'
import Text from '../globalTextStyle'
import BankManagementApi from '../../Services/BankManagementApi'

import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import BackButton from "../../Components/BackButton";
import CloseIconButton from '../CloseIconButton'
import moment from 'moment'
import Toast from 'react-native-simple-toast'

export default class BankingRequestDetailsModal extends PureComponent {
  
  updated = false;

  constructor (props) {
    super(props)

    this.state = {
      isOpen: this.props.isOpen,
      bankRequestDetails: this.props.data,
      isProcessing: false,
      isConfirmationModalOpen: false
    }
  }

  componentWillReceiveProps (nextProps) {
    // this.updated = false;
    this.setState({
      isOpen: nextProps.isOpen,
      bankRequestDetails: nextProps.data
    });
  }

  renderLoadingView = () => {
    return(
      <View style={{flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
      }}>
        <ActivityIndicator size="large" color="#0084FF" />
      </View>
    )
  }

  processConfirmation(){
    this.setState({isProcessing: true})
    BankManagementApi.confirmTopupRequest(this.state.bankRequestDetails.bank_account.id, this.state.bankRequestDetails.id, "user_confirmed")
    .then(data => {      
      // Toast.show("Your request was sent for confirmation", Toast.LONG);
    
      this.updated = true;
      this.setState({bankRequestDetails: data})         
      this.setState({isProcessing: false})      
      // this.props.onClosed()
    })
    .catch(error => {
      this.setState({isProcessing: false})
      console.log(error) 
      Toast.show("Something went wrong.", Toast.LONG);
    })
  }

  renderConfirmButton(request){
    if(request == null) return;
    if(request.status.code == "unconfirmed"){
      return (
        <TouchableOpacity 
        style={[styles.button, styles.generalButtonPrimary]}
        onPress={() => { this.processConfirmation() }}
        >
          <Text style={styles.buttonPrimaryTitle}>Confirm</Text>
        </TouchableOpacity>
      )
    }
    else if(request.status.code == "user_confirmed"){
      return(
        <Text style={{paddingTop: 35, color: 'black', fontSize: 12}}>We'll review it shortly</Text>
      )
    }
    else{
      return null
    }
    
  }

  renderActivityIndicator = () => {
    return(
      <View style={{flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        marginTop: 50
      }}>
        <ActivityIndicator size="large" color="#0084FF" />
      </View>
    )
  }

  render () {
    let request = this.state.bankRequestDetails && this.state.bankRequestDetails
    let createdAt = request && moment.utc(request.created_at).local().format('DD MMMM, YYYY')
    let refNo = request && request.ref_no
    let statusName = request && request.status.name
    let amount = request && request.amount + " " + request.asset
    let bankName = request && request.registered_bank.name
    let accountName = request && request.registered_bank.account_name
    let bankNum = request && request.registered_bank.account_number

    let footer = this.state.isProcessing ? this.renderActivityIndicator() : this.renderConfirmButton(request)

    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => {
          // this.props.onClosed(this.state.bankRequestDetails, this.updated)
          // this.updated = false;
        }}
        onOpened={() => { 
      }}>
        
      <View style={styles.header}>
        <View style={styles.headerContent}>
          <BackButton
            onPress={() => {
              this.props.onClosed(this.state.bankRequestDetails, this.updated)
              this.updated = false;
            }}
            arrow={"left"}
            style={{position: 'absolute', top: 5, zIndex: 50}} />
          <Text style={styles.headerTitle}>Topup Details</Text>
        </View>
      </View>
      <View style={styles.background}>
        <View style={styles.container}>        
          <View style={[blockStyles.listItem]}>
            <View style={[blockStyles.listItemLeft]}>
              <View style={styles.leftAlignContent}>
                <Text style={styles.textBold}>REQUEST</Text>
                <Text style={styles.textLight}>{createdAt}</Text>
              </View>
            </View>
            <View style={[blockStyles.listItemRight]}>
              <View style={styles.rightAlignContent}>
                <Text style={styles.textBoldLight}>{refNo}</Text>
                <Text style={styles.requestStatus}>{statusName}</Text>
              </View>                
            </View>
          </View>
          <View>
        <Text style={[listStyles.sectionTitle,blockStyles.bottomSeparator]}>TRUST ACCOUNT</Text>
        <View style={[blockStyles.listItem]}>
          <View style={[blockStyles.listItemLeft]}>
              <View style={styles.leftAlignContent}>
                <Text style={blockStyles.listItemText}>BANK NAME</Text>
                <Text style={blockStyles.listItemSubText}>{bankName}</Text>
              </View>
            </View>
            <View style={[blockStyles.listItemRight]}>
              <View style={styles.rightAlignContent}>
                <Text style={blockStyles.listItemText}>ACCOUNT #</Text>
                <Text style={blockStyles.listItemSubText}>{bankNum}</Text>
              </View>                
            </View>
        </View>
        <View style={[blockStyles.listItem,blockStyles.bottomSeparator]}>
          <View style={[blockStyles.listItemLeft]}>
              <View style={styles.leftAlignContent}>
                <Text style={blockStyles.listItemText}>ACCOUNT NAME</Text>
                <Text style={blockStyles.listItemSubText}>{accountName}</Text>
              </View>
            </View>
            <View style={[blockStyles.listItemRight]}>
              <View style={styles.rightAlignContent}>
                <Text style={blockStyles.listItemText}></Text>
                <Text style={blockStyles.listItemSubText}></Text>
              </View>                
            </View>
        </View>
        </View>
        <View style={[styles.centerVertical]}>
          {/* <View style={styles.centerContent}> */}
          <Text style={blockStyles.listItemText}>TOTAL</Text>
          <Text style={blockStyles.listItemTextFunds}>{amount}</Text>
          {footer}
          {/* </View> */}
        </View>
        </View>
      </View>
      </Modal>
    );
  }
}

const listStyles = StyleSheet.create({
  sectionHeader: {
    paddingRight: 16,
    paddingLeft: 16,
    paddingTop: 16,
    paddingBottom: 5,
    
  },
  sectionTitle: {
    fontWeight: "500",
    color: '#000',
    fontSize: 15
  }
 
});

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  textBold: {
      fontWeight: "500",
      fontSize: 19,
      color: "#000"
  },
  textBoldLight: {
    fontWeight: "500",
    fontSize: 14,
    color: "#A0A0A0"
  },
  requestStatus: {
    fontSize: 13,
    fontWeight: "400",
    color: "#C8C8C8",
    borderRadius: 30,
    borderColor: "#C8C8C8",
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 5,
    paddingBottom: 5,
    borderWidth: 1,
    marginTop: 15
  },
  textLight: {
    fontSize: 12,
    color: "#D8D8D8"
  },
  headerContent: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  header: {
    height: Header.HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'white',
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'flex-end',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16,
    zIndex: 100
  },
  headerTitle: {
    textAlign: 'center',
    fontSize: 18,
    flex: 1,
    fontWeight: Platform.OS == 'ios' ? 'bold' : '500',
    paddingTop: 10,
    color: '#212121'
  },
  background: {
    flex: 1,
    backgroundColor: "#F0F3F7",
    paddingTop: 90,
    paddingRight: 40,
    paddingLeft: 40,
    paddingBottom: 100
  },
  container: {
    // alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    height: 450,
    backgroundColor: "#ffffff",
    // marginTop: 70,
    // marginRight: 40,
    // marginLeft: 40,
    padding: 25,
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 6,
    // shadowRadius: 3.84,
    borderRadius: 5,
    elevation: 2
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButtonSecondary:{
    backgroundColor: '#ffffff'
  },
  centerVertical: {
    // borderColor: 'red', borderWidth: 1,
    // flexDirection: "column",
    flex: 1,
    paddingTop: 1,
    paddingBottom: 1,
    alignItems: 'center'
    //alignContent: "stretch"
  },
  generalButton: {
    alignItems: 'center',
    minHeight: 25,
    minWidth: 90, 
    paddingTop: 4,
    paddingBottom: 4,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 30,
    marginTop: 10,
    borderRadius: 27,
    borderWidth: 3,
    borderColor: '#0084FF',
    marginRight: 15
  },
  generalButtonTitle: {
    fontSize: 13,
    fontWeight: "500",
    color: '#0084FF',
    paddingTop: 5,
    paddingBottom: 5
  },
  button: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    minHeight: 45,
    minWidth: 250, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    paddingRight: Platform.OS == 'ios' ? 25 : 20,
    paddingLeft: Platform.OS == 'ios' ? 25 : 20,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
    backgroundColor: '#0084FF',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 30,
    marginBottom: 20
  },
  buttonPrimaryTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white'
  },
  scrollViewBody: {
    flex: 1,
    // paddingLeft: 20, paddingRight: 20
  },
  centerContent: {
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  leftAlignContent: {
    alignItems: "flex-start"
  },
  rightAlignContent: {
    alignItems: "flex-end"
  },
  contentPadding: {
    paddingTop: 9,
    paddingBottom: 9
  },
});

const blockStyles = StyleSheet.create({
  
  container: {
    marginTop: 5,
    marginBottom: 5,
    paddingBottom: 18
  },
  containerTitle: {
    color: "#ABAFB6",
    fontWeight: "500",
    fontSize: 12
  },  
  block: {
    backgroundColor: "white"
  },

  emptyBlock: {
    minHeight: 173
  },
  centerBlock:{
    // flex: 1,
    // flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "white",
    paddingTop: 35,
    paddingBottom: 35
  },
  blockIconContainer: {
    width: 80,
    height: 80,
    borderRadius: 40,
    padding: 15,
    marginRight: 15,
    backgroundColor: "#F4F6FA",
    marginBottom: 10
  },
  blockIcon: {
    width: 30,
    height: 30
  },
  plusIcon: {
    width: 18,
    height: 18
  },
  statusIconHeader: {
    width: 22,
    height: 22,
    position: 'absolute',
    left: 17,
    top: -80
  },
  transparentBg: {
    backgroundColor: "transparent"
  },
  listItemColumn: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "column",

    paddingTop: 15,
    paddingBottom: 15,

    alignContent: "center",
    justifyContent: "space-between",
    //alignContent: "stretch"
  },
  listItem: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    paddingTop: 4,
    paddingBottom: 17,
    alignContent: "center",
    justifyContent: "space-between",
    //alignContent: "stretch"
  },
  listItemLeft: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-start"
    //paddingTop: 15,
    //paddingBottom: 15,

  },
  listItemRight: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-end"
    //paddingTop: 15,
    //paddingBottom: 15,

  },
  listItemRightFunds: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-end",
    paddingTop: 8,
    //paddingBottom: 15,

  },
  listItemText: {
    fontWeight: "bold",
    fontSize: 14,
    color: "#A0A0A0"
  },
  listItemTextFunds: {
    fontSize: 16,
    color: "#000000",
    fontWeight: "bold"
  },
  listItemSubText: {
    fontSize: 14,
    color: '#000',
    fontWeight: "bold"
  },
  listItemTextPrimary: {
    fontSize: 16,

    color: "#0084FF",

    textAlign: "center"
  },
  listItemTextSecondary: {
    fontSize: 16,

    color: "#0084FF",

    textAlign: "center"
  },
  blockGap: {
    height: 60
  },
  bottomSeparator: {
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
    // borderWidth: 1,
    borderStyle: 'dotted',
    borderRadius: 1,
    paddingBottom: 10,
    marginBottom: 10
  }
});
