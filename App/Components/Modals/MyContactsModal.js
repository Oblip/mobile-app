import React, { Component } from 'react';
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  SectionList,
  Platform,
  Alert
} from 'react-native'
import Text from '../globalTextStyle';

import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'
import ListItem from '../ListItem'

 const DATA = [
  {
    title: 'A',
    data: [
      {
        title: 'Alice Polago',
        subtitle: '668-9898',
        extraData: {
          isAccount: false,
          phoneNumber: '668-9898',
        }
      },
      {
        title: 'Alison Scalf',
        subtitle: '@scalf_alison',
        extraData: {
          isAccount: true,
          username: 'scalf_alison'
        }
      },
      {
        title: 'Allen Guerra',
        subtitle: '@guerra_allen',
        extraData: {
          isAccount: true,
          username: 'guerra_allen'
        }
      }
    ]
  },
  {
     title: 'B',
      data: [
        {
          title: 'Batista Nicole',
          subtitle: '@batista_nic',
          extraData: {
          isAccount: true,
          username: 'batista_nic'
        }
      },
        {
          title: 'Bobby Freisen',
          subtitle: '@freisen_bobby',
          extraData: {
          isAccount: true,
          username: 'freisen_bobby'
        }
      }
      ]
  },
   {
     title: 'L',
      data: [
        {
          title: 'Luis Manzanero',
          subtitle: '@lmanzanero', 
          extraData: {
          isAccount: true,
          username: 'lmanzanero'
        }
      },
        {
          title: 'Lucas Rodriquez',
          subtitle: '657-8994',
          extraData: {
          isAccount: false,
          username: '657-8994'
        }
      }
    ]
  },
    {
    title: 'M',
    data: [
      {
        title: 'Manuel Gonzael De Jesus',
        subtitle: '@manuelg', 
        extraData: {
          isAccount: true,
          username: 'ManuelGonzaelDeJesus'
        }
      }
    ]
  },
  {
     title: 'R',
      data: [
        {
          title: 'Reggie Escobar',
          subtitle: '@prodoxx',
          extraData: {
          isAccount: true,
          username: 'prodoxx'
        }
        },
        {
          title: 'Rita Solaris',
          subtitle: '@solaris_r',
          extraData: {
          isAccount: true,
          username: 'solaris_r'
        }
        },
        {
          title: 'Rezzy Dogulas',
          subtitle: '664-4584',
          extraData: {
          isAccount: false,
          username: '664-4584'
        }
        }
      ]
  },
    {
     title: 'z',
      data: [
        {
          title: 'Rav Pinelo',
          subtitle: '@pinelo_rav',
          extraData: {
          isAccount: true,
          username: 'pinelo_rav'
        }
        }
      ]
  }
];

export default class MyContactsModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isOpen: this.props.isOpen,
      searchQuery: '',
    }
  }

  
  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen
    })
  }

  clearSearch = () => {
    this.setState({
      searchQuery: '',
    })
    this.searchInput.blur();
  }


  render () {
    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => { 
      
        }}>

        <View style={styles.container}>
          <View style={styles.header}>
            <View style={styles.headerContent}>
              <CloseIconButton
                style={{position: 'absolute', zIndex: 50,
                paddingBottom: 20, paddingLeft: 20, paddingRight: 20 }}
                onPressed={() => { this.props.onClosed() }} />
              <Text style={styles.headerTitle}>My Contacts</Text>
            </View>
          </View>
          <View style={styles.subHeaderContainer}>
            <View style={searchStyles.container}>
                <Image 
                  style={searchStyles.leftImage}
                  source={require('../../Assets/Images/search.png')} />

                <TextInput 
                  underlineColorAndroid="transparent"
                  placeholder="Search"
                  placeholderTextColor={'rgba(22,40,66,0.4)'}
                  keyboardType='phone-pad'
                  returnKeyType='search'
                  ref={(input) => { this.searchInput = input; }}
                  onChangeText={(text) => {
                    this.setState({searchQuery: text})
                    if (text == '') this.searchInput.blur();
                  }}
                  onSubmitEditing={() => {
                    Alert.alert('Not available', 'Sorry this feature is not available right now.')
                  }}
                  style={searchStyles.textInput} /> 

                <View style={{
                  opacity: this.state.searchQuery != '' ? 1 : 0,
                  zIndex: this.state.searchQuery != '' ? 1 : -100,
                 
                  justifyContent:'center',
                }}>
                  <TouchableOpacity style={searchStyles.closeButton} 
                    onPress={() => {
                      { this.state.searchQuery != '' ? this.clearSearch() : null }
                    }}>
                    <Image 
                        style={searchStyles.closeIcon}
                        source={require('../../Assets/Images/close_icon.png')} />
                  </TouchableOpacity>
                </View>
            </View>
          </View>
          <View style={styles.content}>
            <SectionList 
              sections={DATA}
              keyExtractor={(item, index) => item + index}
              renderSectionHeader={({section}) => (
                <View style={listStyles.sectionHeader}>
                  <Text style={listStyles.sectionTitle} > {section.title} </Text>
                </View>
              )}
              ListEmptyComponent={() => (
                <View style={listStyles.emptyContainer}>
                  <View style={listStyles.emptyBox}>
                    <Text style={listStyles.emptyTitle}>Nothing Here!</Text>
                    <Text style={listStyles.emptyDescription}>Your future contacts will appear here.</Text>
                  </View>
                </View>
              )}
              renderItem={({item, index, section}) => (
                <ListItem 
                  data={item}
                  currentAccountId={0}
                  onItemPressed={(data) => {
                    // this.props.onContactItemPress(data);
                    Alert.alert('Not available', 'Sorry this feature is not available right now.') 
                  }}
                />
              )}
            />
          </View>
        </View>
      </Modal>
    )
  }

}

const listStyles = StyleSheet.create({
  sectionHeader: {
    paddingRight: 16,
    paddingLeft: 16,
    paddingTop: 16,
    paddingBottom: 5,
    
  },
  sectionTitle: {
    fontWeight: '500',
    color: '#ABAFB6',
    fontSize: 12
  },
  emptyTitle: {
    fontSize: 16,
    color: 'black'
  },
  emptyDescription: {
    fontSize: 12,
    color: 'rgba(0,0,0,0.50)'
  },
  emptyBox: {
    backgroundColor: 'white',
    marginBottom: 10,
    width: '100%',
    minHeight: 150,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  emptyContainer: {
    flex: 1,
    marginTop: 30,
   
  }
})

const searchStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems:'center',
    // flex: 1,
    justifyContent:'center',
    backgroundColor: 'rgba(240,243, 247, 0.50)',
    borderRadius: 8,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 5
  },
  leftImage: {
    width:18,
    height: 18,
    tintColor: 'rgba(22,40,66, 0.4)'
  },
  closeButton: {
    padding: 10,
    marginLeft: 10,
  },
  closeIcon: {
    height: 16,
    width:16,
    marginLeft:5,
    // tintColor: '#727272'
    tintColor: 'black'
  },
  textInput: {
    flex: 1,
    height: 38,
    paddingLeft: 15,
    // borderColor: 'red',
    // borderWidth: 1,
  }
  
});

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  headerTitle: {
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
    flex: 1,
    fontWeight: 'bold',
    paddingTop: 10
  },
  subHeaderContainer: {
    backgroundColor: 'white',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop:16,
    paddingBottom: 16,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  hintText: {
    fontSize: 14,
    textAlign: 'center',
    color: 'rgba(0,0,0,0.5)'
  },
  headerContent: {flexDirection: 'row', justifyContent: 'space-between',},
  header: {
    height: Header.HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'white',
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'flex-end',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16
  },
  content: {
    // borderColor: 'red', borderWidth: 1,
    backgroundColor: '#F0F3F7',
    flex: 1,
    // alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  
  },

  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : StatusBar.currentHeight
  },
})