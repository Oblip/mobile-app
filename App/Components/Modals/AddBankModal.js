import React, { PureComponent } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Platform,
  TextInput,
  // Text
} from 'react-native'
import Text from '../globalTextStyle';

import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'
import SelectItemModal from './SelectItemModal'
import EnterTextModal from './EnterTextModal'
import ConfirmationModal from './ConfirmationModal';
import BankManagementApi from '../../Services/BankManagementApi';
import Toast from 'react-native-simple-toast'
import LoadingModal from './LoadingModal'
import SecureDataStorage from '../../Libs/SecureDataStorage'
import _ from 'underscore'

export default class AddBankModal extends PureComponent {
  constructor (props) {
    super (props);
        
    this.state = {
      isOpen: this.props.isOpen,
      isSelectBankOpen: false,
      isEnterAccountOpen: false,
      isConfirmationModalOpen: false,
      currentBank: null,
      bankData: null,
      bankAccountNumber: null,
      // currentBankAccount: undefined,
      bankAccountName: undefined,
      bankAccountNameDisplay: undefined,
      showLoading: false,
      checked: false,
      accountData: undefined
    }
  }

  componentWillMount() { 

    BankManagementApi.fetchRegisteredBanks()
    .then(res => {
      this.setBankInfo(res);
    }).catch(error => {
      console.log(error)
      Toast.show("Network problem. Try again later", Toast.LONG);
    });

    SecureDataStorage.getAuthAccount()
      .then(res => {
        this.setState({
          accountData: res.account,
          bankAccountName: this.getFullName(res.account),
          bankAccountNameDisplay: this.getFullName(res.account)
        });        
      }).catch(error => {
        // TODO: handle this
      })
  }

  getFullName = (account) => {
    let a = account
    switch(a.type) {
      case 'personal':
        return a != null ? a.profile.name.first_name + ' ' + a.profile.name.last_name : '';
      case 'business':
      return a != null ? a.name : '';
    }  
  }


  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen
    })
  }

  setBankInfo(bankInfo){
    allBanks = [];

    bankInfo.registered_banks.map((bank) => {
      allBanks.push({
        value: bank.id,
        title: bank.name,
        accountName: bank.account_name
      });    
    });
    //console.log(allBanks);
    this.setState({
      bankData: allBanks,
      currentBank: allBanks[0]
    });
  }

  renderSelectBankModal = () => {
    return(
      <SelectItemModal
        isOpen={this.state.isSelectBankOpen}
        onClosed={() => { this.setState({isSelectBankOpen: false})}}
        data={this.state.bankData}
        onItemSelected={(item) => {
          this.setState({
            currentBank: item,
            isSelectBankOpen: false
          })
        }}
        currentItem={this.state.currentBank}
        title={'Select Bank'} />
    )
  }

  renderConfirmationModal = () => {
    return(
      <ConfirmationModal
        isOpen={this.state.isConfirmationModalOpen}
        onClosed={() => { 
          this.setState(
            {
              currentBank: this.state.bankData[0],
              bankAccountNumber: null,
              isConfirmationModalOpen: false
            })
          this.props.onClosed()
        }}        
        modalText={'Bank Account Added'}
        modalSubtext={"We'll verify it shortly and notify you.."} />
    )
  }

  renderEnterAccountModal = () => {
    return(
      <EnterTextModal
        isOpen={this.state.isEnterAccountOpen}
        onClosed={() => { 
          this.setState({
            isEnterAccountOpen: false
          }) 
        }}
        onSubmit={(value) => {
          this.setState({
            bankAccountNumber: value
          })
        }}  
        value={this.state.bankAccountNumber}        
        />
    )
  }

  createAccount = () => {
    if(this.state.currentBank == undefined){
      Toast.show("Please select a bank", Toast.LONG)
      return
    }else if(this.state.bankAccountName == undefined){
      Toast.show("Please enter bank account name", Toast.LONG)
      return
    }    
    else if(this.state.bankAccountNumber == null || this.state.bankAccountNumber.length <= 5 || this.state.bankAccountNumber == ''){
      Toast.show("Please enter a valid bank account number", Toast.LONG)
      return
    }
    //SAVE BANK ACCOUNT INFO HERE
    this.setState({showLoading: true})

    let data = {
      id: this.state.currentBank.value,
      accountNumber: this.state.bankAccountNumber,
      accountName: this.state.bankAccountName,
      isPartner: (this.state.accountData.type == 'business')
    }

    BankManagementApi.createBankAccount(data)
    .then(res => {      
      //Toast.show("Success", Toast.LONG);
      this.setState({
        showLoading: false,
        isConfirmationModalOpen: true
      })
    }).catch(error => {
      console.log(error);
      this.setState({
        showLoading: false
      })
      Toast.show("Network problem. Try again later", Toast.LONG);
    });
  }

  updateBankName = (text) => {
    this.setState({
      bankAccountName: text
    })
  }

  renderAccountNumberText = () => {
    return (
      <Text style={[{color: this.state.bankAccountNumber ? 'black' : '#D8D8D8' , fontSize: 16, padding: 13}]}>
        { this.state.bankAccountNumber ? this.state.bankAccountNumber : 'Enter your bank account number' }
      </Text>
    )
  }

  render () {
    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => { 
      
        }}>
          <View style={styles.container}>
            { this.renderSelectBankModal() }
            { this.renderEnterAccountModal() }    
            { this.renderConfirmationModal() }  
            <LoadingModal showLoading={this.state.showLoading} />      
            <View style={styles.header}>
              <CloseIconButton
                style={{position: 'absolute', zIndex: 50}}
                onPressed={() => { this.props.onClosed() }} />
            </View>
            <View style={styles.modalImage}>
              <Image
                source={require('../../Assets/Images/large_bank_icon.png')}
                style={styles.documents_icon}
                resizeMode={'contain'} />
            </View>
            <Text style={styles.modalTitle}>
                Bank Account
            </Text>
            <ScrollView 
              contentContainerStyle={{paddingTop: 30, paddingBottom: 30}}
              style={[styles.content]}>
              <View>
                  {/* <View style={[styles.fieldContainer, styles.itemInline]}>
                    {this.renderCheckBox()}
                    <Text style={styles.secondaryText}>Is a business/partner bank account</Text>
                  </View> */}
                  <View style={styles.fieldContainer}>
                    <Text style={styles.inputLabel}>Your bank</Text>
                    <TouchableOpacity 
                      onPress={() => { this.setState({isSelectBankOpen: true}) } }
                      style={styles.customSelectInputContainer}>
                        <Text style={styles.customSelectInputValue}>
                          { this.state.currentBank ? this.state.currentBank.title : null}
                        </Text>
                      <View style={styles.centerContent}>
                        <Image
                          source={require('../../Assets/Images/arrow_drop_down_icon.png')}
                          resizeMode={'contain'}
                          style={styles.dropDownIcon} />
                      </View>
                    </TouchableOpacity>
                  </View>
                  {this.state.accountData && this.state.accountData.type == 'business' && <View style={styles.fieldContainer}>
                    <Text style={styles.inputLabel}>Account Name</Text>                  
                    <View style={styles.customInputContainer}>
                      <TextInput
                          style={styles.input}
                          autoCorrect={false}
                          placeholderTextColor={'#D8D8D8'}
                          underlineColorAndroid='transparent'
                          placeholder='Business Name'
                          value={this.state.bankAccountName}
                          onChangeText={(text) => this.updateBankName(text)}
                          // editable={this.state.checked}
                        />                      
                    </View>
                  </View>}
                  <View style={styles.fieldContainer}>
                    <Text style={styles.inputLabel}>Account Number</Text>
                    <TouchableOpacity 
                      onPress={() => { this.setState({isEnterAccountOpen: true}) } }
                      style={styles.customInputContainer}>
                        { this.renderAccountNumberText () }
                    </TouchableOpacity>
                  </View>
              </View>
            </ScrollView>
            <View style={styles.buttonContainer}> 
              <TouchableOpacity
                onPress={() => { this.createAccount() }}
                style={[styles.generalButton, styles.generalButtonPrimary]}>
                <Text style={[styles.generalButtonTitle, styles.generalButtonTitlePrimary]}>Add New</Text>
              </TouchableOpacity>
            </View>
          </View>
        
        </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
  },
  textHighlight: {
    color: 'black'
  },

  textPlaceholder: {
    color: '#D8D8D8'
  },

  content: {
    flex: 1,
    paddingLeft: 18, paddingRight: 18,
    paddingBottom: 14
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: '500',
    color: '#030303',
    marginBottom: 20,
    textAlign: 'center'
  },
  centerContent: { 
    alignContent: 'center', alignItems:'center', justifyContent: 'center',
  },
  container: {
    flex: 1,
    // borderWidth: 1, borderColor: 'green',
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : 0
  },
  header: {
    height: Header.HEIGHT,
    backgroundColor: 'white',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16,
    justifyContent: 'center',
  },
  documents_icon: {
    width: 70, height: 60
  },
  modalImage: {
    height: 80,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  contentPadding: {       
    paddingLeft: 18, paddingRight: 18,
    paddingTop: 14, paddingBottom: 14
  },
  inputLabel: {
    // color: '#D7D5DB',
    color: '#000000',
    fontSize: 12,
    fontWeight: '500',
    marginBottom: 4
  },
  fieldContainer: {
    marginBottom: 35,
  },
  customSelectInputValue: {
    fontSize: 16,
    padding: 13,
    color: '#000000'
  },
  dropDownIcon: {
    width: 10, height: 5,
    tintColor: 'rgba(0,0,0,0.54)'
  },
  customSelectInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 5,
    paddingLeft: 18, 
    paddingRight: 18,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    // height: 50,
    alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch'
  },

  generalButtonIcon: {
    width: 10,
    height: 24,
    marginRight: 10,
  },
  generalButtonTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#232228'
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButtonIconPrimary: {
    tintColor: 'white',
    marginRight: 5
  },
  generalButton: {
    alignItems: 'center',
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 50,
    width: 250, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    // paddingRight: 15,
    // paddingLeft: 10,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
  },

  buttonContainer: {
    flexDirection:'row',
    alignItems:'flex-end',
    justifyContent:'center',
    backgroundColor: 'white',
    marginTop: 5,
    marginBottom: 15,
    // borderColor: 'red', borderWidth: 1,
  },
  input: {
    flex: 1, fontSize: 16, color: 'black',
    padding: 10
  },

  customInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 10,
    paddingLeft: 10, 
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    // height: 50
  },
  itemInline: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-start"
    //paddingTop: 15,
    //paddingBottom: 15,

  },
  secondaryText: {
    paddingTop: 5,
    color: '#DFE2E4',
    textAlign: 'center',
    fontSize: 15,
    fontWeight: '300',
  }
})