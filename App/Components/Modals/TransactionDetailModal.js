import React, { PureComponent } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native'
import Text from '../globalTextStyle';
import DismissKeyboard from 'dismissKeyboard';
import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'

export default class TransactionReviewModal extends PureComponent {
  
  constructor (props) {
    super(props)

    this.state = {
      isOpen: this.props.isOpen,
      remarks: '',
      isLoading: true,
      extraData: this.props.extraData
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen,
      extraData: nextProps.extraData,
      isLoading: nextProps.isLoading
    });
  }  
  
  renderAvatar = () => { 
    return (
      <View>     
        <TouchableOpacity
          onPress={() => {
            // console.log("Icon CLicked")
          }}>
          <Image
            style={headerStyles.avatar}
            source={require('../../Assets/Images/grape_icon.png')} />
        </TouchableOpacity>
      </View>
    );
  };

  renderHeader = () => {
    return (   
      <View style={headerStyles.header}>
        <View style={headerStyles.headerContent}>
          {this.renderAvatar()}
        </View>
        <Text style={headerStyles.headerTitle}>Reggie Escobar</Text>
        <Text style={headerStyles.headerSubtitle}>@prodoxx</Text>
      </View>
    );
  };

  renderAmountDetails = () => {
    return (
      <View style={styles.cashContainer}>  
        <View>
          <Text style={styles.cashAmount}>100.45</Text>
          <Text style={styles.cashDescription}>For partner top up at Chen's Store</Text>
        </View>
      </View> 
    );
  }

  renderTransactionDetails = () => {
    return (
      <View style={styles.transactionContainer}>
        <View style={styles.transactionKeypairContainer}>
          <Text style={styles.transactionKeypairKey}>Id</Text>
          <Text style={styles.transactionKeypairValue}>2323-lkjo0-2lklkj</Text>
        </View>
        <View style={styles.transactionKeypairContainer}>
          <Text style={styles.transactionKeypairKey}>Amount</Text>
          <Text style={styles.transactionKeypairValue}>100.45</Text>
        </View>
        <View style={styles.transactionKeypairContainer}>
          <Text style={styles.transactionKeypairKey}>Asset</Text>
          <Text style={styles.transactionKeypairValue}>BZD</Text>
        </View>
        <View style={styles.transactionKeypairContainer}>
          <Text style={styles.transactionKeypairKey}>Type</Text>
          <Text style={styles.transactionKeypairValue}>Partner Topup</Text>
        </View>
        <View style={styles.transactionKeypairContainer}>
          <Text style={styles.transactionKeypairKey}>Network Fee</Text>
          <Text style={styles.transactionKeypairValue}>0.000001 XLM</Text>
        </View>
        <View style={styles.transactionKeypairContainer}>
          <Text style={styles.transactionKeypairKey}>Transaction</Text>
          <TouchableOpacity><Text style={[styles.transactionKeypairValue, {color: '#0084FF'}] }>View</Text></TouchableOpacity>
        </View>
        <View style={styles.transactionKeypairContainer}>
          <Text style={styles.transactionKeypairKey}>Date</Text>
          <Text style={styles.transactionKeypairValue}>Friday at 12:51pm</Text>
        </View>
      </View>
    )
  }

  renderBody = () => {

    return (
      <ScrollView contentContainerStyle={styles.content}  keyboardDismissMode='on-drag'>
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}>
          { this.renderHeader() }
          { this.renderAmountDetails() }
          { this.renderTransactionDetails () }
        </View>
        
      </ScrollView> 
    )
  }

  renderLoadingView = () => {
    return(
      <View style={{flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
      }}>
        <ActivityIndicator size="large" color="#0084FF" />
      </View>
    )
  }

  render () {

    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        keyboardTopOffset={0}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => { 
      
        }}>
        <TouchableWithoutFeedback onPress={()=>{DismissKeyboard()}}>
          <View style={styles.container}>
            <View style={styles.header}>
              <CloseIconButton
                  style={{position: 'absolute', zIndex: 50}}
                  onPressed={() => { this.props.onClosed() }} />
            </View>

            { this.state.isLoading ? this.renderLoadingView() : this.renderBody () }
            
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}


const headerStyles = StyleSheet.create({
  header: {
    height: 150,
    alignItems: "center",
    backgroundColor: "white",
  },
  headerTitle: {
    fontWeight: "bold",
    fontSize: 16,
    color: "black",
    marginTop: 10
  },
  headerSubtitle: {
    marginTop: 3,
    color: 'rgba(0,0,0,0.5)',
    fontSize: 14
  },
  headerAvatarImage: {
    width: 60,
    height: 60,
    borderRadius: 32
  },
  avatar: {
    width: 60,
    height: 60,
  }
});

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    flex: 1,
    paddingLeft: 30, paddingRight: 30,
    alignContent: 'center',
    textAlign: 'center',
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  transactionContainer: {
    flex: 1,
    marginBottom: 35,
    paddingTop: 20,
    justifyContent: 'center',
  },

  transactionKeypairContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15,
    fontFamily: 'Roboto'
  },

  transactionKeypairKey: { 
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgba(0, 0, 0, 0.4)'
  },

  transactionKeypairValue: { 
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.4)',
  },

  keypairContainer: {
    flexDirection: 'row',
    marginBottom: 15
  },
  headerContent: {flexDirection: 'row', justifyContent: 'space-between',},
  header: {
    height: Header.HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'flex-end',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16
  },
  cashAmount: {
    textAlign: 'center',
    fontSize: 50,
    fontWeight: 'bold',
    color: 'black',
    paddingTop: 10,
    color: 'rgba(0, 0, 0, 0.35)'
  },
  cashDescription: {
    textAlign: 'center',
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.5)',
    paddingBottom:40
  },
  cashContainer: {
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
    height: 200
  },

  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : 0
  },
})
