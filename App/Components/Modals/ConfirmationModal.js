import React, { Component } from 'react';
import {  
  View,
  StyleSheet,
  Image,
  StatusBar,
  TouchableOpacity,
} from 'react-native'
import Text from '../globalTextStyle';
import Modal from 'react-native-modalbox';


export default class ConfirmationModal extends Component {  

  constructor (props) {
    super(props)
    this.state = {
      isOpen: this.props.isOpen,
      modalText: this.props.modalText,
      modalSubtext: this.props.modalSubtext
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen,
      modalText: nextProps.modalText
    })
  }

  render () {
    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => { 
      
        }}>
        <View style={styles.container}>
          <StatusBar
            barStyle={'dark-content'}
            backgroundColor={'white'} />

            <View style={styles.content}>
              <Image
                source={require('../../Assets/Images/success_large_icon.png')}
                resizeMode={'cover'}
                style={styles.successIcon}
              />
              <Text style={styles.title}>{this.props.modalText}</Text>
            </View>
            <View style={styles.buttonContainer}> 
              <Text style={styles.buttonLabel}>{this.props.modalSubtext}</Text>
              <TouchableOpacity
                onPress={() => {this.props.onClosed()}}
                style={[styles.generalButton, styles.generalButtonPrimary]}>
                <Text style={[styles.generalButtonTitle]}>Done</Text>
              </TouchableOpacity>
            </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  successIcon: {
    width: 62,
    height: 62
  },

  title: {
    textAlign: 'center',
    fontSize: 26,
    color: '#232228',
    marginTop: 10
  },
  content: {
    flex: 1,
    marginBottom: 50,
    paddingLeft: '15%',
    paddingRight: '15%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  },
  buttonContainer: {
    flexDirection:'column',
    alignItems: 'center', 
    backgroundColor: 'white',
    marginTop: 5,
    marginBottom: 15,
    // borderColor: 'red', borderWidth: 1,
  },
  generalButtonPrimary: {
    backgroundColor: '#ffffff',
  },
  generalButton: {
    alignItems: 'center',
    minHeight: 40,
    minWidth: 250, 
    paddingTop: 6,
    paddingBottom: 6,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 30,
    marginTop: 10,
    borderRadius: 27,
    borderWidth: 3,
    borderColor: '#0084FF'
  },
  generalButtonTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#0084FF',
    paddingTop: 10,
    paddingBottom: 10
  },
  buttonLabel: {
    color: '#D7D5DB',
    fontSize: 12
  },
})