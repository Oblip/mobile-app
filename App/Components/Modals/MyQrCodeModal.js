import React, { PureComponent } from 'react';
import {
  View,
  StatusBar,
  StyleSheet,
  Platform
} from 'react-native'
import Text from '../globalTextStyle';

import Modal from 'react-native-modalbox';
import QRCode from 'react-native-qrcode-svg';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'
import SecureDataStorage from '../../Libs/SecureDataStorage'
import Toast from 'react-native-simple-toast'

export default class MyQrCodeModal extends PureComponent {
  constructor (props) {
    super (props)

    this.state = {
      isOpen: this.props.isOpen,
      url: ''
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen
    })
  }

  componentWillMount () {
    SecureDataStorage.getAuthAccount()
    .then(res => {
      let account = res.account;
      let url = 'https://oblip.me/';

      switch(account.type) {
        case 'personal':
          url = url + account.username;
          this.setState({url: url});
          break;
        case 'business':

          if(account.is_agent) {
            url = `${url}${account.username}?partner=true`
            this.setState({url: url});
          } else {
            url = `${url}${account.username}`
            this.setState({url: url});
          }

          break;
      }
    }).catch(error => {
      Toast.show('You are not logged in.', Toast.SHORT);
    })
  }

  render () {
    const { username } = this.state;
    return (
      <Modal 
      backdropPressToClose={false}
      style={styles.modal}
      swipeToClose={false}
      position={'center'}
      backdrop={false}
      sideOpening={'vertical'}
      isOpen={this.state.isOpen}
      onClosed={() => this.props.onClosed()}
      onOpened={() => { 
     
      }}>
      
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <CloseIconButton
              style={{ position: 'absolute', zIndex: 50, 
              paddingBottom: 20, paddingLeft: 20, paddingRight: 20 }}
              onPressed={() => { this.props.onClosed() }} />
            <Text style={styles.headerTitle}>My Qr Code</Text>
          </View>
        </View>
        <View style={styles.hintContainer}>
          <Text style={styles.hintText}>Ask sender to scan this QR Code</Text>
        </View>
        <View style={styles.content}>
          <View style={styles.qrCodeContainer}>
            <QRCode
              value={this.state.url}
              logo={require('../../Assets/Images/qr_code_logo_circle.png')}
              logoSize={60}
              logoBackgroundColor='transparent'
              size={260}
            />
          </View>
        </View>
      </View>
    </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  headerTitle: {
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
    flex: 1,
    fontWeight: 'bold',
    paddingTop: 10
  },
  hintContainer: {
    backgroundColor: 'white',
    height: 61,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  hintText: {
    fontSize: 14,
    textAlign: 'center',
    color: 'rgba(0,0,0,0.5)'
  },
  content: {
    // borderColor: 'red', borderWidth: 1,
    backgroundColor: '#F0F3F7',
    flex: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    paddingRight: 20, paddingLeft: 20
  },

  qrCodeContainer: {
    backgroundColor: 'white',
    borderRadius: 10,
    // height: 293,
    // width: 297,
    padding: 20,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    marginBottom: 50,
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 6,
    // shadowRadius: 3.84,
    elevation: 2
  },
  
  headerContent: {flexDirection: 'row', justifyContent: 'space-between',},
  header: {
    height: Header.HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'white',
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'flex-end',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : StatusBar.currentHeight
  },
})