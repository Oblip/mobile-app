import React, { PureComponent } from 'react';
import {  
  View,
  StyleSheet,
  Image,
  TextInput,
  FlatList,
  Platform,
  ScrollView
} from 'react-native'
// import Text from '../globalTextStyle';
import Modal from 'react-native-modalbox'; 
import CloseIconButton from '../CloseIconButton'
import Text from '../globalTextStyle'
import ListItem from '../../Components/ListItem'
import _ from 'underscore'
import SecureDataStorage from '../../Libs/SecureDataStorage'

export default class AccountSwitchModal extends PureComponent {  

  personalData;

  constructor (props) {
    super(props)

    let data = this.formateData(this.props.data)

    this.state = {
      isOpen: this.props.isOpen,  
      data: data
    }
  }

  formateData = (data) => {
    let list = _.map(data, (a) => {
      console.log(a)

      if(a.type == 'personal') {
        return {
          title: this.getFullName(a),
          subtitle: `@${a.username}`,
          extra: {
            id: a.id,
            checkingAccountId: a.checking_account_id,
            type: 'personal'
          }
        }
      } else if (a.type == 'business') {
        return {
          title: a.name,
          subtitle: `@${a.username}`,
          extra: {
            id: a.id,
            checkingAccountId: a.checking_account_id,
            type: 'business'
          }
        }
      }
    })

    console.log(list)

    return list;
  }

  formatPersonalData = (a) => {
    let data = {
      title: this.getFullName(a.account),
      subtitle: `@${a.account.username}`
    }

    return data;
  }


  getFullName = (a) => {
    //console.log("firstname: " + this.state.accountData.profile.name.first_name);
    return a != null ? a.profile.name.first_name + ' ' + a.profile.name.last_name : '';
  }

  componentWillReceiveProps (nextProps) {
    let data = this.formateData(nextProps.data)

    this.setState({
      isOpen: nextProps.isOpen,
      data: data
    })
  }


  renderHeader = () => {
    return(
      <View>
        <CloseIconButton
          style={{position: 'absolute',right: 15, top: 13, zIndex: 50}}
          onPressed={() => { 
            this.props.onClosed()
           }} />
        <View style={styles.header}>
          <Text style={styles.title}>Switch Account</Text> 
        </View>
      </View>
    )
  }

  renderItem = (account) => { 
      return (
        <ListItem
          data={account}
          style={{marginTop: 2}}
          onItemPressed={() => {
              this.props.onAccountItemPress(account);
            }}
        />      
      )
  }


  render () {   
    return (
      <Modal 
        backdropPressToClose={true}
        style={styles.modal}
        position={'center'}
        backdropOpacity={0.67}
        backdrop={true}
        sideOpening={'vertical'} 
        coverScreen={true}
        useNativeDriver={true}
        isOpen={this.state.isOpen} 
        onOpened={() => {}}
        onClosed={() => {
          this.props.onClosed()
        }}
        > 

        { this.renderHeader() } 
      
        <ScrollView
          contentContainerStyle={styles.content}
        >
              <FlatList
            data={this.state.data}
            renderItem={({item}) => this.renderItem(item)}
            keyExtractor={(item, index) => (item + index).toString()}
          />
        </ScrollView>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: { 
    margin:0,
    minHeight: 230,
    maxHeight: 300,
    width: '85%',
    borderRadius: 13,
    backgroundColor: 'white',
  },
  content: {
    marginBottom: 50,
    flex: 1,
    borderRadius: 13,
    backgroundColor: 'transparent'
  },

  container: {
    paddingTop: 25, paddingBottom: 40,
    paddingLeft: 40, paddingRight: 40,
    flex: 1
  },
  header: {
    marginTop: 36,
    marginBottom: 5
  },

  title: {
    fontFamily: Platform.OS == 'ios' ? 'Avenir-Black' : 'sans-serif-black',
    textAlign: 'center',
    fontSize: 27,
    marginBottom: 10,
    color: '#212121'
  }, 
})