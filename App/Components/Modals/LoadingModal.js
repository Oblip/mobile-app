import React, { PureComponent } from 'react';
import {
  View,
  Dimensions,
  StyleSheet,
  Platform
} from 'react-native'
import Animation from 'lottie-react-native';
import Modal from 'react-native-modalbox';

const viewport = Dimensions.get('window')

export default class LoadingModal extends PureComponent {
  animation;
  _isMounted = false;

  constructor (props) {
    super (props)

    this.state = {
      isOpen: this.props.showLoading
    }
  }

  componentWillMount() {
    this._isMounted = true;
  }

  componentWillUnmount () {
    this._isMounted = false;
  }

  componentWillReceiveProps (nextProps) {
   if(this._isMounted) {
    this.setState({
      isOpen: nextProps.showLoading
    });
   }
  }

  render () {
    return (
      <Modal 
      backdropPressToClose={false}
      style={styles.modal}
      swipeToClose={false}
      position={'center'}
      backdrop={true}
      sideOpening={'horizontal'}
      onOpened={() => { 
        // Keyboard.dismiss(); 
       
        if(this.animation) {
          this.animation.play();
        }
        
      }}
      onClosed={() => { if (this.animation) { this.animation.stop() }}}
      isOpen={this.state.isOpen}>

        <View style={styles.container}>
          <View style={styles.loadingContainer}>
            <Animation
              ref={animation => {
                this.animation = animation;
              }}
              loop={true}
              style={styles.loading}
              source={require('../../Assets/Animations/loading_1.json')}
            />
          </View>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    margin: 0
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  },
  loadingContainer: {
    width: Platform.OS == 'ios' ? viewport.width / 3 :   viewport.width / 2.3,
    height: Platform.OS == 'ios' ? viewport.width / 3 :   viewport.width / 2.3,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  },
  loading: {
    width: '100%',
    height: '100%'
  }
})