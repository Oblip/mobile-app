import React, { PureComponent } from 'react';
import getDirections from 'react-native-google-maps-directions';
import BussinessApi from '../../Services/BusinessApi'
import {
  View,
  Image, 
  StatusBar,
  StyleSheet,
  Platform,
  TouchableOpacity,
  ScrollView
} from 'react-native'
import Text from '../globalTextStyle';

import Modal from 'react-native-modalbox'; 
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'
import Toast from 'react-native-simple-toast'
import Geolocation from 'react-native-geolocation-service';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps'
import Permissions from 'react-native-permissions';

export default class PartnerModal extends PureComponent {
  constructor (props) {
    super (props)

    this.state = {
      isOpen: this.props.isOpen,
      isMapView: false,
      region: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121
      },
      markers: []
    } 
  }

  componentWillReceiveProps (nextProps) { 
    this.setState({
      isOpen: nextProps.isOpen
    })
  } 

    //making reference to marker with refs
    setMarkerRef = (ref) => {
      this.marker = ref
    }
  

  getNearByPartners = () =>{  
    this.setState({
      markers: []
    })
    console.log("Clearing markers")
    
    //Fetching new partnered based on the current region
    BussinessApi.filter(true,true, this.state.region.latitude, this.state.region.longitude)
      .then(res => {  
        this.setState({
          markers: res
        })
        console.log("getting new filtered partners this region: ", this.state.region) 
      }).catch(error => {   
        Toast.show("Network problem. Try again later", Toast.LONG); 
    });  
  }
  
  setGeoLocation = () => {
    Geolocation.getCurrentPosition((position) => {
      this.setState({
        region: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121, 
        }
      })   
    }, (error) => {
      console.log(error.code, error.message);
    },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );    
  }
  

  componentDidMount(){  
    if(Platform.OS == 'ios') {
      Geolocation.requestAuthorization();
      this.setGeoLocation();
    } else {
      Permissions.check('location')
        .then(result => {
          if(result == 'authorized') {
            console.log('authorized!!')
            this.setGeoLocation()
          } else {
            // request permission
            Permissions.request('location').then(response => {
              if(response == 'authorized') {
                this.setGeoLocation()
              }
            });
          }
        })
    }
  }

  handleGetDirections(lat, lng){
    console.log("getting directions...", lat)
    let data = {
      source: {
       latitude: this.state.latitude,
       longitude: this.state.longitude
     },
     destination: {
       latitude: lat,
       longitude: lng
     },
     params: [
       {
         key: "travelmode",
         value: "driving"        // may be "walking", "bicycling" or "transit" as well
       },
       {
         key: "dir_action",
         value: "navigate"       // this instantly initializes navigation using the given travel mode
       }
     ]
   }

   getDirections(data)
  }

   mapView = () => {     
    return (
      <View style={styles.mapContainer}>
          <MapView
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={styles.map}
              region={this.state.region}
              showsUserLocation={true}
              moveOnMarkerPress={false}
              onRegionChangeComplete={(center) => {
                 this.setState({
                   region: center
                 }) 
                 this.getNearByPartners()
              }}>
               {
                 this.state.markers.map((agent, key) => {   
                    return( 
                        <Marker
                          coordinate={{
                            latitude: undefined ? 0 : agent.lat,
                            longitude: undefined ?  0 : agent.lng,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421
                          }}
                          key={key}
                          style={styles.marker} 
                          ref={this.setMarkerRef} 
                          onCalloutPress={() => this.handleGetDirections(agent.lat, agent.lng)}
                        > 
                          <Image source={require('../../Assets/Images/round-place-24px.png')} style={{ width: 34, height: 34 }} />
                          <Callout 
                            tooltip={true}  
                            style={styles.boxShadow}>
                            <TouchableOpacity style={styles.boxShadow}>
                                <View style={styles.callOut}>
                                  <Text style={styles.callOutText}>
                                    {agent.name}
                                  </Text>
                                </View>
                            </TouchableOpacity> 
                          </Callout>
                        </Marker>
                    ); 
                 }) 
               }
        </MapView>
      </View>
    )
  }


  static navigationOptions = ({ navigation }) => ({
    header: navigation.getParam("header", null), 
  });

  

  render () { 
    // const toggleAgentView = () => { 
    //     this.setState(prevState => ({
    //       isMapView: !prevState.isMapView
    //     }))
    // }
    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => {}}
        >
      
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <CloseIconButton
                style={{ position: 'absolute', zIndex: 50, 
                paddingBottom: 25, paddingLeft: 20, paddingRight: 20 }}
                onPressed={() => { this.props.onClosed() }} />
              <Text style={styles.headerTitle}>Partners</Text>
              {/* <View style={styles.map_icon}>
                  <TouchableOpacity
                    onPress={() => { 
                      toggleAgentView()
                    }}> 
                    {
                      this.state.isMapView ? <Image
                      source={require('../../Assets/Images/round_list_icon.png')}
                      resizeMode={'contain'}
                      style={styles.list_icon} 
                      /> : <Image
                      source={require('../../Assets/Images/map-icon.png')}
                      resizeMode={'contain'}
                      style={styles.header_icon} 
                      />
                    }
              
                </TouchableOpacity>
              </View> */}
              {/* <View style={styles.filter_list_icon}>
                <TouchableOpacity
                  onPress={() => { 
                  }}> 
                  <Image
                      source={require('../../Assets/Images/round-filter_list-24px.png')}
                      resizeMode={'contain'}
                      style={styles.header_icon} 
                      />
                </TouchableOpacity>
              </View> */}
          </View>
        </View> 
        <View style={styles.content}>
              {/* List Items */} 
              {/* this.state.isMapView ? mapView() :  listView() */}
            { 
              this.mapView()
            }
        </View>
      </View>
    </Modal>
    )
  }
}

const styles = StyleSheet.create({
  mapContainer: {
    ...StyleSheet.absoluteFillObject,
    height: "100%",
    width: "100%",
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  modal: {
    margin: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  headerTitle: {
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
    flex: 1,
    fontWeight: 'bold',
    paddingTop: 10
  },
  content: { 
    backgroundColor: '#F0F3F7',
    flex: 1
  },
  contentContainer: {  
    paddingTop: 18,    
    width: "100%"
  },
  headerContent: {flexDirection: 'row', justifyContent: 'space-between',},
  header: {
    height: Header.HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'white',
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'flex-end',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : StatusBar.currentHeight
  },
  // map_icon: {    
  //  position: "absolute",
  //  top: "-35%",
  //  left: "81%",
  // },
  // filter_list_icon: { 
  //   position: "absolute",
  //   left: "92%",
  //   top: "-35%"
  // },
  header_icon: {
    width: 24
  },
  list_icon: {
    width: 25,
    marginTop: -0.5
  },
  boxShadow: { 
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  callOut: { 
    width: 110,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "#ffffff", 
    borderBottomColor: '#000000',
    borderBottomWidth: 0.1, 
    borderRadius: 15
  },
  callOutText: {
    textAlign: "center",
    margin: 0,
    color: "#0084FF",
    fontSize: 12,
    lineHeight: 14
  }
})