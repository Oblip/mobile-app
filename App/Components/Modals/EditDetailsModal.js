import React, { PureComponent } from 'react';
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  StyleSheet,
  Platform
} from 'react-native'
import Text from '../globalTextStyle';

import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'
import SelectItemModal from './SelectItemModal' 
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment'

export default class EditDetailsModal extends PureComponent {
  constructor (props) {
    super (props);

    this.countries = [{
      value: 1,
      title: 'Belize',
      subtitle: '',
    },{
      value: 2,
      title: 'Gutatemala',
      subtitle: '',
    }];
    
    this.state = {
      isOpen: this.props.isOpen,
      formKey: 0,
      isDateTimePickerVisible: false,
      birthday: '',
      country: null,
      isSelectCountryOpen: false,

    }
  }

  componentWillReceiveProps (nextProps) {  
    this.setState({
      isOpen: nextProps.isOpen,
      formKey: nextProps.formKey,
    })
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
  
  _handleDatePicked = (date) => {
      // console.log(date.toLocaleDateString())
      this.setState({birthday: moment(date).format('YYYY-MM-DD')})
      this._hideDateTimePicker();
     
  };


  renderSelectCountryModal = () => {
    return(
      <SelectItemModal
        isOpen={this.state.isSelectCountryOpen}
        onClosed={() => { this.setState({isSelectNationalityOpen: false})}}
        data={this.countries}
        onItemSelected={(item) => {
          this.setState({
            country: item,
            isSelectCountryOpen: false
          })
        }}
        currentItem={this.state.country}
        title={'Countries'} />
    )
  }
  
  // TODO: refactor to camel case
  FullNameAndDateofBirth = () => {
    return(
      <View style={styles.content}>
          <Text style={styles.title}>Your full name</Text>
          <View style={styles.customInputContainer}>
            <TextInput  
                style={styles.input}
                placeholder="full name"
                placeholderTextColor={'rgba(22,40,66,0.4)'} 
                underlineColorAndroid="transparent" 
                /> 
          </View>
          <Text style={styles.title}>Your date of Birth</Text>
          <View style={styles.fieldContainer}> 
              <TouchableOpacity 
                onPress={() => {}}
                style={styles.customSelectInputContainer}>
                <View style={styles.centerContent}>
                  <Text style={{color: this.state.birthday === '' ? 'rgba(0,0,0,0.16)' : '#172B49', fontSize: 15}}>
                  {this.state.birthday === '' ? 'dd/mm/yy' : this.state.birthday}
                  </Text>
                </View>
                <View style={styles.centerContent}>
                  <Image
                    source={require('../../Assets/Images/arrow_drop_down_icon.png')}
                    resizeMode={'contain'}
                    style={styles.drop_down_icon} />
                </View>
              </TouchableOpacity>
            </View>
      </View> 
    )
  }

  // TODO: refactor to camel case naming
  HomeAddress = () => {
    return(
      <View style={styles.content}> 
        <Text style={styles.title}>Street name</Text>
        <View style={styles.customInputContainer}>
          <TextInput  
              style={styles.input}
              placeholder="street name"
              placeholderTextColor={'rgba(22,40,66,0.4)'}  
              underlineColorAndroid="transparent"
            /> 
        </View>
        <Text style={styles.title}>City</Text>
        <View style={styles.customInputContainer}>
          <TextInput  
              style={styles.input}
              placeholder="city"
              placeholderTextColor={'rgba(22,40,66,0.4)'}  
              underlineColorAndroid="transparent"
            /> 
        </View>
        <Text style={styles.title}>State/District</Text>
        <View style={styles.customInputContainer}>
          <TextInput  
              style={styles.input}
              placeholder="state/sistrict"
              placeholderTextColor={'rgba(22,40,66,0.4)'}  
              underlineColorAndroid="transparent"
              />  
        </View> 
        <Text style={styles.title}>Country</Text>
        <View style={styles.fieldContainer}> 
            <TouchableOpacity 
              onPress={() => { this.setState({isSelectCountryOpen: true})} }
              style={styles.customSelectInputContainer}>
              <View style={styles.centerContent}>
                <Text style={{color: this.state.country === null ? 'rgba(0,0,0,0.16)' : '#172B49', fontSize: 15}}>
                {this.state.country == null ? 'select your country' : this.state.country.title}
                </Text>
              </View>
              <View style={styles.centerContent}>
                <Image
                  source={require('../../Assets/Images/arrow_drop_down_icon.png')}
                  resizeMode={'contain'}
                  style={styles.drop_down_icon} />
              </View>
            </TouchableOpacity>
          </View>
      </View>
    )
  }

  // TODO: refactor to camel case naming
  PhoneNumber = () => {
    return(
      <View style={styles.content}>  
          <Text style={styles.title}>Phone number</Text>
          <View style={styles.customInputContainer}>
              <TextInput  
                  style={styles.input}
                  placeholder="enter your phone number"
                  placeholderTextColor={'rgba(22,40,66,0.4)'}  
                  underlineColorAndroid="transparent"
                  />  
            </View>  
      </View>
    )
  }

  // TODO: refactor to camel case naming
  Email = () => {
    return(
      <View style={styles.content}>  
          <Text style={styles.title}>Email</Text>  
            <View style={styles.customInputContainer}>
              <TextInput  
                  style={styles.input}
                  placeholder="email"
                  placeholderTextColor={'rgba(22,40,66,0.4)'}  
                  underlineColorAndroid="transparent"
              />  
          </View>
      </View>
    )
  }

  // Displays form group inputs inside edit details modal
  // TODO: refactor this to use Constants instead of numbers
  currentFromGroup = (key) => {
    if(key == 0){
      return this.FullNameAndDateofBirth(); 
    } else if(key == 1) {
      return this.HomeAddress();
    } else if (key == 2) {
      return this.PhoneNumber();
    } else if (key == 3){ 
      return this.Email();
    } else {
      console.log("No more feild groups");
    }

  }

  render () {
    return (
      <Modal 
          backdropPressToClose={false}
          style={styles.modal}
          swipeToClose={false}
          position={'center'}
          backdrop={false}
          sideOpening={'vertical'}
          isOpen={this.state.isOpen}
          onClosed={() => this.props.onClosed()}
          onOpened={() => {}} >

        { this.renderSelectCountryModal () }

        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          mode={'date'}
        />

        <EditDetailsModal
          style={{zIndex: 50}}
          onClosed={this.onEditDetailsModalClose}
          isOpen={this.state.isMyEditDetailsModalOpen}  
          formKey={this.state.formKey}
        />

        <ScrollView>
            <View style={styles.container}>
                <View style={styles.header}>
                  <View style={styles.headerContent}>
                    <CloseIconButton
                      style={{position: 'absolute', zIndex: 50}}
                      onPressed={() => { this.props.onClosed()}} />
                      <Text style={styles.headerTitle}>Edit Details</Text>
                    </View>
                </View>
                <View style={styles.formContainer}>
                  {this.currentFromGroup(this.state.formKey)}
                </View>
            </View>
        </ScrollView>
        <View style={styles.buttonContainer}> 
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.setParams({ header: null });
              }}
              style={[styles.generalButton, styles.generalButtonPrimary]}>
              <Text style={[styles.generalButtonTitle, styles.generalButtonTitlePrimary]}>Save</Text>
            </TouchableOpacity>
        </View> 
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {   
    margin: 0, 
    flex: 1,
    backgroundColor: 'white'
  },
  content: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    alignContent: 'center',  
  }, 
  formContainer: {
    marginTop: 20
  },
  container: {
    margin: 0,
    flex: 1, 
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : 0
  },
  headerTitle: {
    textAlign: 'center',
    fontSize: 18,
    color: 'black',
    flex: 1,
    fontWeight: 'bold',
    paddingTop: 10
  },
  header: {
    height: Header.HEIGHT,
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16,
    justifyContent: 'flex-end',
    flexDirection: 'column',
  },
  documents_icon: {
    width: 130, height: 120
  },
  modalImage: {
    height: 150,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  generalButtonIcon: {
    width: 10,
    height: 24,
    marginRight: 10,
  },
  generalButtonTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#232228'
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButtonIconPrimary: {
    tintColor: 'white',
    marginRight: 5
  },
  generalButton: {
    alignItems: 'center',
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 50,
    width: 250, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    // paddingRight: 15,
    // paddingLeft: 10,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 3,
  },

  buttonContainer: {
    flexDirection:'row',
    alignItems:'flex-end',
    justifyContent:'center',
    marginTop: 5,
    marginBottom: 15,
    backgroundColor: 'white'
    // borderColor: 'red', borderWidth: 1,
  },
  title: {
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    color: '#232228',
    marginTop: 5
  },
  input: {
    flex: 1, fontSize: 14, color: 'black',
    padding: 10
  },
  headerContent: {flexDirection: 'row', justifyContent: 'space-between',},
  inputLabel: {
    color: '#D7D5DB',
    fontSize: 12,
    fontWeight: '500',
  },
  centerContent: { 
    alignContent: 'center', alignItems:'center', justifyContent: 'center',
  },
  fieldContainer: {
    marginBottom: 20
  },
  customSelectInputValue: {
    fontSize: 16,
    color: '#000000'
  },
  drop_down_icon: {
    width: 10, height: 5,
    tintColor: 'rgba(0,0,0,0.54)'
  },
  customSelectInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 5,
    paddingLeft: 18, 
    paddingRight: 18,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    height: 60,
    alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch'
  },
  customInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 10,
    paddingLeft: 10, 
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    marginBottom: 30,
    flexDirection: 'row',
  }
});
 