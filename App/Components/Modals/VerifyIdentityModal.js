import React, { PureComponent } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Alert,
  StyleSheet,
  ScrollView,
  Platform
} from 'react-native'
import Text from '../globalTextStyle';

import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'
import SelectItemModal from './SelectItemModal'

export default class VerifyIdentityModal extends PureComponent {
  constructor (props) {
    super (props);

    nationalities = [{
      value: 1,
      title: 'Belizean',
      subtitle: 'A citizen of Belize',
    },{
      value: 2,
      title: 'Dominican',
      subtitle: 'A citizen of The Republic of Dominica',
    }];

    documentTypes = [{
      value: 1,
      title: 'Driving License',
      subtitle: 'Issued in Belize or any supported country'
    }, {
      value: 2,
      title: 'Passport',
      subtitle: ''
    }, {
      value: 3,
      title: 'Social Security Card',
      subtitle: ''
    }, {
      value: 4,
      title: 'National Voter ID Card',
      subtitle: ''
    }]

    this.state = {
      isOpen: this.props.isOpen,
      isSelectNationalityOpen: false,
      isSelectDocumentTypeOpen: false,
      currentNationality: nationalities[0],
      nationalityData: nationalities,
      currentDocumentType: documentTypes[0],
      documentTypeData: documentTypes
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen
    })
  }

  renderSelectNationalityModal = () => {
    return(
      <SelectItemModal
        isOpen={this.state.isSelectNationalityOpen}
        onClosed={() => { this.setState({isSelectNationalityOpen: false})}}
        data={this.state.nationalityData}
        onItemSelected={(item) => {
          this.setState({
            currentNationality: item,
            isSelectNationalityOpen: false
          })
        }}
        currentItem={this.state.currentNationality}
        title={'Nationality'} />
    )
  }

  renderSelectDocumentTypeModal = () => {
    return(
      <SelectItemModal
        isOpen={this.state.isSelectDocumentTypeOpen}
        onClosed={() => { this.setState({isSelectDocumentTypeOpen: false})}}
        data={this.state.documentTypeData}
        onItemSelected={(item) => {
          this.setState({
            currentDocumentType: item,
            isSelectDocumentTypeOpen: false
          })
        }}
        currentItem={this.state.currentDocumentType}
        title={'Type of Document'} />
    )
  }

  render () {
    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => { 
      
        }}>
          <View style={styles.container}>
            { this.renderSelectNationalityModal() }
            { this.renderSelectDocumentTypeModal() }
            <View style={styles.header}>
              <CloseIconButton
                style={{position: 'absolute', zIndex: 50}}
                onPressed={() => { this.props.onClosed() }} />
            </View>
            <View style={styles.modalImage}>
              <Image
                source={require('../../Assets/Images/documents_icon.png')}
                style={styles.documents_icon}
                resizeMode={'contain'} />
            </View>
            <ScrollView 
              style={[styles.content]}>
              <Text style={styles.modalTitle}>
                Submit a document that can prove you live in Belize
              </Text>
              <View>
                  <View style={styles.fieldContainer}>
                    <Text style={styles.inputLabel}>Nationality</Text>
                    <TouchableOpacity 
                      onPress={() => { this.setState({isSelectNationalityOpen: true}) } }
                      style={styles.customSelectInputContainer}>
                      <View style={styles.centerContent}>
                        <Text style={styles.customSelectInputValue}>
                          { this.state.currentNationality ? this.state.currentNationality.title : null}
                        </Text>
                      </View>
                      <View style={styles.centerContent}>
                        <Image
                          source={require('../../Assets/Images/arrow_drop_down_icon.png')}
                          resizeMode={'contain'}
                          style={styles.drop_down_icon} />
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.fieldContainer}>
                    <Text style={styles.inputLabel}>Type of document</Text>
                    <TouchableOpacity 
                      onPress={() => { this.setState({isSelectDocumentTypeOpen: true}) } }
                      style={styles.customSelectInputContainer}>
                      <View style={styles.centerContent}>
                        <Text style={styles.customSelectInputValue}>
                          { this.state.currentDocumentType ? this.state.currentDocumentType.title : null }
                        </Text>
                      </View>
                      <View style={styles.centerContent}>
                        <Image
                          source={require('../../Assets/Images/arrow_drop_down_icon.png')}
                          resizeMode={'contain'}
                          style={styles.drop_down_icon} />
                      </View>
                    </TouchableOpacity>
                  </View>
              </View>
            </ScrollView>
            <View style={styles.buttonContainer}> 
              <TouchableOpacity
                onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                style={[styles.generalButton, styles.generalButtonPrimary]}>
                <Text style={[styles.generalButtonTitle, styles.generalButtonTitlePrimary]}>Continue</Text>
              </TouchableOpacity>
            </View>
          </View>
        
        </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
  },
  content: {
    flex: 1,
    paddingLeft: 18, paddingRight: 18,
    paddingBottom: 14
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: '500',
    color: '#030303',
    marginBottom: 20,
    textAlign: 'center'
  },
  centerContent: { 
    alignContent: 'center', alignItems:'center', justifyContent: 'center',
  },
  container: {
    flex: 1,
    // borderWidth: 1, borderColor: 'green',
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : 0
  },
  header: {
    height: Header.HEIGHT,
    backgroundColor: 'white',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16,
    justifyContent: 'center',
  },
  documents_icon: {
    width: 130, height: 120
  },
  modalImage: {
    height: 130,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  contentPadding: {       
    paddingLeft: 18, paddingRight: 18,
    paddingTop: 14, paddingBottom: 14
  },
  inputLabel: {
    color: '#D7D5DB',
    fontSize: 12,
    fontWeight: '500',
  },
  fieldContainer: {
    marginBottom: 20
  },
  customSelectInputValue: {
    fontSize: 16,
    color: '#000000'
  },
  drop_down_icon: {
    width: 10, height: 5,
    tintColor: 'rgba(0,0,0,0.54)'
  },
  customSelectInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 5,
    paddingLeft: 18, 
    paddingRight: 18,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    height: 50,
    alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch'
  },

  generalButtonIcon: {
    width: 10,
    height: 24,
    marginRight: 10,
  },
  generalButtonTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#232228'
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButtonIconPrimary: {
    tintColor: 'white',
    marginRight: 5
  },
  generalButton: {
    alignItems: 'center',
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 50,
    width: 250, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    // paddingRight: 15,
    // paddingLeft: 10,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
  },

  buttonContainer: {
    flexDirection:'row',
    alignItems:'flex-end',
    justifyContent:'center',
    backgroundColor: 'white',
    marginTop: 5,
    marginBottom: 15,
    // borderColor: 'red', borderWidth: 1,
  },
})