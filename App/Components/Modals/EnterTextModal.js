import React, { PureComponent } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Platform,
  TextInput
} from 'react-native'
import Text from '../globalTextStyle';

import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'

export default class EnterTextModal extends PureComponent {
  constructor (props) {
    super (props);

    this.state = {
      isOpen: this.props.isOpen,
      bankAccount: undefined
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen,
      bankAccount: nextProps.value
    })
  }

  _appendToAccountNumber = (value) => {
    let currentBankAccount = this.state.bankAccount != undefined ? this.state.bankAccount.toString() : ""

    if (value == '') { return; }
    
    let newValue = currentBankAccount + '' + value      
    this.setState({bankAccount:  newValue.trim()})    

    // this.amountText.fadeInDownBig(500)  
  }

  _removeValueFromAccountNumber = () => {
    let currentBankAccount = this.state.bankAccount.toString()
    
    if(currentBankAccount.length > 1) {
      let amountToRemove = 1     
      
      setTimeout(() => {
        this.setState({bankAccount: currentBankAccount.substr(0, currentBankAccount.length - amountToRemove)})
      }, 200)
    } else {
      this.setState({bankAccount: ''})
    }

    // this.amountText.fadeOut(800)
  }

  onDoneButtonPressed = () => {
    // this.props.onChangeText(bankAccount)
    this.props.onSubmit(this.state.bankAccount)
    this.props.onClosed()
  }

  render () {
    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => { 
      
        }}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <CloseIconButton
              style={{position: 'absolute', zIndex: 50}}
              onPressed={() => { this.props.onClosed() }} />
            <Text style={styles.headerTitle}>{this.props.title}</Text>
          </View>
        </View>
        <View style={styles.container}>         
          <View style={styles.centerContent}>
            <Text style={styles.modalTitle}>Enter your bank account number</Text>
          </View>
          <View style={styles.customInputContainer}>
            <TextInput
                style={styles.input}
                autoCorrect={false}
                editable={false}
                placeholderTextColor={'#D8D8D8'}
                underlineColorAndroid='transparent'
                placeholder='Bank account number'
                value={this.state.bankAccount}
              />
          </View>          
        </View>
        <View style={keyboardStyles.container}>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('1')}>
              <Text style={keyboardStyles.keyText}>1</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('2')}>
              <Text style={keyboardStyles.keyText}>2</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('3')}>
              <Text style={keyboardStyles.keyText}>3</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('4')}>
              <Text style={keyboardStyles.keyText}>4</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('5')}>
              <Text style={keyboardStyles.keyText}>5</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('6')}>
              <Text style={keyboardStyles.keyText}>6</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('7')}>
              <Text style={keyboardStyles.keyText}>7</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('8')}>
              <Text style={keyboardStyles.keyText}>8</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('9')}>
              <Text style={keyboardStyles.keyText}>9</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton}>
              <Text style={[keyboardStyles.keyText]}></Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._appendToAccountNumber('0')}>
              <Text style={keyboardStyles.keyText}>0</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._removeValueFromAccountNumber()}>
              <Image
                source={require('../../Assets/Images/ios_back_icon.png')}
                resizeMode={'contain'}
                style={keyboardStyles.deleteIcon}
                />
            </TouchableOpacity>
          </View>
        </View>
        <View style={[styles.buttonContainer]}>
          <TouchableOpacity onPress={() => this.onDoneButtonPressed()}>            
            <Text style={styles.modalButton}>Done</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    )
  }
}

const keyboardStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20
  },

  deleteIcon: {
    width: 12,
    height: 21
  },
  keyButton: {
    // borderColor: 'red', borderWidth: 1,
    flex: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    padding: 10
  },
  keyText: {
    fontSize: 27,
    color: 'rgba(0,0,0,0.5)',
    fontWeight:  Platform.OS == 'ios' ? '400' : '300'
  },
  keyRow: {
    flexDirection: 'row',
    width: '100%',
    paddingBottom: 10,
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'space-around', 
    alignContent: 'center', alignItems:'center',
  }
})

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
    backgroundColor: 'white',
    
  },
  content: {
    flex: 1,
    marginTop: 10,
    paddingLeft: 18, paddingRight: 18,
    paddingBottom: 14
  },
  centerContent: { 
    alignContent: 'center', alignItems:'center', justifyContent: 'center',
  },
  modalButton: {
    fontSize: 22,
    fontWeight: "500",
    paddingTop: 40,
    paddingBottom: 40,
    paddingRight: 100,
    paddingLeft: 100,
    marginBottom: 20,
    color: "#0084FF",
  },
  modalTitle: {
    // // fontWeight: "500",
    // marginBottom: 35,
    // textAlign: 'center',
    // // color: '#030303',
    // color: '#232228',
    // fontSize: 18
    textAlign: 'center',
    fontSize: 18,
    color: '#232228',
    marginBottom: 20
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    // borderWidth: 1, borderColor: 'green',
    backgroundColor: 'white',
    // paddingLeft: 20,
    // paddingRight: 20,
    paddingLeft: '15%',
    paddingRight: '15%',
    paddingTop: Platform.OS == 'ios' ? 20 : 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    height: 75,
    // borderColor: 'orange', borderWidth: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  headerTitle: {
    textAlign: 'center',
    fontSize: 18,
    flex: 1,
    fontWeight: Platform.OS == 'ios' ? 'bold' : '500',
    paddingTop: 10,
    color: '#212121'
  },
  headerContent: {flexDirection: 'row', justifyContent: 'space-between',},
  header: {
    height: Header.HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'white',
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'flex-end',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16,
    zIndex: 100
  },
  input: {
    flex: 1, fontSize: 16, color: 'black',
    padding: 10
  },

  customInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 10,
    paddingLeft: 10, 
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    // height: 50
  }
})