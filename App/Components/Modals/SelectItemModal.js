import React, { PureComponent } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Platform
} from 'react-native'
import Text from '../globalTextStyle';

import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'

export default class SelectItemModal extends PureComponent {
  constructor (props) {
    super (props);

    this.state = {
      isOpen: this.props.isOpen,
      data: this.props.data,
      currentItem: this.props.currentItem
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen,
      data: nextProps.data,
      currentItem: nextProps.currentItem
    })
  }

  renderItem = (item) => {

    renderActiveIcon = () => {
      return (
        <View style={[itemStyles.iconContainer, styles.centerContent]}>
          <Image
            source={require('../../Assets/Images/done_icon.png')}
            resizeMode={'contain'}
            style={itemStyles.selectedIcon} />
        </View>
      )
    }

    return(
      <TouchableOpacity 
        onPress={() => { this.props.onItemSelected(item) } }
        style={itemStyles.container}>
        <View style={itemStyles.content}>
          <Text style={this.state.currentItem == item ? itemStyles.activeTitle : itemStyles.title}>{item.title}</Text>
          <Text style={itemStyles.subtitle}>{item.subtitle}</Text>
        </View>
        { this.state.currentItem == item ? renderActiveIcon () : null }
      </TouchableOpacity>
    )
  }

  render () {
    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => { 
      
        }}>
        <View style={styles.container}>
          <View style={styles.header}>
            <View style={styles.headerContent}>
              <CloseIconButton
                style={{position: 'absolute', zIndex: 50}}
                onPressed={() => { this.props.onClosed() }} />
              <Text style={styles.headerTitle}>{this.props.title}</Text>
            </View>
          </View>
          <FlatList
            style={styles.content}
            data={this.state.data}
            renderItem={({item}) => this.renderItem(item)}
            keyExtractor={(item, index) => item + index}
          />
        </View>
      </Modal>
    )
  }
}

const itemStyles = StyleSheet.create({
  container: {
    // borderColor: 'red', borderWidth: 1,
    paddingTop: 10, paddingBottom: 10,
    flexDirection: 'row',
    alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch' 
  },
  content: {
    // borderColor: 'green', borderWidth: 2,
    width: '85%'
  },
  title: {
    fontSize: 15,
    color: '#000000'
  },
  activeTitle: {
    color: '#0084FF',
    fontWeight: Platform.OS == 'ios' ? 'bold' : '500',
  },
  subtitle: {
    color: '#bbb8bf',
    fontSize: 14,
    marginTop: 3
  },
  iconContainer: {
    width: 30,
  },
  selectedIcon: {
    width: 19,
    height: 15,
    tintColor: '#0084FF'
  }
})

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
    backgroundColor: 'white',
    
  },
  content: {
    flex: 1,
    marginTop: 10,
    paddingLeft: 18, paddingRight: 18,
    paddingBottom: 14
  },
  centerContent: { 
    alignContent: 'center', alignItems:'center', justifyContent: 'center',
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: '500',
    color: '#030303',
    marginBottom: 20,
    textAlign: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    // borderWidth: 1, borderColor: 'green',
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : 0
  },
  headerTitle: {
    textAlign: 'center',
    fontSize: 18,
    flex: 1,
    fontWeight: Platform.OS == 'ios' ? 'bold' : '500',
    paddingTop: 10,
    color: '#212121'
  },
  headerContent: {flexDirection: 'row', justifyContent: 'space-between',},
  header: {
    height: Header.HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'white',
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'flex-end',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16,
    zIndex: 100
  },
  // header: {
  //   height: Header.HEIGHT,
  //   paddingLeft: 5, paddingRight: 16,
  //   paddingBottom: 16,
  //   justifyContent: 'center',
  // },
})