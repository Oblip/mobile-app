import React, { PureComponent } from 'react';
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  TouchableWithoutFeedback,
  Platform,
  KeyboardAvoidingView
} from 'react-native'
import Text from '../globalTextStyle';

import DismissKeyboard from 'dismissKeyboard';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import Modal from 'react-native-modalbox';
import { Header } from 'react-navigation';
import CloseIconButton from '../CloseIconButton'

export default class TransactionReviewModal extends PureComponent {
  
  constructor (props) {
    super(props)

    this.state = {
      isOpen: this.props.isOpen,
      remarks: '',
      isLoading: true,
      extraData: this.props.extraData
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen,
      extraData: nextProps.extraData,
      isLoading: nextProps.isLoading
    });
  }  

  renderBody = () => {
    const amount = this.state.extraData && `${this.state.extraData.amount} ${this.state.extraData.asset}`

    return (
      <View style={{flex: 1}}>
        <ScrollView style={styles.content} keyboardDismissMode='on-drag'>
          <View>
            <View style={styles.recipientInfoContainer}>
              <View style={styles.keypairContainer}>
                <Text style={styles.recipientInfoKey}>To: </Text>
                <Text style={styles.recipientInfoValue}>{this.state.extraData && this.state.extraData.account && this.state.extraData.account.name}</Text>
              </View>
              <View style={styles.keypairContainer}>
                <Text style={styles.recipientInfoKey}>For: </Text>
                <TextInput 
                  keyboardType={'default'}
                  underlineColorAndroid={'#0084FF'}
                  autoCorrect={true}
                  returnKeyLabel={'Confirm'}
                  returnKeyType={'go'}
                  value={this.state.message}
                  onChangeText={(text) => this.props.onMessageChanged(text)}
                  style={[styles.forTextInput, Platform.OS == 'ios' ? styles.addPrimaryBorderBottom : null]}
                  placeholder={'pizza, gas, tip, etc.'}
                  />
              </View>
            </View>
            {/* <View style={styles.header}>
              <View style={styles.headerContent}>
                <Text style={[styles.headerTitle, {fontSize: 14}]}>Review</Text>
              </View>
            </View>
            <View style={styles.reviewContainer}>
              <View style={styles.reviewKeypairContainer}>
                <Text style={styles.reviewKeypairKey}>Amount</Text>
                <Text style={styles.reviewKeypairValue}>{amount}</Text>
              </View>
              <View style={styles.reviewKeypairContainer}>
                <Text style={styles.reviewKeypairKey}>Network Fee</Text>
                <Text style={styles.reviewKeypairValue}>0.000001 XLM</Text>
              </View>
              <View style={styles.reviewKeypairContainer}>
                <Text style={styles.reviewKeypairKey}>Partner Fee</Text>
                <Text style={styles.reviewKeypairValue}>0.0005 XLM</Text>
              </View>
            </View> */}
          </View>
          
        </ScrollView>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => {
              this.props.onConfirmation();
            }}
            style={styles.button}>
            <Text style={styles.buttonPrimaryTitle}>Confirm</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderLoadingView = () => {
    return(
      <View style={{flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
      }}>
        <ActivityIndicator size="large" color="#0084FF" />
      </View>
    )
  }

  render () {
    const amount = this.state.extraData && `${this.state.extraData.amount} ${this.state.extraData.asset}`

    return (
      <Modal 
        backdropPressToClose={false}
        style={styles.modal}
        swipeToClose={false}
        position={'center'}
        backdrop={false}
        keyboardTopOffset={0}
        sideOpening={'vertical'}
        isOpen={this.state.isOpen}
        onClosed={() => this.props.onClosed()}
        onOpened={() => { 
      
        }}>
        <TouchableWithoutFeedback onPress={()=>{DismissKeyboard()}}>
          <View style={styles.container}>
            <View style={styles.header}>
              <View style={styles.headerContent}>
                <CloseIconButton
                  style={{position: 'absolute', zIndex: 50}}
                  onPressed={() => { this.props.onClosed() }} />
                <Text style={styles.headerTitle}>{amount}</Text>
              </View>
            </View>
            { this.state.isLoading ? this.renderLoadingView() : this.renderBody () }
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    margin: 0,
    flex: 1,
    backgroundColor: 'white',
  },
  buttonPrimaryTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white'
  },
  buttonContainer: {
    height: 75,
    marginBottom: 30,
    // borderColor: 'orange', borderWidth: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 55,
    width: '80%',
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#0084FF',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    paddingRight: Platform.OS == 'ios' ? 20 : 15,
    paddingLeft: Platform.OS == 'ios' ? 20 : 15,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
    
  },
  content: {
    flex: 1,
    paddingLeft: 30, paddingRight: 30,
    // justifyContent: 'space-between',
    flexDirection: 'column',
    backgroundColor: 'white'
  },

  forTextInput: {
    flex: 1,
    // height: 36,
    fontSize: 14,
    // backgroundColor: 'red'
  },

  addPrimaryBorderBottom: {
    paddingBottom: 4,
    borderBottomColor: '#0084FF', borderBottomWidth: 1,
  },
  recipientInfoContainer: {
    // borderColor: 'blue', borderWidth: 1,
    marginTop: 20
  },

  reviewContainer: {
    marginBottom: 15,
    paddingTop: 20
  },

  reviewKeypairContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 15
  },

  reviewKeypairKey: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#0084FF'
  },

  reviewKeypairValue: {
    fontSize: 12,
    color: '#000000',
  },

  keypairContainer: {
    flexDirection: 'row',
    marginBottom: 15
  },

  recipientInfoKey: {
    fontSize: 14,
    flexDirection: 'column',
    alignSelf: 'center',
    fontWeight: 'bold',
    color: '#0084FF',
    marginRight: 10
  },

  recipientInfoValue: {
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold'
  },

  headerTitle: {
    textAlign: 'center',
    fontSize: 17,
    color: 'black',
    flex: 1,
    fontWeight: 'bold',
    paddingTop: 10
  },
  headerContent: {flexDirection: 'row', justifyContent: 'space-between',},
  header: {
    height: Header.HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'white',
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'flex-end',
    paddingLeft: 5, paddingRight: 16,
    paddingBottom: 16
  },
  container: {
    flex: 1,
    // borderColor: 'red', borderWidth: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS == 'ios' ? 20 : 0
  },
})