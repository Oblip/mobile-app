import React, { PureComponent } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native' 
import Text from './globalTextStyle';

const AVATAR_SIZE = 40
const AVATAR_RADIUS = 20
const DEFAULT_AVATAR = require('../Assets/Images/pineapple_icon.png')

export default class ListItem extends PureComponent {
  constructor (props) {
    super(props);

    this.state = {
      data: this.props.data
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      data: nextProps.data
    })
  }

  renderButton(){
    return(
      <View
        style={[styles.statusButton]}>
            <Image
              style={{
                width: 20, height: 20,
                marginRight: 5
              }}
              resizeMode={'cover'}
              source={require('../Assets/Images/red_notice_icon.png')}
              />
            <Text style={[styles.generalButtonTitle, {color: '#FF1744', paddingBottom: 18}]}>Confirm</Text>
      </View>
    )
  }

  render () {
    let button = this.state.data.button === "unconfirmed" ?  this.renderButton() : undefined
    return (
      <TouchableOpacity
        style={[styles.itemContainer, this.props.style]}
        onPress={() => {
          this.props.onItemPressed(this.state.data) 
        }}>
        <View style={styles.avatarContainer}>
          <Image 
            source={this.state.data.icon || DEFAULT_AVATAR}
            resizeMode={'center'}
            style={this.props.iconStyle || styles.avatar}
            />
        </View>
        <View style={styles.itemBody}>
          <View style={styles.content}>
            <Text style={styles.itemTitle}>{this.state.data && this.state.data.title}</Text>
            <Text style={styles.itemSubtitle} numberOfLines={1}>{this.state.data && this.state.data.subtitle}</Text>
          </View>
        </View>
        <View style={styles.listItemRight}>
          {button}
          <View style={[styles.content, styles.itemSideLayout]}>
            <Text style={[styles.rightContentTitle, this.props.rightTitleStyle]}>{this.state.data && this.state.data.rightTitle}</Text>
            <Text style={styles.rightContentSubtitle}>{this.state.data && this.state.data.rightSubtitle}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    backgroundColor: 'white',
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 8,
    paddingBottom: 8,
    flexDirection: 'row',
    marginTop: 11,
    minHeight: 57
  },
  itemTitle: {
    fontSize: 15,
    fontWeight: '700',
    color: '#030303'
  },
  itemSubtitle: {
    color: '#B8BBC0',
    fontSize: 12,
    flex: 1,
    fontWeight: '300',
    marginTop: 2,
  },
  avatarContainer: {
    // height: AVATAR_SIZE + 5,
    // width: AVATAR_SIZE + 5,
    // borderRadius: AVATAR_RADIUS * 2,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  itemBody: {
    marginTop: -4,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  itemSideLayout: {
    // marginTop: 7
  },
  content: {
    paddingLeft: 16, 
    paddingTop: 5,
    // borderColor: 'red', borderWidth: 1,
  },
  rightContentTitle: {
    fontSize: 13,
    fontWeight: '500',
    // color: '#46474B',
    color: '#0084FF',
    textAlign: 'right'
  },
  rightContentSubtitle: {
    fontSize: 12,
    marginTop: 3,
    textAlign: 'right',
    // fontWeight: 'normal',
    color: 'black'
    // color: 'rgba(70,71,75, 0.71)'
  },
  avatar: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_RADIUS,
    justifyContent: 'center',
  },
  generalButtonSecondary:{
    backgroundColor: '#ffffff'
  },
  statusButton:{
    flexDirection: 'row',
    position: 'absolute',
    right: 50,
    top: 15
  },
  generalButton: {
    alignItems: 'center',
    minHeight: 15,
    minWidth: 80, 
    paddingTop: 4,
    paddingBottom: 4,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 5,
    marginTop: 10,
    borderRadius: 27,
    borderWidth: 2,
    borderColor: '#0084FF',
    marginRight: 8
  },
  generalButtonTitle: {
    fontSize: 12,
    fontWeight: '400',
    color: '#0084FF',
    paddingTop: 2,
    paddingBottom: 2
  },
  listItemRight: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-end"
    //paddingTop: 15,
    //paddingBottom: 15,
  }
})