import React, { PureComponent } from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity
} from 'react-native'

export default class BackButton extends PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      arrow: this.props.arrow ? this.props.arrow : "right"
    }
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      arrow: nextProps.arrow
    });
  }

  getArrowIcon(){
    if (this.state.arrow === "right"){
      return Platform.OS == 'ios' ? require('../Assets/Images/ios_back_right_icon.png') : require('../Assets/Images/android_back_right_icon.png')
    }
    else{
      return Platform.OS == 'ios' ? require('../Assets/Images/ios_back_icon.png') : require('../Assets/Images/android_back_left_icon_dark.png')
    }
  }

  render () {
    const arrow = this.getArrowIcon()
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onPress()
        }}
        style={[styles.button, this.props.style]}>
          <Image
            source={arrow}
            style={[Platform.OS == 'ios' ? styles.iosIcon : styles.androidIcon, this.props.iconStyle]}
            resizeMode={'cover'} />
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    paddingLeft: 15, 
    paddingRight: 15, 
    paddingBottom: 10, 
    paddingTop: 10,
    // borderColor: 'red', borderWidth: 1,
    flex: 1
  },
  iosIcon: {
    width: 12,
    height: 21
  },
  androidIcon: {
    width: 17,
    height: 16,
  }
})