import React, { PureComponent } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity

} from 'react-native'
import Text from './globalTextStyle';

export default class NormalButton extends PureComponent {
  constructor (props) {
    super (props)
  }

  render () {
    return (
      <TouchableOpacity
        onPress={() => this.props.onPress()}
        style={[styles.button, this.props.style]}>
          <Text style={styles.buttonText}>{this.props.title}</Text>
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    padding: 5
  },
  buttonText: {
    color: '#0084FF',
    fontSize: 16,
    fontWeight: '500',
    textDecorationLine: 'underline'
  }
})