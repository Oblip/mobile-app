import React, { PureComponent } from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity
} from 'react-native'

export default class CloseIconButton extends PureComponent {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onPressed()
        }}
        style={[styles.button, this.props.style]}>
          <Image
            source={Platform.OS == 'ios' ? require('../Assets/Images/ios_close_icon.png') : require('../Assets/Images/android_close_icon.png')}
            style={[styles.icon, this.props.iconStyle]}
            resizeMode={'cover'} />
        </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    paddingLeft: 15, paddingRight: 15, paddingBottom: 10, 
    paddingTop: 10,
    // borderColor: 'red', borderWidth: 1,
    flex: 1
  },
  icon: {
    width: 16,
    tintColor: '#000000',
    height: 16
  }
})