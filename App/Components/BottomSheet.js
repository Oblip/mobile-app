import React, { PureComponent } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
  FlatList,
} from 'react-native'

import Text from './globalTextStyle';
import Modal from 'react-native-modalbox';

const AVATAR_SIZE = 40
const AVATAR_RADIUS = 20

export default class BottomSheet extends PureComponent {

  state = {
    isOpen: this.props.isOpen,
    data: this.props.data
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      isOpen: nextProps.isOpen,
      data: nextProps.data
    })
  }

  renderItem = (item) => {
   return (
    <TouchableOpacity
      style={styles.itemContainer}
      onPress={() => {
        item.action();
      }}>
        <View style={styles.iconContainer}>
          <Image 
            source={item.icon}
            resizeMode={'cover'}
            style={styles.icon}
            />
        </View>

        <View style={styles.itemBody}>
          <Text style={styles.title}>{item.title}</Text>
        </View>
    </TouchableOpacity>
   )
  }

  render () {
   return(
    <Modal 
      backdropPressToClose={true}
      style={styles.modal}
      position={'bottom'}
      backdrop={true}
      backdropOpacity={0.67}
      isOpen={this.state.isOpen}
      onClosed={() => this.props.onClosed()}
      onOpened={() => { 

      }}>
        <View style={styles.container}>
          <FlatList
            style={styles.content}
            data={this.state.data}
            renderItem={({item}) => this.renderItem(item)}
            keyExtractor={(item, index) => item + index}
          />
        </View>
      </Modal>
   )
  }
}

const styles = StyleSheet.create({
  modal: {
    margin:0,
    backgroundColor: 'transparent'
    // backgroundColor: 'rgba(0,0,0,0.5)'
  },
  container: {
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
    minHeight: 100,
    width: '100%'
  },
  content: {
    width: '85%'
  },
  itemContainer: {
    backgroundColor: 'white',
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
    paddingBottom: 16,
    flexDirection: 'row',
    marginTop: 3
  },
  iconContainer: {
    height: AVATAR_SIZE + 5,
    width: AVATAR_SIZE + 5,
    borderRadius: AVATAR_RADIUS * 2,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  icon: {
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_RADIUS,
    justifyContent: 'center',
  },
  itemBody: {
    marginLeft: 25,
    flex: 1,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 16,
    color: 'black'
  }
});
