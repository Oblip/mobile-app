import React, { PureComponent } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native' 
import Text from './globalTextStyle';

export default class AgentsListItem extends PureComponent {
  constructor (props) {
    super(props);
  }
  render () {
    return (
      <TouchableOpacity onPress={() => {alert("Feature not available")}}>
        <View style={styles.itemContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{this.props.agentName}</Text>
            </View> 
            <Text style={styles.distanceText}>{this.props.agentLocation}</Text>
            <View style={styles.iconContainer}>
                <TouchableOpacity onPress={() => {alert(`Ready to Top up at ${this.props.agentName}?`)}}>
                  
                  <Image
                    source={require('../Assets/Images/more_icon.png')}
                    resizeMode={'cover'}
                    style={styles.more_icon} />
              </TouchableOpacity> 
            </View>
          </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  itemContainer: { 
    backgroundColor: 'white',
    paddingTop: 16,
    paddingBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
    flexDirection: 'row',
    marginTop: 8, 
    width: "100%"
  },
  titleContainer: { 
    width: 100, 
    height: 40,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  title: {
    position: "absolute",
    left: "4.37%", 
    fontSize: 14,
    lineHeight: 16,
    fontWeight: 'bold',
    color: "black"
  },
  distanceText: { 
    position: "absolute",
    height: 14, 
    fontSize: 12, 
    top: "70%",
    left: "84%",
    right: "13.35%"
  },
  iconContainer: {
    position: "absolute",
    left: "97%", 
    top: "60%"
  },
  more_icon: { 
    height: 24,
    width: 24
  }
})