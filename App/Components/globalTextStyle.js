'use strict';

import React, { PureComponent} from 'react';
import {
  Text,
  Platform
} from 'react-native';

export default class AppText extends PureComponent {
  constructor(props) {
    super(props)
    
    let style = this.generateStyle(this.props.style)
    

    this.state = {
      style: style
    }
  }

  generateStyle = (extra) => {
    let style = [{fontFamily: Platform.OS == 'ios' ? 'Avenir' : 'sans-serif', fontSize: 16}]; 
    if( extra ) {
      if( Array.isArray(extra) ) {
        style = style.concat(extra)
      } else {
        style.push(extra)
      }
    }

    return style
  }

  componentWillReceiveProps (nextProps) {
    let style = this.generateStyle(nextProps.style)
    this.setState({
      style: style
    })
  }

  render() { return (
    <Text {...this.props} style={this.state.style}>
      {this.props.children}
    </Text>
  )}
}