// @flow
import firebase from 'react-native-firebase';
import type { RemoteMessage } from 'react-native-firebase';
import PushNotification from '../Libs/PushNotification'

// // Optional flow type
// import type { RemoteMessage } from 'react-native-firebase';

export default async (message: RemoteMessage) => {
  // handle your message

  return PushNotification.handleNotification(message)
    
}
