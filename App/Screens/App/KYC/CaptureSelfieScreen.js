import React, { Component } from 'react';
import {  
  View,
  Text,
  Platform,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Dimensions
} from 'react-native'

import { RNCamera } from 'react-native-camera';
import ImageCrop from '../../../Libs/ImageCrop'
import { Svg, Defs, Rect, Mask, Circle,Ellipse } from 'react-native-svg';
// import {Svg, G, Path, Circle } from 'react-native-svg';

const { height, width } = Dimensions.get('window');
const maskRowHeight = Math.round((height - 200) / 10);
const maskColWidth = (width - 300) / 2;

const maskTotalHeight = height - (maskRowHeight * 2)
const maskTotalWidth = (width - (maskColWidth * 2)) - 50

const OvalOverlay = () => (
  
  <Svg height="100%" width="100%" viewBox="0 0 100 100" preserveAspectRatio="none" >
      <Defs>
          <Mask id="mask" x="0" y="0" height="100%" width="100%">
              <Rect height="100%" width="100%" fill="#fff" />
              {/* <Circle r="24" cx="50" cy="40" /> */}
              <Ellipse
                cx="50"
                cy="41"
                rx="31"
                ry="26"
              />
          </Mask>
      </Defs>
      <Rect height="100%" width="100%" fill="rgba(1,1,1,0.6)" mask="url(#mask)" fill-opacity="0" />
  </Svg>

);


export default class CaptureSelfieScreen extends Component {
  constructor (props) {
    super (props)

    this.state = {
      isPictureTaken: false,
      pictureUri: '',
      zoom: 50,
      bottomViewHeight: 0,
      topViewHeight: 0,
      isPictureCropped: false,
      croppedImageBase64: 'data:image/jpeg;base64,',
      // croppedImageBase64: 'https://scontent-mia3-1.xx.fbcdn.net/v/t1.0-9/60085092_600047273827187_1143021557744926720_o.jpg?_nc_cat=109&_nc_ht=scontent-mia3-1.xx&oh=6d6cfc6752b94a161817c3c1a32bd373&oe=5D90A2BB'
    }
  }

  static navigationOptions = ({navigation, screenProps}) => ({
    title: 'Selfie',
    headerTitleStyle: {
      flex: 1,
      textAlign: 'center',
      color: navigation.getParam('headerTitleColor', 'white')
    },
    headerStyle: {
      position: navigation.getParam('headerPositionType', 'absolute'),
      top: navigation.getParam('headerTopPosition', Platform.OS == 'ios' ? 20 : StatusBar.currentHeight),  
      backgroundColor: navigation.getParam('headerBackgroundColor', 'transparent'),
      elevation: 0.1,
      width: '100%',
      shadowColor: 'transparent',
      shadowOpacity: 0,
      borderBottomColor: 'transparent',
      
    },
    // Capture
    headerTintColor: navigation.getParam('headerTintColor', 'white'),
    headerRight: <View></View>
  });

  takePicture = async () => {
    if(this.camera) {
      const options = { 
        quality: 0.5,
         base64: false, 
         mirrorImage: true,
         pauseAfterCapture: false,
         fixOrientation: true,
        };

      await this.camera.takePictureAsync(options)
        .then(data => {
          // console.log(data.uri)
          this.props.navigation.setParams({
            headerTitleColor: '#212121',
            headerPositionType: 'relative',
            headerTopPosition: 0,
            headerTintColor: '#212121',
            headerBackgroundColor: 'white'
          })
          this.startCropping()
          this.setState({
            pictureUri: data.uri,
            isPictureTaken: true,
          })
        })
    }
  }

  renderImageCropPreview = () => {
   
    return (
      <View style={{opacity: 0}}>
        <ImageCrop 
          ref={'cropper'}
          image={this.state.pictureUri}
          // cropHeight={maskTotalWidth - 25}
          // cropWidth={maskTotalWidth - 50}
          cropHeight={280}
          cropWidth={230}
          zoom={this.state.zoom}
          centerX={0.5}
          centerY={0.4}
          panToMove={false}
          pinchToZoom={true}
          style={{
            alignSelf: 'center',
            borderRadius: 500,
            // borderRadius: (maskTotalWidth - 25) / 2
          }}
        />
      </View>
    )
  }

  renderCroppedImage = () => {
    return (
     <View style={{
       width: 290,
       height: 290,
       alignContent: 'center', alignItems:'center',  justifyContent: 'center',
     }}>
       <View style={{
          transform: [
            {scaleY: 1.5},
            {scaleX: 1.3}
          ],
          alignContent: 'center', alignItems:'center',  justifyContent: 'center',
       }}>
          <Image
            resizeMode={'cover'}
            style={{
              // alignSelf: 'center',
              width: 190, 
              height:  190,
              borderRadius: 190 / 2,
            }}
            source={{uri: this.state.croppedImageBase64}} />
       </View>
     </View>
    )
  }

  startCropping = () => {
    if(!this.state.isPictureCropped) {
     let zoomTimer=  setTimeout(() => {
       console.log('Gonna Zoom!')
        this.setState({zoom: 43})
        clearTimeout(zoomTimer)
      }, 800)
      let cropTimer = setTimeout(() => {
        console.log('Gonna crop!')
        if(this.refs.cropper) {
          
          this.refs.cropper.crop()
            .then(res =>{
              this.setState({
                isPictureCropped: true,
                croppedImageBase64: res,
              });

              clearTimeout(cropTimer)
            })
        }
        
      }, 2000)
     }
  }

  renderSelfiePreviewView = () => {
    

     return(
      <View style={selfiePreviewStyles.container}>
      <StatusBar
      backgroundColor={'white'}
      barStyle={'dark-content'}
      translucent={false} />
      <View style={[styles.contentPadding, selfiePreviewStyles.content]}>
        
          <View style={selfiePreviewStyles.previewContainer}>
            { (this.state.pictureUri != '' && !this.state.isPictureCropped) ? this.renderImageCropPreview() : this.renderCroppedImage() }
            {/* <Image
              source={{uri: this.state.pictureUri}}
              style={{width: '100%', height: '100%', borderColor: 'red', borderWidth: 1 }}
              resizeMode={'cover'} /> */}
          </View>
      
        <View style={selfiePreviewStyles.buttonsContainer}>
          <View style={selfiePreviewStyles.buttonContainer}>
            <TouchableOpacity
              onPress={() => {}}
              style={[selfiePreviewStyles.button,selfiePreviewStyles.generalButtonPrimary]}>
              <Text style={selfiePreviewStyles.buttonPrimaryTitle}>My selfie is clear</Text>
            </TouchableOpacity>
          </View>
          <View style={selfiePreviewStyles.buttonContainer}>
          <TouchableOpacity
            style={selfiePreviewStyles.secondaryButton}
            onPress={() => {
              this.props.navigation.setParams({
                headerTitleColor: 'white',
                headerPositionType: 'absolute',
                headerTopPosition: Platform.OS == 'ios' ? 20 : StatusBar.currentHeight,
                headerTintColor: 'white',
                headerBackgroundColor: 'transparent'
              })
              this.setState({
                isPictureTaken: false,
                isPictureCropped: false,
                croppedImageBase64: '',
                pictureUri: '',
                zoom: 50
              })
            }}>
              <Text style={selfiePreviewStyles.secondaryButtonTitle}>Take a new picture</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      </View>
      )
  }

  renderCaptureSelfieView = () => {
    return (
      <View style={captureSelfieStyles.container}>
        <StatusBar
        backgroundColor={'transparent'}
        barStyle={'light-content'}
        translucent={true} />
        {!this.state.isPictureTaken && <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style = {captureSelfieStyles.preview}
        type={RNCamera.Constants.Type.front}
        mirrorImage={true}
        captureAudio={false}
        fixOrientation={true}
        pauseAfterCapture={true}
        autoFocus={RNCamera.Constants.AutoFocus.on}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}>

          <View style={captureSelfieStyles.overlay}>
            <OvalOverlay />
          </View>
          
          <View style={[captureSelfieStyles.content, styles.contentPadding]}>
            <Text style={captureSelfieStyles.instructionText}>Position your face in the oval</Text>
          </View>
          <View style={captureSelfieStyles.buttonContainer}>
            <TouchableOpacity
              onPress={() =>  this.takePicture()}>
              <Image
              resizeMode={'contain'}
              source={require('../../../Assets/Images/photo_capture_btn.png')}
              style={captureSelfieStyles.photoCaptureImage} />
              </TouchableOpacity>
          </View>
      </RNCamera> }
      </View>
    )
  }

  render () {
    if(!this.state.isPictureTaken) {
        return this.renderCaptureSelfieView()
      } else {
        return this.renderSelfiePreviewView()
      }
  }
}

    
const styles = StyleSheet.create({
  contentPadding: {       
    paddingLeft: 18, paddingRight: 18,
    paddingTop: 14, paddingBottom: 14
  },
})

const selfiePreviewStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  content: {
    flex: 1,
    alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch'
  },
  buttonContainer: {
    height: 75,
    // borderColor: 'orange', borderWidth: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  buttonPrimaryTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white'
  },
  secondaryButtonTitle: {
    fontSize: 14,
    color: '#0084FF',
  },
  secondaryButton: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 45,
    minWidth: 50,
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 2,
    borderRadius: 6,
    paddingRight: 50,
    paddingLeft: 50,
    paddingTop: 20,
    paddingBottom: 20,
  },
  button: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 45,
    minWidth: 50, 
    backgroundColor: '#0084FF',
    justifyContent: 'center',
    marginBottom: 2,
    marginTop: 10,
    borderRadius: 6,
    paddingRight: 50,
    paddingLeft: 50,
    paddingTop: 20,
    paddingBottom: 20,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
    
  },
  previewContainer: {
    height: maskTotalWidth + 30,
    // borderColor: 'red', borderWidth: 1,
    width: '100%',
    marginTop: 20, marginBottom: 20,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    // alignContent: 'center',
    // justifyContent: 'center'
  },
  title: {
    color: '#353335',
    fontSize: 22,
    textAlign: 'center',
    marginBottom: 5
  },
  description: {
    marginTop: 5,
    fontSize: 16,
    color: '#9B9A9B',
    textAlign: 'center'
  },
})

const captureSelfieStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center'
  },

  overlay: {
    width: '100%', height: '100%', position: 'absolute'
  },
  buttonContainer: {
    marginBottom: 40
  },
  photoCaptureImage: {
    width: 73, height: 73
  },
  title: {
    color: 'white',
    fontSize: 22,
    textAlign: 'center',
    marginBottom: 10
  },
  instructionText: {
    marginTop: 90,
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  preview: {
    flex: 1,
    zIndex:1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  content: {
    position: 'absolute',
    width: '100%',
    // height: 200,
    // borderColor: 'red', borderWidth: 1,
    zIndex: 10,
    top: (height / 2) + 20
  },
  maskOutter: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  maskInner: {
    width: 300,
    backgroundColor: 'transparent',
    borderColor: 'rgba(255,255,255,0.3)',
    borderRadius: 6,
    borderWidth: 1.5,
  },
  maskFrame: {
    backgroundColor: 'rgba(1,1,1,0.6)',
  },
  maskRow: {
    width: '100%',
  },
  maskCenter: { flexDirection: 'row' },
})