import React, { Component } from 'react';
import {  
  View,
  Text,
  Platform,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Dimensions
} from 'react-native'

import { RNCamera } from 'react-native-camera';
import { Header } from 'react-navigation';
import ImageCrop from '../../../Libs/ImageCrop'

const { height, width } = Dimensions.get('window');
const maskRowHeight = Math.round((height - 200) / 10);
const maskColWidth = (width - 300) / 2;

const maskTotalHeight = height - (maskRowHeight * 2)
const maskTotalWidth = width - (maskColWidth * 2)

// console.log('Total screen height: ' + height)
// console.log('Mask Row Height: ' + maskRowHeight)
// console.log('Mask Col Width: ' + maskColWidth)
// console.log('Mask Width: ' + maskTotalWidth)
// console.log('Mask height: ' + maskTotalHeight)

export default class CaptureDocumentScreen extends Component {
  constructor (props) {
    super (props)

    this.camOn = true
    
    this.state = {
      isPictureTaken: false,
      pictureUri: '',
      zoom: 50,
      bottomViewHeight: 0,
      topViewHeight: 0,
      isPictureCropped: false,
      croppedImageBase64: 'data:image/jpeg;base64,',
    }
  }
  
  static navigationOptions = ({navigation, screenProps}) => ({
    title: 'Social Security Card',
    headerTitleStyle: {
      flex: 1,
      textAlign: 'center',
      color: navigation.getParam('headerTitleColor', 'white')
    },
    headerStyle: {
      position: navigation.getParam('headerPositionType', 'absolute'),
      top: navigation.getParam('headerTopPosition', Platform.OS == 'ios' ? 20 : StatusBar.currentHeight),  
      backgroundColor: navigation.getParam('headerBackgroundColor', 'transparent'),
      elevation: 0.1,
      width: '100%',
      shadowColor: 'transparent',
      shadowOpacity: 0,
      borderBottomColor: 'transparent',
      
    },
    // Capture
    headerTintColor: navigation.getParam('headerTintColor', 'white'),
    headerRight: <View></View>
  });

  // componentDidMount ()  {
  //   this._navDidFocusListener = this.props.navigation.addListener('didFocus', () => {
  //     console.log('Capture Document: it focused')
  //   })
  //   this._navDidBlurListener = this.props.navigation.addListener('didBlur', () => {
  //     console.log('It blur :/')
  //     this.camOn = false
  //   });
  // }

  // componentWillUnmount () {
  //   this._navDidFocusListener.remove()
  //   this._navDidBlurListener.remove()
  // }

  
  takePicture = async () => {
    if (this.camera2) {
      const options = { 
        quality: 0.5,
         base64: false, 
         mirrorImage: false,
         pauseAfterCapture: false,
         fixOrientation: true,
        };
      await this.camera2.takePictureAsync(options)
        .then(data => {
          // console.log(data.uri)
          this.props.navigation.setParams({
            headerTitleColor: '#212121',
            headerPositionType: 'relative',
            headerTopPosition: 0,
            headerTintColor: '#212121',
            headerBackgroundColor: 'white'
          })
          
          this.setState({
            pictureUri: data.uri,
            isPictureTaken: true,
          })
        })
    }
  }
  
  renderCaptureDocumentView = () => {
    return (
      <View style={captureDocumentStyles.container}>
        <StatusBar
        backgroundColor={'transparent'}
        barStyle={'light-content'}
        translucent={true} />
        {!this.state.isPictureTaken && <RNCamera
        ref={ref => {
          this.camera2 = ref;
        }}
        style = {captureDocumentStyles.preview}
        type={RNCamera.Constants.Type.back}
        mirrorImage={true}
        captureAudio={false}
        fixOrientation={true}
        pauseAfterCapture={true}
        autoFocus={RNCamera.Constants.AutoFocus.on}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}>
          
          <View style={captureDocumentStyles.maskOutter}>
          
            <View 
              onLayout={(event) => {
                console.log('top view height: ' + event.nativeEvent.layout.height)
                this.setState({
                  topViewHeight: event.nativeEvent.layout.height
                })
              }}
              style={[{ flex: maskRowHeight - 20  }, captureDocumentStyles.maskRow, captureDocumentStyles.maskFrame]} />
            <View style={[{ flex: 30 }, captureDocumentStyles.maskCenter]}>
            <View style={[{ width: maskColWidth}, captureDocumentStyles.maskFrame]} />
            <View style={captureDocumentStyles.maskInner} />
            <View style={[{ width: maskColWidth}, captureDocumentStyles.maskFrame]} />
            </View>
            
            <View
              onLayout={(event) => {
                console.log('Botmt view height: ' + event.nativeEvent.layout.height)
                this.setState({
                  bottomViewHeight: event.nativeEvent.layout.height
                })
              }}
              style={[{ flex: maskRowHeight + 8}, captureDocumentStyles.maskRow, captureDocumentStyles.maskFrame]} />
          </View>
          
          <View style={[captureDocumentStyles.content, styles.contentPadding]}>
          <Text style={captureDocumentStyles.title}>Font of Card</Text>
          <Text style={captureDocumentStyles.instructionText}>Position all 4 corners of the front clearly in the frame</Text>
          </View>
          <View style={captureDocumentStyles.buttonContainer}>

          
          <TouchableOpacity
            onPress={() =>  this.takePicture()}>
            <Image
            resizeMode={'contain'}
            source={require('../../../Assets/Images/photo_capture_btn.png')}
            style={captureDocumentStyles.photoCaptureImage} />
            </TouchableOpacity>
          </View>
      </RNCamera> }
      </View>
    )
  }

  renderImageCropPreview = () => {
   
    return (
      <ImageCrop 
        ref={'cropper'}
        image={this.state.pictureUri}
        cropHeight={height - (this.state.bottomViewHeight + this.state.topViewHeight)}
        cropWidth={maskTotalWidth}
        zoom={this.state.zoom}
        centerX={0.5}
        centerY={0.35}
        panToMove={false}
        pinchToZoom={true}
      />
    )
  }

  renderCroppedImage = () => {
    return (
      <Image
        resizeMode={'cover'}
        style={{width: '100%', height: '100%' }}
        source={{uri: this.state.croppedImageBase64}} />
    )
  }
    
  renderDocumentPreviewView = () => {


   if(!this.state.isPictureCropped) {
    setTimeout(() => {
      this.setState({zoom: 33})
    }, 800)
    setTimeout(() => {
      console.log('Ready to crop!')
      if(this.refs.cropper) {
        
        this.refs.cropper.crop()
          .then(res =>{
            this.setState({
              isPictureCropped: true,
              croppedImageBase64: res,
            });
          })
      }
      
    }, 1000)
   }

    return(
      <View style={documentPreviewStyles.container}>
      <StatusBar
      backgroundColor={'white'}
      barStyle={'dark-content'}
      translucent={false} />
      <View style={[styles.contentPadding, documentPreviewStyles.content]}>
        <View>
          <View style={documentPreviewStyles.previewContainer}>
            { (this.state.pictureUri != '' && !this.state.isPictureCropped) ? this.renderImageCropPreview() : this.renderCroppedImage() }
            {/* <Image
              source={{uri: this.state.pictureUri}}
              style={{width: '100%', height: '100%', borderColor: 'red', borderWidth: 1 }}
              resizeMode={'cover'} /> */}
          </View>
          <View>
            <Text style={documentPreviewStyles.title}>Check quality</Text>
            <Text style={documentPreviewStyles.description}>
              Make sure your card is physically present, all details are clear to read
              with no blur or glare
            </Text>
          </View>
        </View>
        <View style={documentPreviewStyles.buttonsContainer}>
          <View style={documentPreviewStyles.buttonContainer}>
            <TouchableOpacity
              onPress={() => {}}
              style={[documentPreviewStyles.button,documentPreviewStyles.generalButtonPrimary]}>
              <Text style={documentPreviewStyles.buttonPrimaryTitle}>My card is readable</Text>
            </TouchableOpacity>
          </View>
          <View style={documentPreviewStyles.buttonContainer}>
          <TouchableOpacity
            style={documentPreviewStyles.secondaryButton}
            onPress={() => {
              this.props.navigation.setParams({
                headerTitleColor: 'white',
                headerPositionType: 'absolute',
                headerTopPosition: Platform.OS == 'ios' ? 20 : StatusBar.currentHeight,
                headerTintColor: 'white',
                headerBackgroundColor: 'transparent'
              })
              this.setState({
                isPictureTaken: false,
                isPictureCropped: false,
                croppedImageBase64: '',
                pictureUri: ''
              })
            }}>
              <Text style={documentPreviewStyles.secondaryButtonTitle}>Take a new picture</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      </View>
      )
    }
    
    render () {
      // return this.renderCaptureDocumentView()
      if(!this.state.isPictureTaken) {
        return this.renderCaptureDocumentView()
      } else {
        return this.renderDocumentPreviewView()
      }
    }
  }
    
const styles = StyleSheet.create({
  contentPadding: {       
    paddingLeft: 18, paddingRight: 18,
    paddingTop: 14, paddingBottom: 14
  },
})

const documentPreviewStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  content: {
    flex: 1,
    alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch'
  },
  buttonContainer: {
    height: 75,
    // borderColor: 'orange', borderWidth: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  buttonPrimaryTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white'
  },
  secondaryButtonTitle: {
    fontSize: 14,
    color: '#0084FF',
  },
  secondaryButton: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 45,
    minWidth: 50,
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 2,
    borderRadius: 6,
    paddingRight: 50,
    paddingLeft: 50,
    paddingTop: 20,
    paddingBottom: 20,
  },
  button: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 45,
    minWidth: 50, 
    backgroundColor: '#0084FF',
    justifyContent: 'center',
    marginBottom: 2,
    marginTop: 10,
    borderRadius: 6,
    paddingRight: 50,
    paddingLeft: 50,
    paddingTop: 20,
    paddingBottom: 20,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
    
  },
  previewContainer: {
    height: 180,
    // borderColor: 'red', borderWidth: 1,
    width: '100%',
    marginTop: 20, marginBottom: 20,
    alignContent: 'center',
    justifyContent: 'center'
  },
  title: {
    color: '#353335',
    fontSize: 22,
    textAlign: 'center',
    marginBottom: 5
  },
  description: {
    marginTop: 5,
    fontSize: 16,
    color: '#9B9A9B',
    textAlign: 'center'
  },
})

const captureDocumentStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center'
  },
  
  buttonContainer: {
    marginBottom: 50
  },
  photoCaptureImage: {
    width: 73, height: 73
  },
  title: {
    color: 'white',
    fontSize: 22,
    textAlign: 'center',
    marginBottom: 10
  },
  instructionText: {
    marginTop: 15,
    fontSize: 16,
    color: 'white',
    textAlign: 'center'
  },
  preview: {
    flex: 1,
    zIndex:1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  content: {
    position: 'absolute',
    width: '100%',
    // height: 200,
    // borderColor: 'red', borderWidth: 1,
    zIndex: 10,
    top: (height / 2) + 20
  },
  maskOutter: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  maskInner: {
    width: 300,
    backgroundColor: 'transparent',
    borderColor: 'rgba(255,255,255,0.3)',
    borderRadius: 6,
    borderWidth: 1.5,
  },
  maskFrame: {
    backgroundColor: 'rgba(1,1,1,0.6)',
  },
  maskRow: {
    width: '100%',
  },
  maskCenter: { flexDirection: 'row' },
})