import React, { PureComponent } from 'react';
import {  
  View,
  Text,
  Platform,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Dimensions
} from 'react-native'


export default class DocumentSubmittedScreen extends PureComponent {
  static navigationOptions = () => ({
    header: null
  });

  render () {
    return(
      <View style={styles.container}>
        <StatusBar
          barStyle={'dark-content'}
          backgroundColor={'white'} />

          <View style={styles.content}>
            <View style={styles.header}>
              <View style={styles.mainImageContainer}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../../Assets/Images/documents_submitted_icon.png')}
                  style={styles.mainImage} />
              </View>
              <Text style={styles.title}>Thanks! We’ll review your documents shortly and notify you.</Text>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={() => {}}
                style={[styles.generalButton, styles.generalButtonPrimary]}>
                <Text style={[styles.generalButtonTitle, styles.generalButtonTitlePrimary]}>Got it!</Text>
              </TouchableOpacity>
            </View>
          </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    paddingLeft: 18, paddingRight: 18,
    paddingTop: 14, paddingBottom: 14
  },
  mainImageContainer: {
    // borderColor: 'red', borderWidth: 1,
    marginBottom: 30,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  header: {
    flex: 1,
    marginTop: 60
  },
  mainImage: {
    width: 182, height: 170
  },
  title: {
    fontSize: 20,
    fontWeight: '500',
    color: '#030303',
    textAlign: 'center'
  },
  buttonContainer: {
    flexDirection:'row',
    alignItems:'flex-end',
    justifyContent:'center',
    marginTop: 5,
    marginBottom: 15,
    // borderColor: 'red', borderWidth: 1,
  },
  generalButtonTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#232228'
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButtonIconPrimary: {
    tintColor: 'white'
  },
  generalButton: {
    alignItems: 'center',
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 50,
    width: 250, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    paddingRight: 15,
    paddingLeft: 10,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
  },
})