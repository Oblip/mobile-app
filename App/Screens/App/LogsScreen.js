import React, { Component } from 'react';
import {  
  View,
  YellowBox,
  StyleSheet,
  SectionList,
  StatusBar,
  RefreshControl,
  ActivityIndicator
} from 'react-native'
import ListItem from '../../Components/ListItem'
import Text from '../../Components/globalTextStyle';
import StellarService from '../../Services/StellarService'
import SecureDataStorage from '../../Libs/SecureDataStorage'
import Toast from 'react-native-simple-toast'
import _ from 'underscore'
import AccountApi from '../../Services/AccountApi'
import DataStorage from '../../Libs/DataStorage'
import moment from 'moment'
import TransactionDetailModal from '../../Components/Modals/TransactionDetailModal'

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

export default class LogsScreen extends Component {
  allLoaded = false

  state = {
    operations: null,
    displayItems: null,
    currentAccount: null,
    isLoading: true,
    isRefreshing: false,
    isFetchingMore: false,
    isTransactionDetailModalOpen: false
  }

  static navigationOptions = ({navigation, screenProps}) => ({
    title: 'Logs',
    headerTitleStyle: {
      flex: 1,
      color: 'black',
      textAlign: 'center',
      alignSelf: 'center'
    },
    headerStyle: {
      backgroundColor: 'white',
      borderBottomColor: 'transparent',
      elevation: 0,
      shadowColor: 'transparent',
      shadowOpacity: 1,
    },
    header: navigation.getParam("header", undefined),
    headerTintColor: 'rgba(0,0,0,1)',
    headerRight: <View></View>
  });

  componentWillMount () {
    this.stellar = new StellarService()

    this.initializeLogs();
  }

  handleLogOperations = (ops, account) => {
    let myCheckingAccountId = account.checking_account_id
    let operations = ops;

    operations.records = this.filterRecords(operations.records, myCheckingAccountId);

    if (operations.records.length == 0) {
      this.allLoaded = true;
      this.setState({isFetchingMore: false,  isRefreshing: false, isLoading: false});
      return; // does not execute the other code
    }

    DataStorage.getIdentities()
      .then(directory => {
        let checkingAccounts = this.extractCheckingAccounts(operations.records)
        let ckIdentities = _.map(directory, (v, i) => { return v.checking_account_id });
        let ckNotInDir = checkingAccounts.filter(x => !ckIdentities.includes(x));

        if (ckNotInDir.length > 0) {

          AccountApi.fetchFederatedAccounts(ckNotInDir)
            .then(identities => {
               // Merge both arrays
              directory = directory.concat(identities)

              // Save to Directory
              DataStorage.saveIdentities(directory)
                .then(saved => {

                   // Update operations records with identity
                   operations.records = this.appendRecordIdentity(operations.records,
                    directory);

                  if(this.state.operations) {
                    operations.records  = this.state.operations.records.concat(operations.records)
                  }
                  
                  // Transfrom data to displayable items
                  let displayItems = this.getDisplayItems(operations.records)


                  this.setState({
                    operations: operations,
                    isFetchingMore: false,
                    isRefreshing: false,
                    displayItems: displayItems
                  })

                }).catch(error => {
                  this.setState({isFetchingMore: false,  isRefreshing: false});
                })
            }).catch(error => {
              // TODO: handle
              this.setState({isFetchingMore: false,  isRefreshing: false});
            })
        } else {
          // nothing new so we don't make a request to api

          // Update operations records with identity
          operations.records = this.appendRecordIdentity(operations.records,
            directory);

          if(this.state.operations) {
            operations.records  = this.state.operations.records.concat(operations.records)
          } 
          
          let displayItems = this.getDisplayItems(operations.records)
          console.log(displayItems)

          this.setState({
            operations: operations,
            isFetchingMore: false,
            isRefreshing: false,
            displayItems: displayItems
          })
        }

      }).catch(error => {
        // Identities are empty so we get fetch all identities and save it.

        let checkingAccounts = this.extractCheckingAccounts(operations.records)
        AccountApi.fetchFederatedAccounts(checkingAccounts)
          .then(identities => {
            // Save to Directory
            DataStorage.saveIdentities(identities)
              .then(saved => {

                // Update operations records with identity
                operations.records = this.appendRecordIdentity(operations.records,
                  identities);

                // Transfrom data to displayable items
                let displayItems = this.getDisplayItems(operations.records)

                this.setState({
                  operations: operations,
                  currentAccount: account,
                  isLoading: false,
                  isRefreshing: false,
                  displayItems: displayItems
                })

              }).catch(error => {
                this.setState({isFetchingMore: false,  isRefreshing: false});
                // TODO: could not save (handle)
                Toast.show('Something went wrong', Toast.SHORT)
              })

          }).catch(error => {
            // TODO: handle
            this.setState({isFetchingMore: false,  isRefreshing: false});
            Toast.show('Something went wrong', Toast.SHORT)
          })
      });
  }

  fetchMoreLogs = () => {
    if(!this.allLoaded) {
      this.setState({isFetchingMore: true})
      this.state.operations.next()
        .then(operations => {
          let account = this.state.currentAccount;
          
          this.handleLogOperations(operations, account)
          
        }).catch(error => {
          this.setState({isFetchingMore: false,  isRefreshing: false});
        })
    } else {
      this.setState({isFetchingMore: false,  isRefreshing: false});
    }
  }

  initializeLogs = () => {
    SecureDataStorage.getAuthAccount()
      .then(authAccount => {

        let account = authAccount.account;
        this.setState({currentAccount: account})

        this.stellar.server.operations()
        .forAccount(account.checking_account_id).limit(15).order('desc')
          .call().then(operations => { 
            console.log(operations)
          this.handleLogOperations(operations, account);

        }).catch(error => {
          console.log('What?')
          Toast.show('Something went wrong', Toast.SHORT)
        })
    });
  }

  extractCheckingAccounts = (records) => {
    return _.uniq(_.map(records, (v, i) => {
      if(!v.is_sender) {
        return v.from;
      } else {
        return v.to;
      }
    }));
  }

  // getCurrentAccountName = (currentAccount) => {
  //   switch(currentAccount.type) {
  //     case 'personal':
  //       return currentAccount.first_name + ' ' + currentAccount.last_name
  //     case 'business':
  //       return currentAccount.name
  //   }
  // }

  getDisplayItems = (records) => {
    let data = []
    let groups = []

    let displaySet = _.map(records, (v, i) => {
      let group = this.getRecordGroup(v);
      groups.push(group);

      let displayData = {
        title: v.identity ? v.identity.name : 'Unknown',
        subtitle: moment(v.created_at).format('LT'),
        rightTitle: v.amount,
        rightSubtitle: v.asset_code,
        group: group,
        extraData: v
      }

      return displayData;
    });

    groups = _.uniq(groups);
    
    _.each(groups, (v, i) => {
      let setInGroup = _.filter(displaySet, (r, i1) => {
        return r.group == v;
      })

      let set = { title: v, data: setInGroup }
      data.push(set)
    });

    return data;
  }


  getRecordGroup = (record) => {
    let obj = moment(record.created_at)

    let group = obj.calendar(null, {
      sameDay: '[Today]',
      nextDay: '[Tomorrow]',
      nextWeek: 'dddd',
      lastDay: '[Yesterday]',
      lastWeek: 'dddd',
      sameElse: 'DD MMMM, YYYY'
    });

    return group;
  }

  appendRecordIdentity = (records, identities) => {
    return _.map(records, (v, i) => {
      if(!v.is_sender) {
        let identity = _.findWhere(identities,
          {checking_account_id: v.from});

        v['identity'] = identity;
      } else {
        let identity = _.findWhere(identities,
          {checking_account_id: v.to});

        v['identity'] = identity;
      }

      return v;
    })
  }

  filterRecords = (records, myCheckingAccountId) => {
    let data = _.filter(records, (v, i) => {
      return v.type == 'payment'
    });

    data = _.map(data, (v, i) => {
      let record = v;
      record['is_sender'] = false
      if(record.from == myCheckingAccountId) {
        record['is_sender'] = true;
        record['amount'] = '-' + record.amount;
      } else {
        record['amount'] = '+' + record.amount;
      } 

      return record;
    })

    return data;
  }

  onRefresh = () => {
    this.setState({
      isRefreshing: true,
      operations: null,
    });

    this.initializeLogs ();

  }

  renderList = () => {
    return(
      <SectionList
        sections={this.state.displayItems || []}
        contentContainerStyle={{paddingBottom: 10}}
        keyExtractor={(item, index) => item + index}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isRefreshing}
            // progressViewOffset={HEADER_MAX_HEIGHT}
            onRefresh={this.onRefresh}
          />
        }
        onScroll={(e) => {
          if(this.isCloseToBottom(e) && !this.state.isFetchingMore) {
            // Get more
            this.fetchMoreLogs();
          }
        }}

        ListFooterComponent={() => {
          if(this.state.isFetchingMore) {
            return(
              <View style={{
                marginTop: 20,
                alignContent: 'center',
                height: 30
              }}>
                  <ActivityIndicator style={{marginTop: 3}} size="small" color="#0084FF" />
              </View>
            )
          } else {
            return(<View></View>)
          }
        }}
        renderSectionHeader={({section}) => (
          <View style={listStyles.sectionHeader}>
            <Text style={listStyles.sectionTitle} > {section.title} </Text>
          </View>
        )}
        ListEmptyComponent={() => {
          if(!this.state.isLoading) {
            return(
              <View style={listStyles.emptyContainer}>
                <View style={listStyles.emptyBox}>
                  <Text style={listStyles.emptyTitle}>Nothing Here!</Text>
                  <Text style={listStyles.emptyDescription}>Your future transaction logs will appear here.</Text>
                </View>
              </View>
            )
          } else {
            return (
              <View style={styles.loadingContainer}>
                <ActivityIndicator size="large" color="#0084FF" />
              </View>
            )
          }
        }}
        renderItem={({item, index, section}) => (
          <ListItem 
            data={item}
            style={index == 0 ? {marginTop: 0} : {}}
            rightTitleStyle={item.extraData.is_sender && {color: '#FC0935' }}
            onItemPressed={() => {
              // alert("Tap!")
              // DataStorage.removeIdentities()
              // this.onTransactionDetailModalOpen()
            }}
          />
        )}
      />
    )
  }

  isCloseToBottom = (e) => {
    const paddingToBottom = 20;
    return e.nativeEvent.layoutMeasurement.height + e.nativeEvent.contentOffset.y >=
    e.nativeEvent.contentSize.height - paddingToBottom;
  };


  
  onTransactionDetailModalOpen = () => {
    this.props.navigation.setParams({ header: null });

    this.setState({
      isTransactionDetailModalOpen: true,
      statusBarBackgroundColor: 'white',
      statusBarStyle: 'dark-content'
    });
  }

  onTransactionDetailModalClose = () => {
    this.props.navigation.setParams({ header: undefined });
    this.setState({
      isTransactionDetailModalOpen: false,
      statusBarBackgroundColor: 'transparent',
      statusBarStyle: 'light-content'
    });
  }


  render () {
    return (
      <View style={styles.container}>
         <TransactionDetailModal
          isOpen={this.state.isTransactionDetailModalOpen}  
          onClosed={this.onTransactionDetailModalClose} />

        <StatusBar
          backgroundColor={'white'}
          translucent={false}
          barStyle={'dark-content'}
        />

        { this.renderList ()}

      </View>
    )
  }
}

const listStyles = StyleSheet.create({
  sectionHeader: {
    paddingRight: 16,
    paddingLeft: 16,
    paddingTop: 8,
    paddingBottom: 8,
    height: 45,
    justifyContent: 'center',
    backgroundColor: '#F0F3F7',
    // borderColor: 'green', borderWidth: 1
  },
  sectionTitle: {
    fontWeight: '500',
    color: '#ABAFB6',
    fontSize: 12
  },
  emptyTitle: {
    fontSize: 16,
    color: 'black'
  },
  emptyDescription: {
    fontSize: 12,
    color: 'rgba(0,0,0,0.50)'
  },
  emptyBox: {
    backgroundColor: 'white',
    marginBottom: 10,
    width: '100%',
    minHeight: 150,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  emptyContainer: {
    flex: 1,
    marginTop: 30,
   
  }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0F3F7',
  },
  loadingContainer: {
    flex: 1,
    height: 300,
    alignContent: 'center',
    justifyContent: 'center'
  }
})