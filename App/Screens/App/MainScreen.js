import React, { Component } from 'react';
import {  
  View,
  Image,
  Vibration,
  TouchableOpacity,
  YellowBox,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  Alert
} from 'react-native';
import Text from '../../Components/globalTextStyle';

import { RNCamera } from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import MyQrCodeModal from '../../Components/Modals/MyQrCodeModal'
import MyContactsModal from '../../Components/Modals/MyContactsModal'
import PartnerModal from '../../Components/Modals/PartnerModal';
import TopupCashoutModal from '../../Components/Modals/TopupCashoutModal'

import BottomSheet from '../../Components/BottomSheet'
import parseUri from '../../Libs/ParseUri';
import Toast from 'react-native-simple-toast'
import SecureDataStorage from '../../Libs/SecureDataStorage';
import PushNotification from '../../Libs/PushNotification'
import ClientApi from '../../Services/ClientApi'
import FayeService from '../../Services/FayeService'

// TODO: check these warnings and fix it later.
YellowBox.ignoreWarnings(['Warning: requires main queue setup', 'Module RNOS']);
YellowBox.ignoreWarnings(['Warning: requires main queue setup', 'Module RCTSplashScreen']);
YellowBox.ignoreWarnings(['Setting a timer']);
YellowBox.ignoreWarnings(['Require cycle:']);

export default class MainScreen extends Component {
  
  constructor (props) {
    super(props)

    // this.camOn = true

    this.state = {
      isMyQrCodeModalOpen: false,
      isMyContactsModalOpen: false,
      isPartnerModalOpen: false,
      isTopupCashoutModalOpen: false,
      isBottomSheetOpen: false,
      topupCashoutType: null,
      statusBarStyle: 'light-content',
      statusBarBackgroundColor: 'transparent',
      shouldScanQrCode: true, 
      scannedUsername: null
    }
  }

  static navigationOptions = () => ({
    header: null
  });

  componentDidMount () {
    this._navDidFocusListener = this.props.navigation.addListener('didFocus', () => {
      this.setState({shouldScanQrCode: true})
    })

    this._navDidBlurListener = this.props.navigation.addListener('didBlur', () => {
      this.setState({shouldScanQrCode: false})
    });

    // Temporary: should remove later
    // this.copySignerKey();

    // Get the token and listen to notifications
    PushNotification.register((token) => {
      console.log('TOken: ')
      console.log(token)
      // this.updateFCM(token); // TODO: Remove later
      PushNotification.listenToNotifications();
    });

    PushNotification.onNotificationOpened(this.handleOnNotificationOpened)
  }

  handleOnNotificationOpened = (notification, didStartupOpen) => {
    console.log('Notification Opened');
    console.log(notification);
    // let extras = notification._data && JSON.parse(notification._data.extras)
    let extras = notification._data
    console.log('Extras: ')
    console.log(extras)
    
    switch(extras.type) {
      case 'type_transaction': 
        this.props.navigation.navigate('LogsScreen')
        break;
      case 'type_bank_account':
        this.props.navigation.navigate('AccountScreen');
        break;
    }
  }


  componentWillUnmount () {
    this._navDidFocusListener.remove();
    this._navDidBlurListener.remove();
    PushNotification.unlistenToNotifications();

    // FayeService.getClient().unsubscribe('/hello');
  }

  // TODO: remove (not using anymore)
  removeCheckingAccounts = () => {
    SecureDataStorage.removeCheckingAccounts()
      .then(res => {
        alert('Removed!')
      })
  }

  // Temporary: should remove ASAP.
  updateFCM = (token) => {
    ClientApi.updateFCM(token)
      .then(res => {
        // Do nothing
        // console.log(res)
      }).catch(error => {
        // DO nothing
        console.log(error)
      })
  }

  // Temporary: should remove later
  copySignerKey = () => {
    SecureDataStorage.getCheckingAccounts()
      .then(cks => {
        // DO Nothing
      }).catch(error => {
       SecureDataStorage.getSignerSeed()
        .then(seed => {

          SecureDataStorage.getAuthAccount()
            .then(authAccount => {
              let account = authAccount.account;

              let checkingAccounts = {}
              checkingAccounts[account.checking_account_id] = seed

              // Saving current checking account in other item
              SecureDataStorage.setCheckingAccounts(checkingAccounts)
                .then(done  => {
                  // DONE :D 
                }).catch(ckError => {
                  // This should not happen
                })
            }).catch(authError => {
              console.log(authError);
              // This shouldn't ever happen!
            })

        }).catch(seedError => {
          console.log(seedError);
          // We didn't get it?
        })

      })
  }

  onBarCodeRead = (e) => {
    this.setState({shouldScanQrCode: false})

    try {
      let data = parseUri(e.data);
      let username = data.path.split('/')[1]
      let isPartner = data.queryKey.partner != undefined ? data.queryKey.partner : false;
      const action = data.queryKey.action
      const type = data.queryKey.type
      const amount = data.queryKey.amount;
      const asset = data.queryKey.asset
      const identifier = data.queryKey.identifier

      if(data.authority != 'oblip.me') { throw new Error('Unable to read'); }
      if(!username) { throw new Error('Unable to read'); }

      Vibration.vibrate(500);

      if(action && action == 'request_review') {
        let extraData = {
          isAccount: true,
          action: action,
          username: username,
          partner: true,
          type: type,
          amount: amount,
          asset: asset,
          identifier: identifier
        }

        this.props.navigation.navigate('TransactionScreen', { extraData: extraData})
      } else if (isPartner) {
          // this is an agent so we show bottom sheet
          this.setState({
            isBottomSheetOpen: true,
            scannedUsername: username
          });
      } else if (!isPartner) {
        let extraData = {
          isAccount: true,
          username: username
        }

        this.props.navigation.navigate('TransactionScreen', { extraData: extraData})
      }
    }


    catch(exception) {
      this.setState({shouldScanQrCode: true})
      Toast.show('Unable to read', Toast.SHORT)
    }
  }

  onMyContactsModalOpen = () => {
    Alert.alert('Not available', 'Sorry this feature is not available right now.')
    // this.setState({
    //   isMyContactsModalOpen: true,
    //   statusBarBackgroundColor: 'white',
    //   statusBarStyle: 'dark-content'
    // })
  }

  onMyContactsModalClose = () => {
    this.setState({
      isMyContactsModalOpen: false,
      statusBarBackgroundColor: 'transparent',
      statusBarStyle: 'light-content'
    })
  }

  onTopupCashoutModalOpen = (type) => {
    this.setState({
      isBottomSheetOpen: false,
      shouldScanQrCode: false,
      isTopupCashoutModalOpen: true,
      topupCashoutType: type,
    })
  }

  onTopupCashoutModalClose = () => {
    this.setState({
      shouldScanQrCode: true,
      isTopupCashoutModalOpen: false,
    })
  }

  onMyQrCodeModalOpen = () => {
    this.setState({
      isMyQrCodeModalOpen: true,
      statusBarBackgroundColor: 'white',
      statusBarStyle: 'dark-content'
    })
  }

  onMyQrCodeModalClose = () => {
   
    this.setState({
      isMyQrCodeModalOpen: false, 
      statusBarBackgroundColor: 'transparent',
      statusBarStyle: 'light-content'
    })
  }

  onPartnerModalOpen = () => {
    this.setState({
      isPartnerModalOpen: true,
      statusBarBackgroundColor: 'white',
      statusBarStyle: 'dark-content'
    });
  }

  onPartnerModalClose = () => {
    this.setState({
      isPartnerModalOpen: false,
      statusBarBackgroundColor: 'transparent',
      statusBarStyle: 'light-content'
    });
  }

  onContactItemPress = (data) => {
    this.props.navigation.navigate('TransactionScreen', { extraData: data.extraData })
  }

  onBottomSheetClose = () => {
    this.setState({
      isBottomSheetOpen: false,
      statusBarBackgroundColor: 'transparent',
      statusBarStyle: 'light-content'
    });

    if (this.state.isTopupCashoutModalOpen) {
      this.setState({ shouldScanQrCode: false})
    } else {
      this.setState({ shouldScanQrCode: true})
    }
  }

  getBottomSheetData =() => {
    return([
      {
        title: 'Make a payment',
        icon: require('../../Assets/Images/send_money_icon.png'),
        action: () => {
          let data = {
            username: this.state.scannedUsername,
            isAccount: true,
            type: 'payment'
          }

          this.props.navigation.navigate('TransactionScreen', {extraData: data})
        }
      },
      {
        title: 'Request a topup',
        icon: require('../../Assets/Images/topup_icon.png'),
        action: () => {
          this.onTopupCashoutModalOpen('topup')
        }
      },
      {
        title: 'Request a cash out',
        icon: require('../../Assets/Images/withdraw_icon.png'),
        action: () => {
          Alert.alert('Not available', 'Sorry this feature is not available right now.')
          // this.onTopupCashoutModalOpen('cashout')
        }
      }
    ])
  }



  render () {
    const { shouldScanQrCode } = this.state;
    
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={this.state.statusBarBackgroundColor}
          translucent={true}
          barStyle={this.state.statusBarStyle}
        />

        <BottomSheet
          data={this.getBottomSheetData()}
          onClosed={this.onBottomSheetClose}
          isOpen={this.state.isBottomSheetOpen} />

        <TopupCashoutModal
          isOpen={this.state.isTopupCashoutModalOpen}
          type={this.state.topupCashoutType}
          scannedUsername={this.state.scannedUsername}
          onClosed={this.onTopupCashoutModalClose} />

        <MyQrCodeModal
          style={{zIndex: 50}}
          onClosed={this.onMyQrCodeModalClose}
          isOpen={this.state.isMyQrCodeModalOpen} />

        <MyContactsModal
          onClosed={this.onMyContactsModalClose}
          onContactItemPress={this.onContactItemPress}
          isOpen={this.state.isMyContactsModalOpen} />

        <PartnerModal
           onClosed={this.onPartnerModalClose}
           isOpen={this.state.isPartnerModalOpen}
        />
        

        { true && <RNCamera
          style = {styles.preview}
          type={RNCamera.Constants.Type.back}
          onBarCodeRead={shouldScanQrCode ? this.onBarCodeRead : null}
          mirrorImage={true}
          captureAudio={false}
          fixOrientation={true}
          autoFocus={RNCamera.Constants.AutoFocus.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}>

          <SafeAreaView style={[styles.screenContent, {}]}>
            <TouchableOpacity
              style={styles.personCircleIconContainer}
              onPress={() => {
                this.props.navigation.navigate('AccountScreen')
              }}>
                <Image
                  source={require('../../Assets/Images/person_circle_icon.png')}
                  style={styles.personCircleIcon}
                  resizeMode={'cover'} />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.agentLocationIconContainer}
              onPress={() => this.onPartnerModalOpen()}
              >
                <Image
                  source={require('../../Assets/Images/agent_location_icon.png')}
                  style={styles.agentLocationIcon}
                  resizeMode={'cover'} />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.logIconContainer}
              onPress={() => {
                this.props.navigation.navigate('LogsScreen')
              }}>
                <Image
                  source={require('../../Assets/Images/log_icon.png')}
                  style={styles.logIcon}
                  resizeMode={'cover'} />
            </TouchableOpacity>
            
            <View style={styles.bottomNavContainer}>
              <TouchableOpacity
                style={styles.bottomNavItem}
                onPress={() => this.onMyQrCodeModalOpen()}>
                  <Image
                    source={require('../../Assets/Images/qr_icon_small.png')}
                    style={[styles.bottomNavItemIcon, styles.qrIconSmall]}
                    resizeMode={'cover'}
                  />
                  <Text style={styles.bottomNavItemText}>My QR Code</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.bottomNavItem}
                  onPress={() => this.onMyContactsModalOpen()}>
                    <Image
                      source={require('../../Assets/Images/contacts_icon_small.png')}
                      style={[styles.bottomNavItemIcon, styles.contactsIconSmall]}
                      resizeMode={'contain'}
                    />
                    <Text style={styles.bottomNavItemText}>My Contacts</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.textHint}>
              <Text style={styles.textHintText}>Position QR code within this frame</Text>
            </View>

          </SafeAreaView>

          

          <BarcodeMask width={'75%'} height={'37.5%'} backgroundColor={'rgba(0,132,255,0.528)'} />
           
        </RNCamera> }

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center'
  },
  screenContent: {
    position: 'absolute',
    zIndex: 1,
    width: '100%', height: '100%',
  },
  preview: {
    flex: 1,
    zIndex:1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  bottomNavContainer: {
    backgroundColor: 'white',
    width: '100%',
    position: 'absolute',
    height: 60,
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },

  bottomNavItem: {
    flexDirection: 'row',
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },

  bottomNavItemIcon: {
    marginRight: 14,
    // width: 24, height: 24,
    // borderColor: 'red', borderWidth: 1,
    tintColor: 'black',
    // tintColor: 'rgba(0,0,0,0.76)'
  },
  bottomNavItemText : {
    fontSize: 16,
    fontWeight: 'bold',
    //fontFamily: "sans-serif",
    color: 'black'
  },
  boldText: {

  },
  qrIconSmall: {
    width: 24, height: 24
  },
  contactsIconSmall: {
    width: 28, height: 28
  },
  personCircleIcon: {
    // width: '100%', height: '100%'
    width: 45, height: 45
  },

  personCircleIconContainer: {
    position: 'absolute',
    left: 25,
    width: 50, height: 50,
    // backgroundColor: 'rgba(9,51,88,0.2)',
    // borderRadius: 6,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    top: "12%",
    zIndex: 1
  },
  logIcon: {
    // width: '100%', height: '100%'
    width: 45, height: 45
  },
  logIconContainer: {
    position: 'absolute',
    right: 25,
    // width: 45, height: 45,
    width: 50, height: 50,
    // backgroundColor: 'rgba(9,51,88,0.2)',
    // borderRadius: 6,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    top: "12%"
  },
  agentLocationIcon: {
    width: 45, height: 45
  },
  agentLocationIconContainer: {
    position: 'absolute',
    left: "44.84%",  
    width: 50, height: 50, 
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    top: "5%"
  },
  maskOutter: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  maskInner: {
    width: 300,
    backgroundColor: 'transparent',
    // borderColor: '#0084FF',
    borderColor: 'white',
    borderWidth: 2,
  },
  maskFrame: {
    // backgroundColor: 'rgba(1,1,1,0.6)',
    // backgroundColor: 'rgba(31,147,252, 0.7)',
    // backgroundColor: 'rgba(9,51,88,0.7)'
    backgroundColor: 'rgba(0,132,255,0.528)'
  },
  maskRow: {
    width: '100%',
  },
  maskCenter: { flexDirection: 'row' },
  textHint: {
    // zIndex: 2000,
    margin: 0,
    position: "absolute",
    bottom: "17%",
    left: "16%", 
    right: "16%", 
    backgroundColor: 'rgba(255, 255, 255, 0.790874)', 
    padding: 14,
    borderRadius: 20.5
  },
  textHintText: {
    textAlign: 'center',
    color: "#000000",
    // lineHeight:16,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    fontSize: 12,
    fontWeight: 'bold',
    // fontFamily: 'Calibre',
    letterSpacing: 0.5,
    margin: 0,
    paddingLeft: 5,
    paddingRight: 5
  }
})