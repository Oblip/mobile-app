import React, { Component } from 'react';
import {  
  View,
  YellowBox,
  Vibration,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Platform
} from 'react-native'
import Text from '../../Components/globalTextStyle';

import Dash from 'react-native-dash';
import { NavigationActions } from 'react-navigation'

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({routeName: 'MainScreen'})
  ]
})

const initialAmountFontSize = 68.4;
const initialCentFontSize = 20.52;
const buttonElevation = 2;
const transactionExtraHeight = 80;

const successColor = '#0084FF';
const failedColor = '#FF1744';

export default class TransactionResultScreen extends Component {
  constructor (props) {
    super(props);

    try {
      this.extraData  = this.props.navigation.state.params.extraData
    } catch(e) {

    }

  }

  static navigationOptions = () => ({
    header: null
  });

  failedTxView = () => {
    return (
      <View style={[styles.resultContainer, {}]}>
        <View style={[styles.txResultContainer, { backgroundColor: failedColor }]}>
          <Image 
            source={require('../../Assets/Images/failed_icon.png')}
            resizeMode={'center'}
            style={styles.failedIcon} />
        </View>
        <View style={styles.resultWrapper}>
          <View style={[styles.transactionInfoContainer, { borderBottomWidth: 0, borderBottomColor: 'transparent'}]}>
            <View style={styles.resultContentPadding}>
              <Text style={[styles.txTypeText, {color: failedColor}]}>Transaction Failed</Text>
              <Text style={styles.message}>
                {this.extraData && this.extraData.errorMessage}
              </Text>
            </View>
          </View>
        </View>
      </View>
    )
  }

  successTxView = () => {
    let amountData = this.extraData.amount.split('.')
    let amount = amountData[0];
    let cent = amountData[1]
    cent = cent == "0" ? '' : cent

    let amountFontSize = initialAmountFontSize - (6 * (amount.length - 1))
    let centFontSize = initialCentFontSize - (3 * (amount.length - 1))

    return (
      <View style={styles.resultContainer}>
        <View style={styles.txResultContainer}>
          <Image 
            source={require('../../Assets/Images/done_icon.png')}
            resizeMode={'center'}
            style={styles.successIcon} />
        </View>
        <View style={[styles.circle, styles.circleLeft]} />
        <View style={[styles.circle, styles.circleRight]} />
        <View style={styles.resultWrapper}>
          <View style={styles.transactionInfoContainer}>
            <View style={styles.resultContentPadding}>
              <Text style={styles.txTypeText}>Successful Transaction</Text>
              <View style={styles.amountContainer}>
                <View style={styles.amountWrapper}>
                  {/* <Text style={styles.amountCurrencySymbol}></Text> */}
                  <Text style={[styles.amountText, {fontSize: amountFontSize} ]}>{amount}</Text>
                  <Text style={[styles.amountCentsText, {fontSize: centFontSize}]}>{cent}</Text>
                </View>
              </View>
              {/* <Text style={styles.assetTitle}>BZD · Belize Dollars</Text> */}
              <Text style={styles.assetTitle}>{this.extraData && this.extraData.asset}</Text>
            </View>
          </View>
          <View>
            <View style={[styles.recipientContainer, styles.resultContentPadding]}>
              <View>
                <Image 
                  source={require('../../Assets/Images/pineapple_icon.png')}
                  resizeMode={'cover'}
                  style={styles.recipientAvatar}
                  />
              </View>
              <View style={styles.recipientInfoContainer}>
                <Text style={styles.recipientName}>{this.extraData && this.extraData.name}</Text>
                {/* <Text style={styles.recipientType} >Personal</Text> */}
              </View>
            </View>
          </View>
          <View style={{position: 'relative', zIndex: 1}}>
            <Dash 
              dashColor={'#eeeeee'}
              style={{width:'99%', height:1}} dashThickness={1} />
          </View>
          <View style={styles.transactionExtraContainer}>
            <View style={[styles.resultContentPadding, {paddingBottom: 10}]}>
              <View style={styles.extraKeyPair}>
                <Text style={styles.extraKey}>Date & Time</Text>
                <Text style={styles.extraValue}>{this.extraData && this.extraData.date}</Text>
              </View>
              <View style={styles.extraKeyPair}>
                <Text style={styles.extraKey}>ID </Text>
    <Text style={styles.extraValue}>{this.extraData && this.extraData.transactionId}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  render () {

    return (
      <View style={[styles.container]}>
        <StatusBar
          backgroundColor={'transparent'}
          translucent={true}
          barStyle={'light-content'}
        />

        <View style={styles.content}>
          { (this.extraData && this.extraData.isSuccess) ? this.successTxView () : this.failedTxView() }
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                this.props.navigation.dispatch(resetAction)
              }}>
                <Text style={styles.buttonText}>Done</Text>
              </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    paddingLeft: 40,
    paddingRight: 40,
    backgroundColor: successColor
  },

  message: {
    paddingTop: 30, paddingBottom: 30,
    textAlign: 'center',
    color: 'rgba(0,0,0,0.5)'
  },

  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },

  buttonContainer: {
    // borderColor: 'red', borderWidth: 1,
    height: 100,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  buttonText: {
    fontSize: 14,
    fontWeight: '500',
    textAlign: 'center',
    color: 'white'
  },
  button: {
    height: 46,
    width: '100%',
    borderRadius: 23,
    borderColor: 'white',
    borderWidth: 2,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  circle: {
    width: 14.4, height: 14.4,
    backgroundColor: '#0084FF',
    // backgroundColor: 'red',
    borderRadius: 7
  },
  circleRight: {
    position: 'absolute',
    zIndex: 2,
    right: 1,
    bottom: transactionExtraHeight - 7.2
  },

  circleLeft: {
    position: 'absolute',
    zIndex: 20,
    left: 1,
    bottom: transactionExtraHeight - 7.2
  },

  extraKeyPair: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  extraKey: {
    fontSize: 10.8,
    fontWeight: '500',
    color: 'rgba(3,3,3,0.5)'
  },
  extraValue: {
    fontSize: 10,
    color: 'rgba(3,3,3,0.5)'
  },
  transactionExtraContainer: {
    backgroundColor: '#F7F9FB',
    borderBottomLeftRadius: 3.6,
    borderBottomRightRadius: 3.6,
    alignContent: 'center',
    height: transactionExtraHeight
  },
  recipientAvatar: {
    width: 46, height: 46
  },

  recipientContainer: {
    flexDirection: 'row',
  },

  recipientType: {
    fontSize: 9,
    color: 'rgba(3,3,3,0.5)'
  },

 
  recipientName: {
    fontSize: 14,
    fontWeight: '500',
    color: 'black'
  },

  recipientInfoContainer: {
    paddingLeft: 16,
    justifyContent: 'center'
  },

  successIcon: {
    width: 43.46,
    height: 42.92
  },

  failedIcon: {
    width: 55,
    height: 55
  },

  txResultContainer: {
    borderColor: 'white', borderWidth: 4.32,
    backgroundColor: successColor,
    width: 56, height: 56,
    borderRadius: 28,
    alignSelf: 'center',
    position: 'absolute',
    // top: -28,
    top: 5,
    zIndex: 100,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  resultContentPadding: {
    paddingLeft: 30, paddingRight: 30,
    paddingTop: 20, paddingBottom: 20
  },

  txTypeText: {
    fontSize: 14.4,
    color: '#0084FF',
    fontWeight: '500',
    textAlign: 'center'
  },

  resultWrapper: {
    backgroundColor: 'white',
    borderRadius: 4,
  },
  
  resultContainer: {
    width: '100%',
    marginTop: 100,
    paddingTop: 31.5,
    paddingLeft: 8, paddingRight: 8,
    // borderColor: 'blue', borderWidth: 1
    // height: 381,
    // paddingLeft: 30, paddingRight: 30
  },
  assetTitle: {
    fontSize: 18,
    fontWeight: '500',
    // color: 'rgba(0,0,0,0.5)',
    color: 'black',
    textAlign: 'center',
    marginTop: 2
  },
  transactionInfoContainer: {
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    paddingTop: 20,
    // paddingBottom: 20,
    borderBottomColor: '#EEEEEE', borderBottomWidth: 1,
  },
  amountContainer: {
    marginTop: 10,
    // marginBottom: 10,
    // height: 120,
    // borderColor: 'purple', borderWidth: 2,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  amountWrapper: {
    flexDirection: 'row',
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  amountCurrencySymbol: {
    fontSize: 20.52,
    marginTop: 15,
    color: 'black',
    fontWeight: '300',
    alignSelf: 'flex-start',
  },

  amountCentsText: {
    fontSize: initialCentFontSize,
    color: 'black',
    fontWeight: '300',
    alignSelf: 'flex-start'
  },
  amountText: {
    fontSize: initialAmountFontSize,
    color: 'black',
  },
})