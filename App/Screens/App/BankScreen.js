import React, { Component } from "react";

import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  StatusBar,
  Animated,
  Image,
  Alert,
  Platform,
  SectionList,
  ActivityIndicator,
  RefreshControl
} from "react-native";

import Text from '../../Components/globalTextStyle';
// import BlockchainManagementApi from '../../Services/BlockchainManagementApi'
import BankManagementApi from '../../Services/BankManagementApi'
import SecureDataStorage from '../../Libs/SecureDataStorage'
import Toast from 'react-native-simple-toast'
import ListItem from '../../Components/ListItem'
import moment from 'moment'
import _ from 'underscore'
import BankingRequestDetailsModal from "../../Components/Modals/BankingRequestDetailsModal";


export default class Accountscreen extends Component {
  
  _isMounted = false;
  defaultLimit = 15;
  nextOffset = 0;
  allLoaded = false;

  constructor(props) {
    super(props);
  try{
    this.state = {
      scrollY: new Animated.Value(0),
      bankAccountData: this.props.navigation.state.params.bankAccountData,
      accountData: this.props.navigation.state.params.accountData,
      isRefreshing: false,
      bankRequests: null,
      requestDetailsModal: false,
      requestSelected: null,
      formattedRequests: [],
      isLoading: true,
      isFetchingMore: false
    }
  } catch (e) {
    Toast.show('Something went wrong. Try again!')
    this.props.navigation.navigate('AccountScreen'); 
  }

}

  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam("title", ""),
    headerTitleStyle: {
      flex: 1,
      color: "black",
      textAlign: "center",
      // opacity: navigation.getParam("headerTextOpacity", 0)
    },
    headerStyle: {
      backgroundColor: "white",
      borderBottomColor: "transparent",
      elevation: 0,
      shadowColor: "transparent",
      shadowOpacity: 1
    },
    header: navigation.getParam("header", undefined),
    headerTintColor: "rgba(0,0,0,1)",
    headerRight: (
      <TouchableOpacity style={headerStyles.rightButton} onPress={() => { 
        Alert.alert('Not available', 'Sorry this feature is not available right now.') }}>
        <Image
        resizeMode={"contain"}
        source={require("../../Assets/Images/setting_icon_black.png")}
        style={[Platform.OS == 'ios' ? styles.iosIcon : styles.androidIcon]}
        />
      </TouchableOpacity>
    )
  });

  componentWillMount() { 
    
    this._isMounted = true
    SecureDataStorage.getAuthAccount()
      .then(res => {
        this.setState({accountData: res.account});
      })

    this.getBankingRequests()
    
    this.props.navigation.setParams({
      headerTextOpacity: this.state.scrollY.interpolate({
        inputRange: [90, 150],
        outputRange: [0, 1],
        extrapolate: "clamp",
      }),
      title: this.state.bankAccountData.registered_bank.name
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onRefresh = () => {
    this.setState({
      isRefreshing: true,
      isLoading: true,
      bankRequests: null,
      isFetchingMore: false,
      formattedRequests: []
    });

    this.allLoaded = false;
    this.nextOffset = 0;

    this.getBankingRequests();
  }
  
  renderHeader = () => {
    let statusIcon = this.getStatusIcon()
    let verified = this.isVerified()
    let buttonBlock = verified ? this.renderButtons() : undefined

    return (
      <View style={headerStyles.header}>
        <View
          style={[blockStyles.blockIconContainer, blockStyles.centerContent]}>
          <Image
            resizeMode={"contain"}
            source={require("../../Assets/Images/bank_icon.png")}
            style={blockStyles.bankIcon}
          />
        </View>
        <View>
          <Image
            resizeMode={"contain"}
            source={statusIcon}
            style={blockStyles.statusIconHeader}
          />
        </View>
        <Text style={headerStyles.headerTitle}>{"Acct#" + this.state.bankAccountData.account_number}</Text>
        {buttonBlock}      
      </View>
    );
  };

  getStatusIcon(){
    let bank = this.state.bankAccountData
    let icon = undefined
    switch(bank.status.code) {
      case "verified":
        icon = require("../../Assets/Images/verified_blue_icon.png") 
        break;
      case "unable_to_verify":
        icon = require("../../Assets/Images/unable_to_verify_status_icon.png")
        break;
      default:
        icon = require("../../Assets/Images/unconfirmed_status_icon.png")
    }
    return icon
  }

  isVerified(){
    let bank = this.state.bankAccountData
    return (bank.status.code == "verified")
  }

  renderButtons = () => {
    
    return(
      <View style={[styles.buttonContainer]}>
        <TouchableOpacity
          onPress={() => { this.props.navigation.navigate('TopupWithdrawScreen', {
              extraData: this.state.bankAccountData,
              type: "topup"
            })}
          }
          style={[styles.button, styles.generalButtonPrimary]}>
          <Text style={styles.buttonPrimaryTitle}>Topup</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => { 
            Alert.alert('Not available', 'Sorry this feature is not available right now.')
            // this.props.navigation.navigate('TopupWithdrawScreen', {
            //   extraData: this.state.bankAccountData,
            //   type: "withdraw"
            // });
          }}
          style={[styles.button, styles.generalButtonPrimary, styles.payButton]}>
          <Text style={styles.buttonPrimaryTitle}>Cashout</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderStatusBlock = () => {
    let bank = this.state.bankAccountData
    let icon = undefined
    let mainText = undefined
    let subText = undefined

    switch(bank.status.code) {
      case "verified":
        icon = require("../../Assets/Images/verified_blue_icon.png") 
        mainText = "Verified"
        subText = "Your banking requests will appear here."
        break;
      case "unable_to_verify":
        icon = require("../../Assets/Images/unable_to_verify_status_icon.png")
        mainText = "Unable to Verify"
        subText = bank.status.message
        break;
      default:
        icon = require("../../Assets/Images/unconfirmed_status_icon.png")
        mainText = "Processing"
        subText = "It usually takes 6 - 8 hours to process."
    }
    return (
      <View style={[blockStyles.centerBlock]}>
        <View>
          <View
          style={styles.centerContent}>
          <Image
            resizeMode={"contain"}
            source={icon}
            style={blockStyles.blockIcon}
          />
          </View>
        </View>
      <Text style={headerStyles.headerTitle}>{mainText}</Text>
      <Text style={headerStyles.headerSubtitle}>{subText}</Text>
    </View>
    )
  }

  getBankingRequests = () => {
    if(!this.allLoaded) {
      const bankID = this.state.bankAccountData.id;

      if(this.state.bankRequests != null && this.state.bankRequests.length > 0) {
        this.setState({isFetchingMore: true})
      }

      BankManagementApi.fetchBankingRequests(bankID, this.nextOffset)
        .then(data => {

          if(data.length == 0) {
            console.log('No more!')
            this.allLoaded = true;
            this.setState({isFetchingMore: false, isLoading: false})
            return;
          }
          
            let requests = this.state.bankRequests || []
            requests = requests.concat(data)

            let items = this.getDisplayItems(requests);
            
            this.setState({
              bankRequests: requests,
              isFetchingMore: false,
              isRefreshing: false,
              formattedRequests: items,
              isLoading: false
            });

          this.nextOffset += data.length;
        })
        .catch(error => {
          console.log(error)   
          this.setState({
            isLoading: false,
            isFetchingMore: false,
            isRefreshing: false,
          })
          Toast.show('Something went wrong', Toast.LONG);
        })
    } else {
      this.setState({isFetchingMore: false})
    }
  }


  getDisplayItems = (records) => {
    let data = []
    let groups = []

    let displaySet = _.map(records, (v, i) => {
      let group = this.getRecordGroup(v);
      groups.push(group);

      let sign = v.type == 'deposit' ? '+' : '-';
      console.log(v.created_at)
      
      let displayData = {
        title: v.ref_no,
        subtitle: moment.utc(v.created_at).local().format('LT'),
        rightTitle: `${sign}${v.amount}`,
        rightSubtitle: v.asset,
        group: group,
        extraData: v,
        icon: this.getBankRequestIcon(v),
        button: v.status.code == 'unconfirmed' ? 'unconfirmed' : undefined,
      }

      return displayData;
    });

    groups = _.uniq(groups);
    
    _.each(groups, (v, i) => {
      let setInGroup = _.filter(displaySet, (r, i1) => {
        return r.group == v;
      })

      let set = { title: v, data: setInGroup }
      data.push(set)
    });
    return data;
  }

  getRecordGroup = (record) => {
    let obj = moment.utc(record.created_at).local()

    let group = obj.calendar(null, {
      sameDay: '[Today]',
      nextDay: '[Tomorrow]',
      nextWeek: 'dddd',
      lastDay: '[Yesterday]',
      lastWeek: 'dddd',
      sameElse: 'DD MMMM, YYYY'
    });

    return group;
  }

  isCloseToBottom = (e) => {
    const paddingToBottom = 30;
    return e.nativeEvent.layoutMeasurement.height + e.nativeEvent.contentOffset.y >=
    e.nativeEvent.contentSize.height - paddingToBottom;
  };

  renderBody = () => {
    // let records = this.state.formattedRequests != null ? this.state.formattedRequests : null  
    return (
    <SectionList
        sections={this.state.formattedRequests}
        keyExtractor={(item, index) => item + index}
        contentContainerStyle={!this.state.bankRequests ? {flex: 1} : { paddingBottom: 10 }}
        renderSectionHeader={({section}) => (
          <View style={listStyles.sectionHeader}>
            <Text style={listStyles.sectionTitle} > {section.title} </Text>
          </View>
        )}
        ListFooterComponent={() => {
          if(this.state.isFetchingMore) {
            return(
              <View style={{
                marginTop: 20,
                alignContent: 'center',
                height: 30
              }}>
                  <ActivityIndicator style={{marginTop: 3}} size="small" color="#0084FF" />
              </View>
            )
          } else {
            return(<View></View>)
          }
        }}
        ListHeaderComponent={this.renderHeader()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={this.onRefresh}
          />
        }
        ListEmptyComponent={() => {
          if(!this.state.isLoading) {
            return this.renderStatusBlock();
          } else {
            return this.renderLoadingView();
          }
        }}
        onScroll={(e) => {
          if(this.isCloseToBottom(e) && !this.state.isFetchingMore) {
            this.getBankingRequests();
          }
        }}
        renderItem={({item, index, section}) => (
          <ListItem
            data={item}
            iconStyle={styles.listItemIcon}
            onItemPressed={() => { this.itemSelected(item)
            }}
          />
        )}
      />
    )
  }

  itemSelected = (item) => {
    this.props.navigation.setParams({ header: null });
    this.setState({requestSelected: item.extraData, requestDetailsModal: true})
  }

  getBankRequestIcon = (request) => {
    let icon = undefined

    switch(request.status.code) {
      case "unconfirmed":
        icon = require("../../Assets/Images/user_unconfirmed.png") 
        break;
      case "unable_to_verify":
        icon = require("../../Assets/Images/unable_to_verify.png")
        break;
      case "user_confirmed":
        icon = require("../../Assets/Images/user_confirmed.png")
        break;
      case "verified":
        icon = require("../../Assets/Images/verified_outline.png")
      break;
      default:
        icon = require("../../Assets/Images/canceled.png")
    }
    return icon
  }

  renderRequestDetailsModal = () => {
    return (
       <BankingRequestDetailsModal
        isOpen={this.state.requestDetailsModal}
        onClosed={(updatedItem, didUpdate) => {
          
          if(didUpdate) {
            let index = _.findIndex(this.state.bankRequests, (v) => {
              return v.id == updatedItem.id
            });

            if(index != -1) {
              let collection = this.state.bankRequests;
              collection[index] = updatedItem;
    
              let displayItems = this.getDisplayItems(collection);
              this.setState({
                requestDetailsModal: false,
                bankRequests: collection,
                formattedRequests: displayItems
              })
            } else {
              this.setState({requestDetailsModal: false})
            }
          } else {
            this.setState({requestDetailsModal: false})
          }
          
          this.props.navigation.setParams({ header: undefined }); 
        }}
        data={this.state.requestSelected}
      />
    );
  };


  renderLoadingView = () => {
    return(
      <View style={{flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        // backgroundColor: 'white',
        // marginTop: 150
      }}>
        <ActivityIndicator size="large" color="#0084FF" />
      </View>
    )
  }

  render() {

    return (
      <View style={styles.container}>        
        <StatusBar
          translucent={false}
          barStyle={"dark-content"}
          backgroundColor={"white"}
        />
        
        {this.renderRequestDetailsModal()}
        { this.renderBody() }
        {/* <ScrollView
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],

            {
              useNativeDriver: false
            }
          )}
          onEndReachedThreshold={0.5}
          scrollEventThrottle={1}
          contentContainerStyle={{flex: 1}}
        >
          {this.renderHeader()}

          <View style={[styles.scrollViewBody]}>
            <View style={{width: '100%'}}>
            { this.state.isLoading ? this.renderLoadingView() : this.renderBody()}           
            </View>            
          </View>
        </ScrollView> */}
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    paddingTop: 16,
    paddingBottom: 25,
    paddingRight: 15,
    paddingLeft: 15,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    flexDirection: "column"
  },
  headerTitle: {
    fontWeight: "bold",
    fontSize: 16,
    color: "rgba(0,0,0,0.7)",
    marginTop: 10
  },
  headerSubtitle: {
    marginTop: 3,
    color: "rgba(0, 0, 0, 0.5)",
    fontWeight: "500",
    fontSize: 12
  },
  rightButton: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,

    paddingTop: 10
  },
});

const listStyles = StyleSheet.create({
  sectionHeader: {
    paddingRight: 16,
    paddingLeft: 16,
    paddingTop: 16,
    paddingBottom: 5,
    
  },
  sectionTitle: {
    fontWeight: '500',
    color: '#ABAFB6',
    fontSize: 12
  },
  emptyTitle: {
    fontSize: 16,
    color: 'black'
  },
  emptyDescription: {
    fontSize: 12,
    color: 'rgba(0,0,0,0.50)'
  },
  emptyBox: {
    backgroundColor: 'white',
    marginBottom: 10,
    width: '100%',
    minHeight: 150,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  emptyContainer: {
    flex: 1,
    marginTop: 30,
   
  }
});

const styles = StyleSheet.create({
  listItemIcon: {
    width: 34,
    height: 34,
    padding: 5,
    // borderColor: 'red', borderWidth: 1,
    borderRadius: 0
  },
  container: {
    flex: 1,
    backgroundColor: "#F0F3F7"
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButtonSecondary:{
    backgroundColor: '#ffffff'
  },
  generalButton: {
    alignItems: 'center',
    minHeight: 25,
    minWidth: 90, 
    paddingTop: 4,
    paddingBottom: 4,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 30,
    marginTop: 10,
    borderRadius: 27,
    borderWidth: 3,
    borderColor: '#0084FF',
    marginRight: 15
  },
  generalButtonTitle: {
    fontSize: 13,
    fontWeight: '500',
    color: '#0084FF',
    paddingTop: 5,
    paddingBottom: 5
  },
  button: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 45,
    minWidth: 90, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    paddingRight: Platform.OS == 'ios' ? 20 : 15,
    paddingLeft: Platform.OS == 'ios' ? 20 : 15,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
    backgroundColor: '#0084FF',
    marginLeft: 15,
    marginRight: 15
  },
  buttonContainer: {
    height: 65,
    // borderColor: 'orange', borderWidth: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: 35,
    paddingBottom: 20
  },
  buttonPrimaryTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white'
  },
  scrollViewBody: {
    flex: 1,
    // paddingLeft: 20, paddingRight: 20
  },
  iosIcon: {
    width: 14,
    height: 23
  },
  androidIcon: {
    width: 20,
    height: 19,
    marginRight: 5,
    marginLeft: 5
  },
  redNoticeIcon: {
    width: 20,
    height: 20
  },
  centerContent: {
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  leftAlignContent: {
    alignItems: "flex-start"
  },
  rightAlignContent: {
    alignItems: "flex-end"
  },
  contentPadding: {
    paddingTop: 9,
    paddingBottom: 9
  },
});

const blockStyles = StyleSheet.create({
  container: {
    marginTop: 5,
    marginBottom: 5,
    paddingBottom: 18
  },
  containerTitle: {
    color: "#ABAFB6",
    fontWeight: "500",
    fontSize: 12
  },
  bankIcon: {
    width: 40,
    height: 40,
    marginLeft: 5
  },
  leftAlignedIcon: {
    width: 28,
    height: 28, 
    marginRight: 20
  },
  block: {
    backgroundColor: "white"
  },

  emptyBlock: {
    minHeight: 173
  },
  centerBlock:{
    // flex: 1,
    // flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "white",
    paddingTop: 35,
    paddingBottom: 35,
    marginTop: 70
  },
  blockIconContainer: {
    width: 80,
    height: 80,
    borderRadius: 40,
    padding: 15,
    marginRight: 15,
    backgroundColor: "#F4F6FA",
    marginBottom: 10
  },
  blockIcon: {
    width: 30,
    height: 30
  },
  plusIcon: {
    width: 18,
    height: 18
  },
  statusIconHeader: {
    width: 22,
    height: 22,
    position: 'absolute',
    left: 17,
    top: -80
  },
  transparentBg: {
    backgroundColor: "transparent"
  },
  listItemColumn: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "column",

    paddingTop: 15,
    paddingBottom: 15,

    alignContent: "center",
    justifyContent: "space-between",
    //alignContent: "stretch"
  },
  listItem: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    paddingTop: 1,
    paddingBottom: 1,
    alignContent: "center",
    justifyContent: "space-between",
    //alignContent: "stretch"
  },
  listItemLeft: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-start"
    //paddingTop: 15,
    //paddingBottom: 15,

  },
  listItemRight: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-end"
    //paddingTop: 15,
    //paddingBottom: 15,

  },
  listItemRightFunds: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-end",
    paddingTop: 8,
    //paddingBottom: 15,

  },
  listItemText: {
    fontSize: 16,

    color: "#000000"
  },
  listItemTextFunds: {
    fontSize: 16,
    color: "#000000",
    paddingTop: 8
  },
  listItemSubText: {
    fontSize: 12,
    color: 'rgba(0,0,0,0.75)'
  },
  listItemTextPrimary: {
    fontSize: 16,

    color: "#0084FF",

    textAlign: "center"
  },
  listItemTextSecondary: {
    fontSize: 16,

    color: "#0084FF",

    textAlign: "center"
  },
  blockGap: {
    height: 60
  },
  bottomSeperator: {
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1
  },
});
