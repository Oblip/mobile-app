import React, { Component } from 'react';
import {  
  View,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  StatusBar,
  Platform,
} from 'react-native'
import ListItem from '../../Components/ListItem'
import NormalButton from '../../Components/NormalButton'
import Text from '../../Components/globalTextStyle';

export default class AgentScreen extends Component {
  constructor (props) {
    super(props)
  }

  static navigationOptions = ({navigation, screenProps}) => ({
    title: '',
    headerTitleStyle: {
      flex: 1,
      color: 'black',
      textAlign: 'center',
      alignSelf: 'center'
    },
    headerStyle: {
      backgroundColor: 'white',
      borderBottomColor: 'transparent',
      elevation: 0,
      shadowColor: 'transparent',
      shadowOpacity: 1,
    },
    headerTintColor: 'rgba(0,0,0,1)',
    headerRight: (
      <View style={styles.navRightItem}>
        <NormalButton
            title={'SHARE'}
            onPress={() => this.onNextPressed()}
          />
      </View>
    )
  });

  renderHeader = () => {
    return(
      <View style={styles.header}>
        <View style={styles.headerContent}>
          <Text style={styles.headerTitle}>ABC Store</Text>
        </View>
        <View style={styles.buttonContainer}>
          <View style={{marginRight: 20}}>
            <TouchableOpacity
              onPress={() => {}}
              style={[styles.generalButton, styles.generalButtonPrimary]}>
              <Text style={[styles.generalButtonTitle, styles.generalButtonTitlePrimary]}>Topup</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => {}}
              style={[styles.generalButton, styles.generalButtonPrimary]}>
              <Text style={[styles.generalButtonTitle, styles.generalButtonTitlePrimary]}>Cash-out</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  render () {
    return (
     <View style={styles.container}>
       <StatusBar
           backgroundColor={'white'}
           translucent={false}
           barStyle={'dark-content'}
         />
 
      <ScrollView>
        { this.renderHeader() }

        <View style={blockStyles.container}>
          <Text style={[blockStyles.containerTitle, styles.contentPadding]}>
            LOCATION
          </Text>

          <View
            style={[
              blockStyles.block,
              blockStyles.emptyBlock,
              styles.contentPadding,
              styles.centerContent
            ]}
          >
            <Text style={blockStyles.blockTitle}>Nothing here!</Text>

            <Text style={blockStyles.blockInstructions}>
              The location is currently unavailable
            </Text>
          </View>
        </View>
      </ScrollView>
 
     </View>
    )
   }
 } 
 
 const styles = StyleSheet.create({
   container: {
     flex: 1,
     backgroundColor: '#F0F3F7',
   },
   navRightItem: {
     paddingRight: 16,
   },

   centerContent: {
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },

   contentPadding: {
    paddingLeft: 18,
    paddingRight: 18,
    paddingTop: 14,
    paddingBottom: 14
  },
   
   headerContent: {
     // borderColor:'orange', borderWidth: 1,
     alignContent: 'center', alignItems:'center',  justifyContent: 'center'
   },
   generalButtonIcon: {
     width: 24,
     height: 24,
     marginRight: 10,
   },
   generalButtonTitle: {
     fontSize: 14,
     fontWeight: '500',
     color: '#232228'
   },
   generalButtonTitlePrimary: {
     color: 'white'
   },
   generalButtonPrimary: {
     backgroundColor: '#0084FF',
   },
   generalButtonIconPrimary: {
     tintColor: 'white',
     marginRight: 5
   },
   generalButton: {
     alignItems: 'center',
     alignSelf: 'stretch',
     flexDirection: 'row',
     height: 45,
     width: 115, 
     paddingTop: 5,
     paddingBottom: 5,
     backgroundColor: '#ffffff',
     justifyContent: 'center',
     marginBottom: 10,
     marginTop: 10,
     borderRadius: 27,
     // paddingRight: 15,
     // paddingLeft: 10,
     shadowColor: "rgba(0,0,0,0.1)",
     shadowOffset: {
       width: 0,
       height: 2,
     },
     shadowOpacity: 6,
     elevation: 2,
   },
 
   buttonContainer: {
     flexDirection:'row',
     alignItems:'flex-end',
     justifyContent:'center',
     marginTop: 5,
     marginBottom: 15,
     // borderColor: 'red', borderWidth: 1,
   },
 
   header: {
     paddingTop: 13,
     paddingBottom: 13,
     // borderColor: 'red', borderWidth: 1,
     height: 190,
     backgroundColor: 'white',
     flexDirection: 'column',
     alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch'
   },
   headerTitle: {
     fontFamily: Platform.OS == 'ios' ? 'Avenir-Black' : 'sans-serif-black',
     fontSize: 27,
     color: '#030303',
     marginTop: 3,
   }
 })

const blockStyles = StyleSheet.create({
  container: {
    marginTop: 10,
    marginBottom: 10
  },
  containerTitle: {
    color: "#ABAFB6",
    fontWeight: "500",
    fontSize: 12
  },
  block: {
    backgroundColor: "white"
  },

  emptyBlock: {
    minHeight: 173
  },

  blockIconContainer: {
    width: 41,
    height: 41,

    borderRadius: 20,

    padding: 10,

    backgroundColor: "#F4F6FA",

    marginBottom: 10
  },
  plusIcon: {
    width: 18,
    height: 18
  },
  bankIcon: {
    width: 18,
    height: 17
  },

  generalIcon: {
    width: 24,
    height: 24
  },
  blockLink: {
    color: "#0084FF",

    fontSize: 16,

    fontWeight: "500"
  },
  blockTitle: {
    color: "black",

    fontSize: 16,

    marginBottom: 5
  },
  blockInstructions: {
    color: "rgba(0,0,0,0.5)",

    fontSize: 12,

    textAlign: "center"
  },
  emptyContent: {
    marginTop: 20
  },

  transparentBg: {
    backgroundColor: "transparent"
  },
  listItem: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",

    paddingTop: 15,
    paddingBottom: 15,

    alignContent: "space-between",
    justifyContent: "space-between",
    alignContent: "stretch"
  },
  listItemText: {
    fontSize: 16,

    color: "#000000"
  },
  listItemTextPrimary: {
    fontSize: 16,

    color: "#0084FF",

    textAlign: "center"
  },
  blockGap: {
    height: 60
  },
  bottomSeperator: {
    borderBottomColor: "#EEEEEE",

    borderBottomWidth: 1
  }
});
