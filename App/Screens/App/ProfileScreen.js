import React, { Component } from 'react';
import {  
  View,
  TouchableOpacity,
  StyleSheet,
  SectionList,
  StatusBar,
  Image
} from 'react-native'
import ListItem from '../../Components/ListItem'
import NormalButton from '../../Components/NormalButton'
import Text from '../../Components/globalTextStyle';

export default class ProfileScreen extends Component {
  constructor (props) {
    super(props)
  }

  static navigationOptions = ({navigation, screenProps}) => ({
    title: 'Profile',
    headerTitleStyle: {
      flex: 1,
      color: 'black',
      textAlign: 'center',
      alignSelf: 'center'
    },
    headerStyle: {
      backgroundColor: 'white',
      borderBottomColor: 'transparent',
      elevation: 0,
      shadowColor: 'transparent',
      shadowOpacity: 1,
    },
    headerTintColor: 'rgba(0,0,0,1)',
    headerRight: (
      <View style={styles.navRightItem}>
        <NormalButton
            title={'Share'}
            onPress={() => this.onNextPressed()}
          />
      </View>
    )
  });

  renderHeader = () => {
    return(
      <View style={styles.header}>
        <View style={styles.headerContent}>
          <View style={styles.avatarContainer}>
            <View>
              <Image 
                source={require('../../Assets/Images/demo_avatar.png')}
                style={styles.headerAvatarImage}
              />
            </View>
          </View>
          <Text style={styles.headerTitle}>Reggie Escobar</Text>
          <Text style={styles.headerSubtitle}>Personal</Text>
        </View>
        <View style={styles.buttonContainer}>
          <View style={{marginRight: 20}}>
            <TouchableOpacity
              onPress={() => {}}
              style={styles.generalButton}>
              <Image
                source={require('../../Assets/Images/remove_person_icon.png')}
                style={styles.generalButtonIcon}
                resizeMode={'contain'}
              />
              <Text style={styles.generalButtonTitle}>Remove</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => {}}
              style={[styles.generalButton, styles.generalButtonPrimary]}>
              <Image
                source={require('../../Assets/Images/money_icon.png')}
                style={[styles.generalButtonIcon, styles.generalButtonIconPrimary]}
                resizeMode={'contain'}
              />
              <Text style={[styles.generalButtonTitle, styles.generalButtonTitlePrimary]}>Payment</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }


  render () {
   return (
    <View style={styles.container}>
      <StatusBar
          backgroundColor={'white'}
          translucent={false}
          barStyle={'dark-content'}
        />

      <SectionList
        sections={[
          {title: 'Today', data: [
            'hi','hi', 'hi', 'hi', 'hi', 'hi', 'hi',
          ]},
          {title: 'Yesterday', data: [
            'hi','hi', 'hi', 'hi', 'hi', 'hi', 'hi',
          ]}
        ]}
        keyExtractor={(item, index) => item + index}
        stickySectionHeadersEnabled={false}
        renderSectionHeader={({section}) => (
          <View style={listStyles.sectionHeader}>
            <Text style={listStyles.sectionTitle} > {section.title} </Text>
          </View>
        )}
        ListHeaderComponent={() => this.renderHeader()}
        ListEmptyComponent={() => (
          <View style={listStyles.emptyContainer}>
            <View style={listStyles.emptyBox}>
              <Text style={listStyles.emptyTitle}>Nothing Here!</Text>
              <Text style={listStyles.emptyDescription}>Your future transaction logs will appear here.</Text>
            </View>
          </View>
        )}
        renderItem={({item, index, section}) => (
          <ListItem 
            data={item}
            currentAccountId={0}
            onItemPressed={() => {
              alert("Tap!")
            }}
          />
        )}
      />

    </View>
   )
  }
} 

const listStyles = StyleSheet.create({
  sectionHeader: {
    paddingRight: 16,
    paddingLeft: 16,
    paddingTop: 16,
    paddingBottom: 5,
    
  },
  sectionTitle: {
    fontWeight: '500',
    color: '#ABAFB6',
    fontSize: 12
  },
  emptyTitle: {
    fontSize: 16,
    color: 'black'
  },
  emptyDescription: {
    fontSize: 12,
    color: 'rgba(0,0,0,0.50)'
  },
  emptyBox: {
    backgroundColor: 'white',
    marginBottom: 10,
    width: '100%',
    minHeight: 150,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },
  emptyContainer: {
    flex: 1,
    marginTop: 30,
   
  }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0F3F7',
  },
  navRightItem: {
    paddingRight: 16,
  },
  
  headerContent: {
    // borderColor:'orange', borderWidth: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  generalButtonIcon: {
    width: 24,
    height: 24,
    marginRight: 10,
  },
  generalButtonTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#232228'
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButtonIconPrimary: {
    tintColor: 'white',
    marginRight: 5
  },
  generalButton: {
    alignItems: 'center',
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 50,
    width: 115, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    // paddingRight: 15,
    // paddingLeft: 10,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
  },

  buttonContainer: {
    flexDirection:'row',
    alignItems:'flex-end',
    justifyContent:'center',
    marginTop: 5,
    marginBottom: 15,
    // borderColor: 'red', borderWidth: 1,
  },

  header: {
    paddingTop: 13,
    // borderColor: 'red', borderWidth: 1,
    height: 215,
    backgroundColor: 'white',
    flexDirection: 'column',
    alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch'
  },
  headerTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#030303',
    marginTop: 3,
  },
  headerSubtitle: {
    color: 'rgba(0,0,0,0.5)',
    fontWeight: '500',
    fontSize: 12,
  },
  headerAvatarImage: {
    width: 60,
    height: 60,
    borderRadius: 30,
    // justifyContent: 'center',
  },
})