import React, { Component } from "react";

import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  StatusBar,
  Animated,
  Image,
  Platform
} from "react-native";

export default class BlockchainIntro extends Component {
  constructor (props) {
    super(props);

  }

  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerStyle: {
      backgroundColor: 'white',
      borderBottomColor: "transparent",
      elevation: 0,
      shadowColor: "transparent",
      shadowOpacity: 1
    },
    headerTintColor: 'black'
  })  

  render () {
    return (
      <View style={styles.container}>
        <Text>Hello world</Text>
      </View>
    )
  }
 }

 const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white'
    }
 })