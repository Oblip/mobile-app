import React, { Component } from 'react';
import {  
  View,
  YellowBox,
  Vibration,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Platform,
  Alert
} from 'react-native'
import Text from '../../Components/globalTextStyle';
import * as Animatable from 'react-native-animatable';
import accounting from 'accounting-js'
import LoadingModal from '../../Components/Modals/LoadingModal'
import ConfirmationModal from '../../Components/Modals/ConfirmationModal'
import SelectItemModal from '../../Components/Modals/SelectItemModal'
import BlockchainManagementApi from '../../Services/BlockchainManagementApi'
import BankManagementApi from '../../Services/BankManagementApi'
import Toast from 'react-native-simple-toast'
import _ from 'underscore'

const initialAmountFontSize = 100;
const initialCentFontSize = 30;
const buttonElevation = 2;


export default class TopupWithdrawScreen extends Component {

  constructor (props) {
    super(props)
    this.amountTextList = [];

    this.state = {
      amount: "0",
      isTransactionReviewModalOpen: false,
      isSelectAssetOpen: false,
      isSelectFundOpen: false,
      currentAsset: undefined,
      currentFund: undefined,
      assetData: undefined,
      fundsData: undefined,
      isConfirmationModalOpen: false,
      showLoading: false
    }

    try {
      this.extraData  = this.props.navigation.state.params.extraData
      this.type = this.props.navigation.state.params.type
    } catch(e) {

    }
    
  }

  static navigationOptions = ({navigation, screenProps}) => ({
    title: '',
    headerStyle: {
      backgroundColor: 'white',
      borderBottomColor: 'transparent',
      elevation: 0,
      shadowColor: 'transparent',
      shadowOpacity: 1,
    },
    headerTintColor: 'rgba(0,0,0,1)',
    headerRight: <View></View>,
    header:  _.isUndefined(navigation.state.params) ? undefined : navigation.state.params.header,
  })

  componentWillMount() {
    this.props.navigation.setParams({
      header: undefined
    });

    // this.onPayButtonPressed()
    BlockchainManagementApi.fetchAvailableAssets()
    .then(data => {
      console.log(data)
      this.setAvailableAssets(data)
    })
    .catch(error => {
      console.log('assets didnt load')
    })

    BlockchainManagementApi.fetchUserFunds()
    .then(data => {
      console.log(data)
      this.setUserFunds(data)
    })
    .catch(error => {
      console.log('funds didnt load')
    })
  }

  setAvailableAssets(assets){
    allAssets = [];

    assets.assets.map((asset) => {
      allAssets.push({
        value: asset.asset_code,
        title: asset.asset_name
      });    
    });
    //console.log(allBanks);
    this.setState({
      assetData: allAssets,
      currentAsset: allAssets[0]
    });
  }

  setUserFunds(funds){
    allFunds = [];

    funds.funds.map((fund) => {
      allFunds.push({
        value: fund.balance,
        title: fund.asset.asset_name,
        subtitle: fund.balance,
        code: fund.asset.asset_code
      });    
    });
    //console.log(allBanks);
    this.setState({
      fundsData: allFunds,
      currentFund: allFunds[0]
    });
  }

  onSelectAssetModalClosed(){
    this.props.navigation.setParams({
      header: undefined
    });
    this.setState({isSelectAssetOpen: false})
  }

  onSelectFundModalClosed(){
    this.props.navigation.setParams({
      header: undefined
    });
    this.setState({isSelectFundOpen: false})
  }

  onConfirmationModalClosed(){
    this.props.navigation.setParams({
      header: undefined
    });
    // this.setState({isConfirmationModalOpen: false})
    this.props.navigation.goBack(null)
  }

  renderSelectAssetModal = () => {
    return(
      <SelectItemModal
        isOpen={this.state.isSelectAssetOpen}
        onClosed={() => { this.onSelectAssetModalClosed()}}
        data={this.state.assetData}
        onItemSelected={(item) => {
          this.setState({
            currentAsset: item,
            isSelectAssetOpen: false
          })
        }}
        currentItem={this.state.currentAsset}
        title={'Select Asset'} />
    )
  }

  renderSelectFundModal = () => {
    return(
      <SelectItemModal
        isOpen={this.state.isSelectFundOpen}
        onClosed={() => { this.onSelectFundModalClosed()}}
        data={this.state.fundsData}
        onItemSelected={(item) => {
          this.setState({
            currentFund: item,
            isSelectFundOpen: false
          })
        }}
        currentItem={this.state.currentFund}
        title={'Select Fund'} />
    )
  }

  renderConfirmationModal = () => {

    let modaltxt = this.type == "topup" ? "Topup Request Created" : "Your cashout request was created"
    let modalsubtxt = this.type == "topup" ? "Deposit funds at the bank and confirm" : "We'll transfer the funds to your account shortly"

    return(
      <ConfirmationModal
        isOpen={this.state.isConfirmationModalOpen}
        onClosed={() => { 
          this.onConfirmationModalClosed()
        }}        
        modalText={modaltxt}
        modalSubtext={modalsubtxt} />
    )
  }

  _addValueToAmount = (value) => {
    let currentValue = this.state.amount.toString()
    let formattedAmount = accounting.formatNumber(currentValue, { precision: 0 })

    // don't allow more than one dot 
    if (value == '.' && currentValue.includes('.')) { return; }

    // only allow two values for cents.
    if(currentValue.includes('.')) {
      let cents = currentValue.split('.')[1]
      if(cents.length == 2) { return; }
    }

    // limit the user from entering more than 6 digits (not including cents)
    if(currentValue.split('.')[0].length >= 6 && value != '.' && (!currentValue.includes('.'))) {  
      Vibration.vibrate(800); this.amountContainer.shake(500)
      return; 
    }

    if(currentValue == '0') {
      if(value == '.') {
        let newValue = currentValue + '' + value
        this.setState({amount:  newValue})
      } else {
        this.setState({amount: value});
      }
    } else {
      let newValue = currentValue + '' + value
      this.setState({amount:  newValue})
    }

    // this.amountText.fadeInDownBig(500)
  
  }

  _removeValueFromAmount = () => {
    let currentValue = this.state.amount.toString()
    let formattedAmount = accounting.formatNumber(currentValue, { precision: 0 })
    
    if(currentValue.length > 1) {
      let amountToRemove = 1

      if (currentValue.endsWith('.')) {
         amountToRemove = 2 
         this.amountTextList[formattedAmount.length - 1].fadeOutDownBig(500)
      }
      
      if(!currentValue.includes('.')) {
        this.amountTextList[formattedAmount.length - 1].fadeOutDownBig(500)
      }
      
      setTimeout(() => {
        this.setState({amount: currentValue.substr(0, currentValue.length - amountToRemove)})
      }, 200)
    } else {
      this.setState({amount: '0'})
    }



    // this.amountText.fadeOut(800)
  }
  
  _renderAmountTexts = (amountString) => {
    let amountFontSize = initialAmountFontSize - (6 * (amountString.length - 1))
    allText = _.map(amountString, (value, i) => {
      return (
        <Animatable.Text
          key={i}
          animation={'fadeInDownBig'}
          duration={500}
          ref={(ref) => {
            this.amountTextList[i] = ref
          }}
          style={[styles.amountText, { fontSize: amountFontSize}]}>
            {value}
        </Animatable.Text>
      )
    })

    return allText
  }

  renderFormattedAmount = () => {
    let currentValue = this.state.amount.toString()
    let values = currentValue.split('.')

    let amount = ''
    let cents = ''
    amount = values[0]
    
    if (values.length > 1) { cents = values[1] }

    // let amountFontSize = initialAmountFontSize - (6 * (amount.length - 1))
    let centFontSize = initialCentFontSize - (3 * (amount.length - 1))

    formattedAmount = accounting.formatNumber(amount, { precision: 0 })
    return (
      <Animatable.View ref={(ref) => this.amountContainer = ref} useNativeDriver={true} style={styles.amountWrapper}>
        {/* <Text style={styles.amountCurrencySymbol}>$</Text> */}

        {this._renderAmountTexts(formattedAmount)}
        {/* <Animatable.Text 
          ref={(ref) => this.amountText = ref} 
          animation={'fadeInDown'}
          style={[styles.amountText, { fontSize: amountFontSize}]}>
            {accounting.formatNumber(amount, { precision: 0 })}
        </Animatable.Text> */}
        <Text style={[styles.amountCentsText, {fontSize: centFontSize}]}>{cents}</Text>
      </Animatable.View>
    )
  }

  onTransactionReviewModalClosed = () => {
    this.props.navigation.setParams({
      header: undefined
    });

    this.setState({
      isTransactionReviewModalOpen: false
    })
  }

  onRequestButtonPressed = () => {
    let amt = parseFloat(this.state.amount)

    if(this.state.currentAsset == undefined){
      Toast.show("Please select an asset", Toast.LONG)
      return
    }else if(this.state.amount == undefined){
      Toast.show("Please enter an amount", Toast.LONG)
      return
    }    
    else if(amt <= 0){
      Toast.show("Please enter a valid amount", Toast.LONG)
      return
    }

    //ask for confirmation before proceeding
    Alert.alert(
      'Confirmation',
      'Are you sure you want to make this topup request?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel',},
        {text: 'Yes', onPress: () => this.processRequest()},
      ],
      {cancelable: false},
    )    
  }

  processRequest = () => {
    this.setState({showLoading: true})//show loading while processing
    this.props.navigation.setParams({
      header: null
    });

    //process request
    if(this.type == "topup"){
      this.processTopupRequest() 
    }   
  }
  
  processTopupRequest = () => {
    let amt = parseInt(this.state.amount, 10)
    let asset = "BZD"
    let registeredBankID = this.extraData.registered_bank.id
    let bankAccountID = this.extraData.id

    let data = {
      amount: amt,
      registered_bank_id: registeredBankID,
      asset: asset,
      bank_account_id: bankAccountID
    }
    BankManagementApi.createTopupRequest(data)
    .then(res => {      
      //Toast.show("Success", Toast.LONG);
      //hide loading and show modal
      this.setState({
        showLoading: false,
        isConfirmationModalOpen: true
      })
    }).catch(error => {
      console.log(error);
      this.setState({
        showLoading: false
      })
      Toast.show("Network problem. Try again later", Toast.LONG);
    });
  }

  onConfirmation = () => {
    // let extraData = {
    //   amount: this.state.amount,
    //   name:  this.extraData.isAccount ? `@${this.extraData.username}` : this.extraData.phoneNumber
    // }
    
    // this.props.navigation.navigate('TransactionResultScreen', { extraData: extraData });
  }

  onRequestTopupHeaderOpen = () => {
    this.props.navigation.setParams({
      header: null
    });
    
    this.setState({isSelectAssetOpen: true})
  }

  onRequestWithdrawHeaderOpen = () => {
    this.props.navigation.setParams({
      header: null
    });
    
    this.setState({isSelectFundOpen: true})
  }

  renderTopupHeader = () => {
    let asset = this.state.currentAsset != undefined ? this.state.currentAsset.value : undefined
    return(
      <View>
      <Text style={styles.fundSubtitle}>SELECTED ASSET</Text>
        <TouchableOpacity
          onPress={() => {this.onRequestTopupHeaderOpen()}}
          style={[styles.button, {elevation: this.state.isSelectAssetOpen || this.state.isConfirmationModalOpen || this.state.isSelectFundOpen || this.state.showLoading ? 0 : buttonElevation}]}>
          <Text style={styles.AssetSelectText}>{asset}</Text>
          <View style={styles.centerContent}>
            <Image
              source={require('../../Assets/Images/arrow_drop_down_icon.png')}
              resizeMode={'contain'}
              style={styles.drop_down_icon} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  renderWithdrawHeader = () => {
    let asset = this.state.currentFund != undefined ? this.state.currentFund.code + " " + parseFloat(this.state.currentFund.value).toFixed(2).toString() : undefined
    return(
      <View>
      <Text style={styles.fundSubtitle}>SELECTED FUND</Text>
        <TouchableOpacity
          onPress={() => {this.onRequestWithdrawHeaderOpen()}}
          style={[styles.button, {elevation: this.state.isSelectAssetOpen || this.state.isConfirmationModalOpen || this.state.isSelectFundOpen || this.state.showLoading ? 0 : buttonElevation}]}>
          <Text style={styles.AssetSelectText}>{asset}</Text>
          <View style={styles.centerContent}>
            <Image
              source={require('../../Assets/Images/arrow_drop_down_icon.png')}
              resizeMode={'contain'}
              style={styles.drop_down_icon2} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }


  render () {
    const headerSelect = this.type == "topup" ? this.renderTopupHeader() : this.renderWithdrawHeader()
    const buttonText = this.type == "topup" ? "Request Topup" : "Request Cashout"
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={'white'}
          translucent={false}
          barStyle={'dark-content'}
        />
        {this.renderSelectAssetModal()}
        {this.renderSelectFundModal()}
        {this.renderConfirmationModal()}
        <LoadingModal showLoading={this.state.showLoading} />
        <View style={styles.fundContainer}>
          {headerSelect}
        </View>

        <View style={styles.amountContainer}>
          {this.renderFormattedAmount()}
        </View>

        {/* TODO: Refactor this; use loop? */}
        <View style={keyboardStyles.container}>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('1')}>
              <Text style={keyboardStyles.keyText}>1</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('2')}>
              <Text style={keyboardStyles.keyText}>2</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('3')}>
              <Text style={keyboardStyles.keyText}>3</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('4')}>
              <Text style={keyboardStyles.keyText}>4</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('5')}>
              <Text style={keyboardStyles.keyText}>5</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('6')}>
              <Text style={keyboardStyles.keyText}>6</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('7')}>
              <Text style={keyboardStyles.keyText}>7</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('8')}>
              <Text style={keyboardStyles.keyText}>8</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('9')}>
              <Text style={keyboardStyles.keyText}>9</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('.')}>
              <Text style={[keyboardStyles.keyText]}>·</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('0')}>
              <Text style={keyboardStyles.keyText}>0</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._removeValueFromAmount()}>
              <Image
                source={require('../../Assets/Images/ios_back_icon.png')}
                resizeMode={'contain'}
                style={keyboardStyles.deleteIcon}
                />
            </TouchableOpacity>
          </View>
        </View>

        <View style={[styles.buttonContainer, {elevation: this.state.isSelectAssetOpen || this.state.isConfirmationModalOpen || this.state.showLoading ? 0 : buttonElevation}]}>
          <TouchableOpacity
            onPress={() => this.onRequestButtonPressed()}
            style={[styles.button, styles.generalButtonPrimary, styles.payButton]}>  
            <Text style={styles.buttonPrimaryTitle}>{buttonText}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
    }

}

const keyboardStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20
  },

  deleteIcon: {
    width: 12,
    height: 21
  },
  keyButton: {
    // borderColor: 'red', borderWidth: 1,
    flex: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    padding: 10
  },
  keyText: {
    fontSize: 27,
    color: 'rgba(0,0,0,0.5)',
    fontWeight:  Platform.OS == 'ios' ? '400' : '300'
  },
  keyRow: {
    flexDirection: 'row',
    width: '100%',
    paddingBottom: 10,
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'space-around', 
    alignContent: 'center', alignItems:'center',
  }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  drop_down_icon: {
    width: 10, height: 5,
    tintColor: 'rgba(0,0,0,0.54)',
    position: 'absolute',
    right: -35,
    top: -3
  },
  drop_down_icon2: {
    width: 10, height: 5,
    tintColor: 'rgba(0,0,0,0.54)',
    position: 'absolute',
    right: -22,
    top: -3
  },
  buttonAvatar: {
    width: 24,
    height: 24,
    marginRight: 10
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  amountContainer: {
    marginTop: 10,
    marginBottom: 10,
    height: 120,
    // borderColor: 'purple', borderWidth: 2,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  amountWrapper: {
    flexDirection: 'row',
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  amountCurrencySymbol: {
    fontSize: 30,
    marginTop: 20,
    color: 'black',
    fontWeight: '100',
    alignSelf: 'flex-start',
  },

  amountCentsText: {
    fontSize: initialCentFontSize,
    color: 'black',
    fontWeight: '300',
    alignSelf: 'flex-start'
  },
  amountText: {
    fontSize: initialAmountFontSize,
    color: 'black'
  },


  buttonContainer: {
    // height: 75,
    // borderColor: 'orange', borderWidth: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  buttonPrimaryTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white'
  },
  
  payButton: {
    paddingLeft: 20, paddingRight: 35,
    height: 50,
  },

  fundContainer: {
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  AssetSelectText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'black'
  },

  button: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 40,
    minWidth: 130, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 40,
    marginTop: 10,
    borderRadius: 27,
    paddingRight: Platform.OS == 'ios' ? 20 : 15,
    paddingLeft: Platform.OS == 'ios' ? 20 : 15,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
    
  },
  fundSubtitle: {
    color: 'rgba(0,0,0,0.5)',
    fontSize: 11,
    textAlign: 'center'
  }

})