import React, { Component } from 'react';
import {  
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  Image
} from 'react-native'; 
import Text from '../../Components/globalTextStyle';

import EditDetailsModal from '../../Components/Modals/EditDetailsModal';

export default class PersonalDetailsScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isMyEditDetailsModalOpen: false, 
      isMyEditDetailsModalClosed: false,
      hideButtomButtons: false,
      statusBarStyle: 'light-content',
      statusBarBackgroundColor: 'transparent',
      formKey: 0
    }
  }

  onEditDetailsModalOpen = (key) => { 
    this.props.navigation.setParams({ header: null });
    this.setState({
      isMyEditDetailsModalOpen: true, 
      hideButtomButtons: true,
      statusBarBackgroundColor: 'transparent',
      statusBarStyle: 'light-content',
      formKey: key
    })
  }

  onEditDetailsModalClose = () => { 
    this.props.navigation.setParams({ header: undefined });
    this.setState({
      isMyEditDetailsModalOpen: false,
      hideButtomButtons: false,
      statusBarBackgroundColor: 'transparent',
      statusBarStyle: 'light-content'
    })
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Personal Details', 
    headerTitleStyle: {
      flex: 1,
      color: 'black',
      textAlign: 'center',
      alignSelf: 'center',
      fontSize: 18
    },
    headerStyle: {
      backgroundColor: 'white',
      borderBottomColor: 'transparent',
      elevation: 0,
      shadowColor: 'transparent',
      shadowOpacity: 1,
    },
    header: navigation.getParam("header", undefined),
    headerTintColor: 'rgba(0,0,0,1)',
    headerRight: <View />
  });

  render() {
    return(
    <View style={styles.container}>
        <StatusBar
          backgroundColor={'white'}
          translucent={false}
          barStyle={'dark-content'}
        />

        <EditDetailsModal
          style={{zIndex: 50}}
          onClosed={this.onEditDetailsModalClose}
          isOpen={this.state.isMyEditDetailsModalOpen}  
          formKey={this.state.formKey}
        />

      <ScrollView>
        <View style={styles.content}>
          <View>
              <View style={styles.header}>
                <View style={styles.headerContent}>
                  <View style={styles.avatarContainer}>
                    <View>
                        <Image 
                          source={require('../../Assets/Images/avatar_placeholder.png')}
                          style={styles.headerAvatarImage}
                        /> 
                    </View>
                  </View> 
                </View>
            </View> 
            <View style={styles.paddedContainer}>
              <View style={styles.group}>
                <View style={styles.formFeilds}>
                  <Text style={styles.formLabels}>Full name and date of birth</Text>
                  <View style={styles.inputRow}>
                    <Text style={styles.formInputs}>Reggie Escobar</Text>
                      <Image 
                      source={require('../../Assets/Images/danger_icon.png')}
                      resizeMode={'contain'}
                      style={styles.noticeIconDanger}
                    />
                    <Text style={styles.noticeTextDanger}>Date of Birth Required</Text>
                  </View>
                </View>
                <View style={styles.editIconView}>
                  <TouchableOpacity
                    onPress={() => {
                      this.onEditDetailsModalOpen(0)
                    }}>
                    <Image 
                      source={require('../../Assets/Images/edit-icon.png')}
                      resizeMode={'contain'}
                      style={styles.editIcon}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={styles.paddedContainer}>
              <View style={styles.group}>
                <View style={styles.formFeilds}>
                  <Text style={styles.formLabels}>Home Address</Text>
                  <Text style={styles.formInputs}>6 Branch Mouth Road</Text>
                  <Text style={styles.formInputs}>Santa Famila Village</Text>
                  <Text style={styles.formInputs}>Cayo District</Text>
                  <Text style={styles.formInputs}>Belize Central America</Text>
                </View>
                <View style={styles.editIconView}>
                <TouchableOpacity
                    onPress={() => this.onEditDetailsModalOpen(1)}>
                  <Image 
                    source={require('../../Assets/Images/edit-icon.png')}
                    resizeMode={'contain'}
                    style={styles.editIcon}
                  />
                   </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={styles.paddedContainer}>
              <View style={styles.group}>
                <View style={styles.formFeilds}>
                  <Text style={styles.formLabels}>Phone number</Text>
                  <Text style={styles.formInputs}>+501 662 0651</Text> 
                </View>
                <View style={styles.editIconView}>
                  <TouchableOpacity
                      onPress={() => this.onEditDetailsModalOpen(2)}>
                    <Image 
                      source={require('../../Assets/Images/edit-icon.png')}
                      resizeMode={'contain'}
                      style={styles.editIcon}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={styles.paddedContainer}>
              <View style={styles.group}>
                <View style={styles.formFeilds}>
                  <Text style={styles.formLabels}>Email</Text>
                  <View style={styles.inputRow}>
                    <Text style={styles.formInputs}></Text>
                      <Image 
                      source={require('../../Assets/Images/warning_icon.png')}
                      resizeMode={'contain'}
                      style={styles.noticeIconWarning}
                    />
                    <Text style={styles.noticeTextWarning}>Not Specified</Text>
                  </View>
                </View>
                <View style={styles.editIconView}>
                <TouchableOpacity
                      onPress={() => this.onEditDetailsModalOpen(3)}>
                  <Image 
                    source={require('../../Assets/Images/edit-icon.png')}
                    resizeMode={'contain'}
                    style={styles.editIcon}
                  />
                </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
      </View> 
      </ScrollView>
            <View style={styles.buttonContainer}> 
              <TouchableOpacity
                onPress={() => {}}
                style={[styles.generalButton, styles.generalButtonPrimary]}>
                <Text style={[styles.generalButtonTitle, styles.generalButtonTitlePrimary]}>Verify Identity</Text>
              </TouchableOpacity>
            </View>
    </View>
    )
  }
} 

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    // borderColor: 'red', borderWidth: 1, 
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  group: {
    flexDirection: 'row',
    flex: 1,
    // borderColor: 'orange', borderWidth:1,
    justifyContent: 'space-between'
  },
  paddedContainer: {
    flexDirection: 'row',
    // borderColor: 'red', borderWidth: 1,  
    paddingRight: 25,
    paddingLeft: 25, 
    paddingBottom: 30,
  },
  navRightItem: {
    paddingRight: 16,
  },
  
  headerContent: {
    // borderColor:'orange', borderWidth: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  generalButtonIcon: {
    width: 10,
    height: 24
  },
  generalButtonTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#232228'
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  generalButtonIconPrimary: {
    tintColor: 'white'
  },
  generalButton: {
    alignItems: 'center',
    alignSelf: 'stretch',
    flexDirection: 'row',
    height: 50,
    width: 250, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    paddingRight: 15,
    paddingLeft: 10,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
  },

  buttonContainer: {
    flexDirection:'row',
    alignItems:'flex-end',
    justifyContent:'center',
    marginTop: 5,
    marginBottom: 15,
    backgroundColor: 'white'
    // borderColor: 'red', borderWidth: 1,
  },

  header: {
    paddingTop: 13,
    // borderColor: 'red', borderWidth: 1,
    height: 140,
    backgroundColor: 'white',
    flexDirection: 'column',
    alignContent: 'space-between', justifyContent: 'space-between', alignContent: 'stretch'
  },
  headerTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#030303',
    marginTop: 3,
  },
  headerSubtitle: {
    color: 'rgba(0,0,0,0.5)',
    fontWeight: '500',
    fontSize: 12,
  },
  headerAvatarImage: {
    width: 60,
    height: 60,
    borderRadius: 30,
    // justifyContent: 'center',
  },
  formFeilds: { 
    // borderColor: 'red', borderWidth: 1
  },
  formLabels: {
    fontSize: 12,
    color: '#D7D5DB',
    fontWeight: '500',
  },
  formInputs: {
    fontSize: 14,
    color: '#000000',
    // fontWeight: 'bold',
    paddingRight: 7
  },
  editIconView: { 
    // borderColor: 'red', borderWidth: 1,  
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  editIcon: {
    width: 24,
    height: 24,
    // borderRadius: 30,
    // padding: 5
  },
  noticeIconWarning: { 
    width: 20,
    height: 20,
  },
  noticeIconDanger: { 
    width: 20,
    height: 20,
  },
  noticeTextWarning: {
    fontSize: 10,
    color: '#F28A32',
    paddingLeft: 10,
    paddingTop: 5
  },
  noticeTextDanger: {
    fontSize: 10,
    color: '#FF1744',
    paddingLeft: 10,
    paddingTop: 5
  },
  inputRow: {
    flexDirection: 'row'
  }
})