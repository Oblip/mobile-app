import React, { Component } from "react";

import {
  View,
  StyleSheet,
  ScrollView,
  YellowBox,
  TouchableOpacity,
  StatusBar,
  Animated,
  Image,
  Alert,
  Platform,
  ActivityIndicator,
} from "react-native";

import Text from '../../Components/globalTextStyle';
import BackButton from "../../Components/BackButton";

import VerifyIdentityModal from "../../Components/Modals/VerifyIdentityModal"
import AddBankModal from "../../Components/Modals/AddBankModal"
import SecureDataStorage from '../../Libs/SecureDataStorage'
import BankManagementApi from '../../Services/BankManagementApi'
import CheckingAccountApi from '../../Services/CheckingAccountApi'
import BusinessApi from '../../Services/BusinessApi'
// import AccountApi from '../../Services/AccountApi'

import AccountSwitchModal from '../../Components/Modals/AccountSwitchModal'
import LoadingModal from '../../Components/Modals/LoadingModal'
import _ from 'underscore'
import Request from '../../Services/Request'
import AccountApi from '../../Services/AccountApi'
import Toast from 'react-native-simple-toast'
import RNRestart from 'react-native-restart';

// TODO: fix warning when doing account switch
YellowBox.ignoreWarnings(["Warning: Can't call setState"]);

export default class Accountscreen extends Component {
  animation;
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(0),
      setupProgress: 10,
      verifyModalOpen: false,
      addBankModalOpen: false,
      accountData: null,
      bankAccounts: undefined,
      userFunds: undefined,
      checkingAccountError: null,
      accounts: [],
      isUserFundLoading: true,
      isBankAccountsLoading: true,
      isAccountSwitchModalOpen: false,
      showLoading: false
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam("title", ""),
    headerTitleStyle: {
      flex: 1,
      color: "white",
      textAlign: "center",
      opacity: navigation.getParam("headerTextOpacity", 0)
    },
    headerStyle: {
      backgroundColor: "#0084FF",
      borderBottomColor: "transparent",
      elevation: 0,
      shadowColor: "transparent",
      shadowOpacity: 1
    },
    header: navigation.getParam("header", undefined),
    headerTintColor: "rgba(0,0,0,1)",
    headerLeft: (
      <TouchableOpacity style={headerStyles.shareButton} onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}>
        <Text style={headerStyles.shareText}>SHARE</Text>
      </TouchableOpacity>
    ),
    headerRight: (
      <BackButton
        onPress={() => {
          navigation.popToTop();
        }}
        arrow={"right"}
      />
    )
  });

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillMount() {
    this._isMounted = true;
    

    SecureDataStorage.getAuthAccount()
      .then(data => {
       
        if(this._isMounted) {
          this.setState({accountData: data.account});
          this.props.navigation.setParams({
            title: this.getUsername()
          });
        }
         
        if(data.account.type == 'business') {
          AccountApi.fetchSingleAccount(data.account.account_id)
            .then(res => {
              let data = this.state.accounts;
              data.push(res);

              if(this._isMounted){
               this.setState({accounts: data})
              }
            }).catch(error => {
              // TODO: handle this
            });
        }


      }).catch(error => {
        console.log(error)
      })
    
    this.getUserBankAccounts()
    this.getUserFunds()
    this.getBusinesses()
    
    this.props.navigation.setParams({
      headerTextOpacity: this.state.scrollY.interpolate({
        inputRange: [90, 150],
        outputRange: [0, 1],
        extrapolate: "clamp"
      })
    });
  }

  getUserBankAccounts(){
    BankManagementApi.fetchBankAccounts()
    .then(data => {
      if(this._isMounted){
        this.setState({bankAccounts: data, isBankAccountsLoading: false})
      }
    })
    .catch(error => {
      console.log('Banks didnt load at all')
      if(this._isMounted){
        this.setState({isBankAccountsLoading: false})
      }
      // Toast.show("Network problem, unable to get bank accounts. Try again later", Toast.LONG);
    })
  }

  getBusinesses = () => {
    
    BusinessApi.fetch()
      .then(res => {
        if(this._isMounted){

          if (res.length > 0) {

            SecureDataStorage.getAuthAccount()
            .then(authAccount => {
              let account = authAccount.account;
              let data = []

              if (account.type == 'business') {
                data = _.reject(res, (item) => {
                  return item.id == account.id;
                });

                let accounts = this.state.accounts;
                accounts = accounts.concat(data)
              
                this.setState({accounts: accounts});
              } else {
                let accounts = this.state.accounts;
                accounts = accounts.concat(res)
                
                this.setState({accounts: accounts})
              }

            }).catch(error => {
              // TODO: handle
              // alert(JSON.stringify(error))
            })
          }   
        }
      }).catch(error => {
        // TODO: handle
        
      })
  }

  getUserFunds(){
    CheckingAccountApi.fetch()
    .then(data => {
      if(this._isMounted){
        this.setState({userFunds: data, isUserFundLoading: false})
      }
    })
    .catch(error => {
      if(this._isMounted){
        this.setState({
          isUserFundLoading: false,
          checkingAccountError: error
        })
      }
    })
  }

  //temp
  getFullName = () => {
    let a = this.state.accountData;
    console.log(a)
    if(a && a.type == 'business') {
      return a.name;
    } else if (a && a.type == 'personal') {
      return a != null ? a.profile.name.first_name + ' ' + (a.profile.name.last_name ? a.profile.name.last_name : '')  : '';
    } else {
      return '';
    }
  }

  getUsername = () => {
    let a = this.state.accountData;
    return a != null ? "@" + a.username : '';
  }

   onAccountItemPress = (account) => { 
    // this.props.navigation.setParams({ header: null });

    // let resetDesign = () => {
    //   this.props.navigation.setParams({ header: undefined });
    //   StatusBar.setBackgroundColor('#0084FF');
    //   StatusBar.setBarStyle('light-content');

    //   this.props.navigation.goBack(null);
    //   this.props.navigation.navigate('AccountScreen');
    // }
    

    if(this._isMounted){
      this.setState({
        isAccountSwitchModalOpen: false,
        // showLoading: true
      });
    }

    SecureDataStorage.getAuthAccount()
      .then(authAccount => {
        let opts = {
          type: account.extra.type,
          businessId: account.extra.id
        };

        Request.fetchNewToken(authAccount, opts)
          .then(res => {
            
           if (!_.isUndefined(account.extra.checkingAccountId)) {
              // Set Checking account
            SecureDataStorage.getCheckingAccounts()
            .then(cks => {
              let seed = cks[account.extra.checkingAccountId];
              SecureDataStorage.setSignerSeed(seed)
                .then(seedRes => {
                  // resetDesign();

                  StatusBar.setBackgroundColor('white');
                  StatusBar.setBarStyle('dark-content');

                  RNRestart.Restart();

                }).catch(error => {
                  // TODO: handle this
                })

            }).catch(error => {
              // TODO: handle this
            });
           } else {
            SecureDataStorage.removeSignerSeed()
              .then(done => {
                // resetDesign();
                RNRestart.Restart();
              }).catch(error => {
                // TODO: handle this
              });
           }

          })
      }).catch(error => {
        // TODO: handle error
      })
    
  }

  onAccountSwitchModalClosed = () => {
    if(this._isMounted){
      this.setState({
        isAccountSwitchModalOpen: false
      })
    }
  }

   renderAccountSwitchModal = () => {  
    return (
      <AccountSwitchModal
        style={{zIndex: 50}}
        isOpen={this.state.isAccountSwitchModalOpen} 
        onClosed={() => {
          this.onAccountSwitchModalClosed()
        }}
        data={this.state.accounts}
        onAccountItemPress={this.onAccountItemPress}
      />
    );
  }

  renderAvatar = () => {

    if (this.state.accounts.length > 0) {
      return(
      
        <View>  
           <TouchableOpacity
            onPress={() => {
              if(this._isMounted){
                this.setState({
                  isAccountSwitchModalOpen: true
                })
              }
              
            }}> 
            <Image
              source={require('../../Assets/Images/avatar.png')}
              style={headerStyles.avatar} />
            </TouchableOpacity>
            <View style={headerStyles.switchIconRow}>
                <Image
                  source={require('../../Assets/Images/switch_icon.png')}
                  style={headerStyles.switchIcon}/>
                <Text style={headerStyles.switchText}>Switch</Text>
            </View>
        </View>
      )
    } else {
      return (
        <View>     
          <TouchableOpacity
            onPress={() => {
              // SecureDataStorage.getAuthAccount()
              // .then(authAccount => {
              //   let opts = null;
              //   authAccount['account']['type'] = 'personal'
              //   Request.fetchNewToken(authAccount, opts)
              //     .then(res => {
              //       this.props.navigation.setParams({ header: undefined });
              //       StatusBar.setBackgroundColor('#0084FF');
              //       StatusBar.setBarStyle('light-content');
                    
              //       if(this._isMounted){
              //         this.setState({
              //           showLoading: false
              //         })
              //       }
        
              //       this.forceUpdate();
              //     })
              // }).catch(error => {
              //   // TODO: handle error
              // })
            }}>
            <Image
              style={headerStyles.avatar}
              source={require('../../Assets/Images/white_strawberry_icon.png')} />
          </TouchableOpacity>
        </View>
      );
    }
  };

  renderHeader = () => {
    return (   
      <View style={headerStyles.header}>
        <View style={headerStyles.headerContent}>
          {this.renderAvatar()}
        </View>
        <Text style={headerStyles.headerTitle}>{this.getFullName()}</Text>
        <Text style={headerStyles.headerSubtitle}>{this.getUsername() }</Text>
      </View>
    );
  };

  renderVerifyIdentityModal = () => {
    return (
      <VerifyIdentityModal
        onClosed={() => {
          this.props.navigation.setParams({ header: undefined });
          if (Platform.OS == 'android') {
            StatusBar.setBackgroundColor('#0084FF')
          }
          StatusBar.setBarStyle('light-content')
          if(this._isMounted){
            this.setState({ verifyModalOpen: false });
          }
        }}
        isOpen={this.state.verifyModalOpen}
      />
    );
  };

  renderAddBankModal = () => {
    return (
       this._isMounted && <AddBankModal
        onClosed={() => {
          this.props.navigation.setParams({ header: undefined });
          if (Platform.OS == 'android') {
            StatusBar.setBackgroundColor('#0084FF')
          }
          StatusBar.setBarStyle('light-content')
          
          if(this._isMounted){
            this.setState({ addBankModalOpen: false });
          }

          this.getUserBankAccounts()
        }}
        isOpen={this.state.addBankModalOpen}
      />
    );
  };

  createBankLine(bank){
    console.log(bank)
    let bankAcctNum = "Acct#" + bank.account_number
    let icon = undefined
    let bankLetter = bank.registered_bank.name.charAt(0)

    switch(bank.status.code) {
      case "verified":
        icon = require("../../Assets/Images/verified_blue_icon.png") 
        break;
      case "unable_to_verify":
        icon = require("../../Assets/Images/unable_to_verify_status_icon.png")
        break;
      default:
        icon = require("../../Assets/Images/unconfirmed_status_icon.png")
    }
    
    return (
      <TouchableOpacity
        key={bank.id}
        style={[blockStyles.listItem,blockStyles.bottomSeperator]}
        onPress={() => { this.props.navigation.navigate('BankScreen', {
            bankAccountData: bank,
            accountData: this.state.accountData
            })}
          }
      >
        <View style={[blockStyles.listItemLeft]}>
        <Text style={styles.bankLetterLabel}>{bankLetter}</Text>
          <View style={styles.leftAlignContent}>
            <Text style={blockStyles.listItemText}>{bank.registered_bank && bank.registered_bank.name}</Text>
            <Text style={blockStyles.listItemSubText}>{bankAcctNum}</Text>
          </View>
        </View>
        <View style={[blockStyles.listItemRight]}>
          <Image
              resizeMode={"contain"}
              style={blockStyles.leftAlignedIcon}
              source={icon}
            />                  
        </View>
      </TouchableOpacity>
    );
  }

  createFundLine(fund, index){
    let label = fund.asset.asset_code == undefined ? 'XLM' : fund.asset.asset_code  
    return (
      <TouchableOpacity
        key={index}
        style={[blockStyles.listItem,blockStyles.bottomSeperator, index == 0 ? { paddingTop: 0 } : null, index == (this.state.userFunds.length - 1 ) ? {paddingBottom: 0, marginBottom: 0} : null ]}
        onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
      >
        <View style={[blockStyles.listItemLeft]}>
          <Text style={styles.fundsLetterLabel}>{label}</Text>
          <Text style={blockStyles.listItemTextFunds}>{fund.asset.asset_name}</Text>
        </View>
        <View style={[blockStyles.listItemRightFunds]}>
          <Text style={blockStyles.listItemTextSecondary}>{fund.balance}</Text>                  
        </View>
      </TouchableOpacity>
    );
  }

  renderBankAccountErrorView(){
    return (
      <View style={[blockStyles.block, blockStyles.contentPadding,blockStyles.emptyBlock, styles.centerContent]}>
        <View
          style={[blockStyles.blockIconContainer,styles.centerContent]}>
          <Image
            resizeMode={"contain"}
            source={require("../../Assets/Images/bank_icon.png")}
            style={blockStyles.bankIcon}
          />
        </View>
        <TouchableOpacity style={[styles.centerContent]}>
          <Text style={blockStyles.blockText}>
            Unable to load your bank accounts
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderBlockLoading = () => {
    return(
      <View style={[blockStyles.block, blockStyles.contentPadding,blockStyles.emptyBlock, styles.centerContent]}>
        <View style={[styles.centerContent]}>
          <ActivityIndicator size="large" color="#0084FF" />
        </View>
      </View>
    )
  }

  renderUserFundsErrorView(){
    return (
      <View style={[blockStyles.block, blockStyles.contentPadding,blockStyles.emptyBlock, styles.centerContent]}>
        <View
          style={[blockStyles.blockIconContainer,styles.centerContent]}
        >
          <Image
            resizeMode={"contain"}
            source={require("../../Assets/Images/dollar_icon.png")}
            style={blockStyles.dollarCircleIcon}
          />
        </View>
        <View style={[styles.centerContent]}>
          <Text style={blockStyles.blockText}>
            Unable to load your funds
          </Text>
        </View>
      </View>
    );
  }

  renderUserBankAccountsView(){
    return(
      <View style={[blockStyles.block, styles.contentPadding]}>
        {this.state.bankAccounts.bank_accounts.map((bank) => {return (this.createBankLine(bank))})}
        <TouchableOpacity
          style={[blockStyles.listItem]}
          onPress={() => {
            this.props.navigation.setParams({ header: null });
            if (Platform.OS == 'android') {
              StatusBar.setBackgroundColor('#ffffff')
            }
            StatusBar.setBarStyle('dark-content')
            if(this._isMounted){
              this.setState({ addBankModalOpen: true });
            }
          }}>
          <View style={[blockStyles.listItemLeft]}>
            <Image
              resizeMode={"contain"}
              style={blockStyles.leftAlignedIcon}
              source={require("../../Assets/Images/add_circle_icon.png")}
            />
            <Text style={blockStyles.listItemTextPrimary}>Add another bank account</Text>
          </View>                 
          
        </TouchableOpacity>
      </View>
    );
  }


  renderUserFundsView(){
    return(
      <View style={[blockStyles.block, styles.contentPadding]}>
        {this.state.userFunds.funds.map((fund,index) => {return (this.createFundLine(fund,index))})}
      </View>
    );
  }

  renderCreateCheckingAccountView = () => {
    return (
      <View style={[blockStyles.block, blockStyles.contentPadding,blockStyles.emptyBlock, styles.centerContent]}>
        <View
          style={[blockStyles.blockIconContainer,styles.centerContent]}
        >
          <Image
            resizeMode={"contain"}
            source={require("../../Assets/Images/wallet_icon.png")}
            style={blockStyles.walletIcon}
          />
        </View>
        <TouchableOpacity 
          style={[styles.centerContent]}
          onPress={() => {
            this.onCheckingAccountCreatePress();
          }}>
          <Text style={blockStyles.blockLink}>
            Create a checking account
          </Text>
          <Text style={blockStyles.blockInstructions}>
            Keep all your funds in a secure place
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  onCheckingAccountCreatePress = () => {
    if(this._isMounted) {
      this.setState({isUserFundLoading: true});
    }
    
    // Create new checking account
    CheckingAccountApi.create()
      .then(res => {
        this.getUserFunds();
        Toast.show('Checking account created.', Toast.LONG);
      }).catch(error => {
        if(this._isMounted) {
          this.setState({isUserFundLoading: false});
        }
        Toast.show('Could not create checking account.', Toast.SHORT);
      });
  }


  renderAddNewBankAccountView(){
    return (
      <View style={[blockStyles.block, blockStyles.contentPadding,blockStyles.emptyBlock, styles.centerContent]}>
        <View
          style={[blockStyles.blockIconContainer,styles.centerContent]}
        >
          <Image
            resizeMode={"contain"}
            source={require("../../Assets/Images/bank_icon.png")}
            style={blockStyles.bankIcon}
          />
        </View>
        <TouchableOpacity 
          style={[styles.centerContent]}
          onPress={() => {
            this.props.navigation.setParams({ header: null });
            if (Platform.OS == 'android') {
              StatusBar.setBackgroundColor('#ffffff')
            }
            StatusBar.setBarStyle('dark-content')
            if(this._isMounted){
              this.setState({ addBankModalOpen: true });
            }
          }}>
          <Text style={blockStyles.blockLink}>
            Add a bank account
          </Text>
          <Text style={blockStyles.blockInstructions}>
            Tranfer funds from and to your bank account
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderBankAccountsBlock(){   
    let a = this.state.accountData
    if(a && a.type == 'business') {
      return (
        <View style={blockStyles.container}>
          <Text style={[blockStyles.containerTitle, styles.contentPadding]}>
            MY BANK ACCOUNTS
          </Text>
          {this.renderBankAccountsBlock()}
        </View>
      )
    }
  }

  renderBankAccountsBlockBody(){
    if(this.state.isBankAccountsLoading) {
      return this.renderBlockLoading()
    } else if(this.state.bankAccounts == undefined){
      return this.renderBankAccountErrorView()
    }      
    else if(this.state.bankAccounts.bank_accounts.length > 0){
      return this.renderUserBankAccountsView()
    }else{
      return this.renderAddNewBankAccountView()
    }
  }

  renderUserFundsBlock(){
    if(this.state.isUserFundLoading) {
      return this.renderBlockLoading()
    } else if(this.state.userFunds == undefined){

      if (this.state.checkingAccountError && 
          this.state.checkingAccountError.http_status && this.state.checkingAccountError.http_status == 400) {
            return this.renderCreateCheckingAccountView()
          } else {
            return this.renderUserFundsErrorView()
          }
    }  else if(this.state.userFunds.funds.length > 0){
      return this.renderUserFundsView()
    }
  }

  render() {
    return (
      <View style={styles.container}>
         
        <StatusBar
          translucent={false}
          barStyle={"light-content"}
          backgroundColor={"#0084FF"}
        />

        {this.renderVerifyIdentityModal()}
        {this._isMounted && this.renderAddBankModal()}
        {this.renderAccountSwitchModal()}
        <LoadingModal showLoading={this.state.showLoading} />
        <ScrollView
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],

            {
              useNativeDriver: false
            }
          )}
          onEndReachedThreshold={0.5}
          scrollEventThrottle={1}
        >
          {this.renderHeader()}

          <View style={styles.scrollViewBody}>
            {/*<View
              style={[styles.accountProgressContainer, styles.contentPadding]}
            >
              <Text style={styles.progressText}>ACCOUNT SETUP PROGRESS</Text>
              <View style={styles.progressBar}>
                <View style={[styles.progress, { width: "20%" }]} />
              </View>
            </View>
            */}
                        
            {/*FUNDS BLOCK */}
            <View style={blockStyles.container}>
              <Text style={[blockStyles.containerTitle, styles.contentPadding]}>
                MY FUNDS
              </Text>
              {this.renderUserFundsBlock()}              
            </View>
            {/* Bank Accounts Block */}
            {this.renderBankAccountsBlock()}
            
            {/* Blockchain Block */}
            {/* <View style={blockStyles.container}>
              <Text style={[blockStyles.containerTitle, styles.contentPadding]}>
                MY BLOCKCHAIN ACCOUNT
              </Text>

              <View style={[blockStyles.block, styles.contentPadding]}>
                <TouchableOpacity
                  style={[blockStyles.listItem, blockStyles.bottomSeperator]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemText}>
                    Account Information
                  </Text>

                  <Image
                    resizeMode={"contain"}
                    style={blockStyles.generalIcon}
                    source={require("../../Assets/Images/account_box_icon.png")}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[blockStyles.listItem, blockStyles.bottomSeperator]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemText}>
                    Configuration
                  </Text>

                  <Image
                    resizeMode={"contain"}
                    style={blockStyles.generalIcon}
                    source={require("../../Assets/Images/settings_icon.png")}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[blockStyles.listItem]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemText}>
                    Security
                  </Text>

                  <Image
                    resizeMode={"contain"}
                    style={blockStyles.generalIcon}
                    source={require("../../Assets/Images/lock_icon.png")}
                  />
                </TouchableOpacity>
              </View>
            </View> */}
            {/* Business Block */}
            {/*<View style={blockStyles.container}>
              <Text style={[blockStyles.containerTitle, styles.contentPadding]}>
                BUSINESS
              </Text>

              <View
                style={[
                  blockStyles.block,
                  blockStyles.emptyBlock,
                  styles.contentPadding
                ]}
              >
                <View style={[blockStyles.emptyContent, styles.centerContent]}>
                  <View
                    style={[
                      blockStyles.blockIconContainer,
                      styles.centerContent
                    ]}
                  >
                    <Image
                      resizeMode={"contain"}
                      source={require("../../Assets/Images/business_icon.png")}
                      style={blockStyles.bankIcon}
                    />
                  </View>
                  <TouchableOpacity onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}>
                    <Text style={blockStyles.blockLink}>
                      Add a business account
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
                  </View>*/}
            <View style={blockStyles.container}>
              <Text style={[blockStyles.containerTitle, styles.contentPadding]}>
                SECURITY
              </Text>

              <View style={[blockStyles.block, styles.contentPadding]}>
                <TouchableOpacity
                  style={[blockStyles.listItem, blockStyles.bottomSeperator]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemText}>
                    Enable App Access Lock
                  </Text>

                  <Image
                    style={[styles.redNoticeIcon]}
                    source={require("../../Assets/Images/red_notice_icon.png")}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[blockStyles.listItem]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemText}>
                    Add Email Account Backup
                  </Text>

                  <Image
                    style={[styles.redNoticeIcon]}
                    source={require("../../Assets/Images/red_notice_icon.png")}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={blockStyles.container}>
              <Text style={[blockStyles.containerTitle, styles.contentPadding]}>
                SETTINGS
              </Text>

              <View style={[blockStyles.block, styles.contentPadding]}>
                <TouchableOpacity
                  style={[blockStyles.listItem, blockStyles.bottomSeperator]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemText}>Personal</Text>

                  <Image
                    resizeMode={"contain"}
                    style={blockStyles.generalIcon}
                    source={require("../../Assets/Images/person_icon.png")}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[blockStyles.listItem]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemText}>Privacy</Text>

                  <Image
                    resizeMode={"contain"}
                    style={blockStyles.generalIcon}
                    source={require("../../Assets/Images/lock_icon.png")}
                  />
                </TouchableOpacity>
              </View>
            </View>
            {/* <View style={blockStyles.blockGap}></View> */}
            <View style={blockStyles.container}>
              <Text
                style={[blockStyles.containerTitle, styles.contentPadding]}
              />

              <View style={[blockStyles.block, styles.contentPadding]}>
                <TouchableOpacity
                  style={[
                    blockStyles.listItem,
                    styles.centerContent,
                    blockStyles.bottomSeperator
                  ]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemTextPrimary}>Support</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[blockStyles.listItem, styles.centerContent]}
                  onPress={() => { Alert.alert('Not available', 'Sorry this feature is not available right now.') }}
                >
                  <Text style={blockStyles.listItemTextPrimary}>Sign Out</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={blockStyles.container}>
              <View
                style={[
                  blockStyles.emptyBlock,
                  blockStyles.transparentBg,
                  styles.centerContent,
                  styles.contentPadding
                ]}
              >
                <Text style={styles.copyrightText}>
                  Copyright &copy; Oblip, Inc
                </Text>
                <Text style={styles.copyrightText}>
                  v0.5.10
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  shareButton: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,

    paddingTop: 10
  },
  shareText: {
    color: "white",

    fontWeight: "500",

    fontSize: 14,

    textDecorationLine: "underline"
  },
  header: {
    paddingTop: 16,
    paddingBottom: 16,

    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",

    backgroundColor: "#0084FF",

    flexDirection: "column"
  },
  headerTitle: {
    fontWeight: "bold",

    fontSize: 16,

    color: "#FFFFFF",

    marginTop: 10
  },
  headerSubtitle: {
    marginTop: 3,

    color: "rgba(255, 255, 255, 0.71)",

    fontWeight: "500",

    fontSize: 12
  },
  headerAvatarImage: {
    width: 60,
    height: 60,
    borderRadius: 32
  },
  avatar: {
    width: 60,
    height: 60,
  },
  switchIconRow: {
    flex: 1,
    flexDirection: "row", 
    position: "absolute", 
    top: "70.2%",
    left: "83.56%"
  },
  switchIcon: {   
    width: 20,
    height: 20, 
  },
  switchText: {  
   paddingTop: 7,  
   paddingRight: 4,
   fontSize: 6, 
   color: "white", 
  },
 verifyButton: {
    borderWidth: 3,
    borderColor: "white",
    backgroundColor: "rgba(247,248,255,0.67)",
    width: 60,
    height: 60,
    borderRadius: 30,
    justifyContent: "center"
  },
  verifyButtonText: {
    fontSize: 10,
    color: "black",
    fontWeight: "500",
    textAlign: "center"
  },
  verificationIcon: {
    position: "absolute",
    right: 0,
    top: -2,
    zIndex: 5
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F0F3F7"
  },
  scrollViewBody: {
    flex: 1
  },
  copyrightText: {
    color: "rgba(0,0,0,0.5)",
    fontSize: 12
  },
  redNoticeIcon: {
    width: 20,
    height: 20
  },
  centerContent: {
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  leftAlignContent: {
    alignItems: "flex-start"
  },
  contentPadding: {
    paddingLeft: 18,
    paddingRight: 18,
    paddingTop: 14,
    paddingBottom: 14
  },
  iosVerifyButton: {
    paddingLeft: 15,
    paddingBottom: 0,
    paddingTop: 12.5
  },
  iosVerifyText: {
    color: "#0084FF",
    fontSize: 14,
    fontWeight: "500"
  },

  accountProgressContainer: {
    height: 68,

    backgroundColor: "white",

    flexDirection: "column"
  },
  progressText: {
    fontWeight: "500",
    fontSize: 9,
    color: "#ABAFB6",
    marginBottom: 3
  },
  progressBar: {
    borderColor: "rgba(238,238,238, 0.5)",
    borderWidth: 1,
    width: "100%",
    height: 18,
    borderRadius: 8
  },
  progress: {
    height: "100%",

    width: 100,

    backgroundColor: "#0084FF",

    borderRadius: 8
  },
  bankLetterLabel:{
    marginRight: 10,
    paddingTop:7,
    paddingBottom:2,
    paddingLeft: 13,
    paddingRight: 13,
    //padding: 10,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: "#DFE2E4",
    textAlign: "center",
    justifyContent: "center"
  },
  fundsLetterLabel:{
    marginRight: 10,
    paddingTop:12.2,
    paddingBottom:11.2,
    paddingLeft: 9.2,
    paddingRight: 9.2,
    //padding: 10,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: "#DFE2E4",
    textAlign: "center",
    justifyContent: "center",
    fontSize: 10
  },
  loading: { 
    width: '100%',
    height: '100%'
  },
});

const blockStyles = StyleSheet.create({
  container: {
    marginTop: 5,
    marginBottom: 5
  },
  containerTitle: {
    color: "#ABAFB6",
    fontWeight: "500",
    fontSize: 12
  },

  block: {
    backgroundColor: "white"
  },

  emptyBlock: {
    minHeight: 173
  },

  blockIconContainer: {
    width: 41,
    height: 41,
    borderRadius: 20,
    padding: 10,
    backgroundColor: "#F4F6FA",
    marginBottom: 10
  },
  plusIcon: {
    width: 18,
    height: 18
  },
  dollarCircleIcon: {
    width: 32,
    height: 32
  },
  bankIcon: {
    width: 20,
    height: 19
  },
  walletIcon: {
    width: 34,
    height: 34
  },

  leftAlignedIcon: {
    width: 28,
    height: 28, 
    marginRight: 20
  },
  generalIcon: {
    width: 24,
    height: 24
  },
  blockLink: {
    color: "#0084FF",

    fontSize: 16,

    fontWeight: "500"
  },
  blockText: {
    color: "#ABAFB6",
    fontSize: 16
  },
  blockTitle: {
    color: "black",

    fontSize: 16,

    marginBottom: 5
  },
  blockInstructions: {
    color: "rgba(0,0,0,0.5)",
    fontSize: 14,
    textAlign: "center"
  },
  emptyContent: {
    marginTop: 20
  },

  transparentBg: {
    backgroundColor: "transparent"
  },
  listItemColumn: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "column",

    paddingTop: 15,
    paddingBottom: 15,

    alignContent: "center",
    justifyContent: "space-between",
    //alignContent: "stretch"
  },
  listItem: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    paddingTop: 10,
    paddingBottom: 10,
    alignContent: "center",
    justifyContent: "space-between",
    // marginBottom: 10
    //alignContent: "stretch"
  },
  listItemLeft: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-start"
    //paddingTop: 15,
    //paddingBottom: 15,

  },
  listItemRight: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-end"
    //paddingTop: 15,
    //paddingBottom: 15,

  },
  listItemRightFunds: {
    // borderColor: 'red', borderWidth: 1,
    flexDirection: "row",
    alignContent: "flex-end",
    paddingTop: 8,
    //paddingBottom: 15,

  },
  listItemText: {
    fontSize: 16,

    color: "#000000"
  },
  listItemTextFunds: {
    fontSize: 16,
    color: "#000000",
    paddingTop: 8
  },
  listItemSubText: {
    fontSize: 12,
    color: 'rgba(0,0,0,0.50)'
  },
  listItemTextPrimary: {
    fontSize: 16,

    color: "#0084FF",

    textAlign: "center"
  },
  listItemTextSecondary: {
    fontSize: 16,

    color: "#0084FF",

    textAlign: "center"
  },
  blockGap: {
    height: 60
  },
  bottomSeperator: {
    borderBottomColor: "#EEEEEE",

    borderBottomWidth: 1
  }
});
