import React, { Component } from 'react';
import {  
  View,
  YellowBox,
  Vibration,
  Image,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Platform,
  Alert
} from 'react-native'
import Text from '../../Components/globalTextStyle';

import * as Animatable from 'react-native-animatable';
import accounting from 'accounting-js'
import TransactionReviewModal from '../../Components/Modals/TransactionReviewModal'
import SelectItemModal from '../../Components/Modals/SelectItemModal'
import _ from 'underscore' 
import cryptoRandomString from 'crypto-random-string'
import BlockchainManagementApi from '../../Services/BlockchainManagementApi'
import TransactionApi from '../../Services/TransactionApi'
import AccountApi from '../../Services/AccountApi'
import LoadingModal from '../../Components/Modals/LoadingModal'
import FayeService from '../../Services/FayeService'
import moment from 'moment'
import Toast from 'react-native-simple-toast'

const initialAmountFontSize = 100;
const initialCentFontSize = 30;
const buttonElevation = 2;

export default class TransactionScreen extends Component {
  constructor (props) {
    super(props)
    this.amountTextList = [];

    this.state = {
      amount: "0",
      message: '',
      isTransactionReviewModalOpen: false,
      isTransactionReviewLoading: true,
      isSelectFundModalOpen: false,
      type: 'payment',
      fundsData: [],
      account: null,
      showLoadingModal: false
    }

    try {
      this.extraData = this.props.navigation.state.params.extraData || {};
      this.extraData.username = this.props.navigation.getParam('username', _.isEmpty(this.extraData) ? undefined : this.extraData.username)

      // if(_.isEmpty(this.extraData)) {
      //   this.extraData.username = this.props.navigation.getParam('username', undefined)
      // }

      if(!this.extraData.username) {
        // TODO: close screen
        Toast.show('A username is required', Toast.LONG);
        this.props.navigation.goBack()
      }

    } catch(e) {
      // TODO: handle this
      console.log(e)
      Toast.show('A username is required', Toast.LONG);
      this.props.navigation.goBack()
    }
    
  }

  static navigationOptions = ({navigation}) => ({
    title: '',
    headerStyle: {
      backgroundColor: 'white',
      borderBottomColor: 'transparent',
      elevation: 0,
      shadowColor: 'transparent',
      shadowOpacity: 1,
    },
    headerTintColor: 'rgba(0,0,0,1)',
    headerRight: <View></View>,
    header:  _.isUndefined(navigation.state.params) ? undefined : navigation.state.params.header,
  })

  componentWillMount() {
    this.props.navigation.setParams({
      header: undefined
    });

    if(this.extraData && this.extraData.action == 'request_review') {
      this.props.navigation.setParams({
        header: null
      });
  
      this.setState({
        isTransactionReviewModalOpen: true
      });
    }

    BlockchainManagementApi.fetchUserFunds()
      .then(data => {
        this.setUserFunds(data);
      })
      .catch(error => {
        // TODO: handle
      });

    if(this.extraData && this.extraData.username) {
      AccountApi.fetchBasic(this.extraData.username)
        .then(res =>{
          this.setState({account: res})
        }).catch(error => {
          // TODO: handle
        })
    }

    this.setParameters();
  }

  setTransactionSubscriber = (identifier) => {
    if (FayeService.getClient()) {
      FayeService.getClient().subscribe(`/${identifier}`,
      (msgData) => {
        console.log(msgData)
  
        FayeService.getClient().unsubscribe(`/${identifier}`)
  
        this.props.navigation.setParams({
          header: undefined
        });
  
        this.setState({showLoadingModal: false})
  
        if (!msgData.http_status) {
          let extraData = {
            name: this.state.account.name,
            isSuccess: true,
            amount: msgData.amount,
            asset: msgData.asset,
            transactionId: msgData.id,
            date: moment().format('DD MMMM YYYY, LT')
          }
  
          Vibration.vibrate(500);
  
          this.props.navigation.navigate('TransactionResultScreen', { extraData: extraData });
  
        } else {
          let extraData = {
            isSuccess: false,
            errorMessage: 'Unable to process the transaction.'
          }
  
          this.props.navigation.navigate('TransactionResultScreen', { extraData: extraData });
        }
  
      }
     );
    }
  }

  componentWillUnmount() {
    if (FayeService.getClient()) {
      FayeService.getClient().unsubscribe(`/${this.state.identifier}`);
    }
  }

  setParameters = () => {
    if (this.extraData) {
      const amount = this.extraData.amount;
      const type = this.extraData.type;
      let identifier = this.extraData.identifier || cryptoRandomString({length: 32});
      identifier = identifier.replace(/\(.+?\)/g, '-');
      const action = this.extraData.action
      const username = this.extraData.username
      

      this.setTransactionSubscriber(identifier);


      this.setState({
        amount: amount || this.state.amount,
        type: type || this.state.type,
        username: username,
        identifier: identifier 
      });

      if (action && action == 'request_review') {
        // Get envelope;
        this.setTransactionEnvelope();
      }
    }
  }

  setTransactionEnvelope = () => {
    const data = {
      to: this.extraData.username,
      amount: this.state.amount != "0" ? this.state.amount : (this.extraData && this.extraData.amount ? this.extraData.amount : "0") ,
      type: this.extraData && this.extraData.type ? this.extraData.type :  this.state.type,
      asset: this.state.currentFund ? this.state.currentFund.code : (this.extraData &&  this.extraData.asset ? this.extraData.asset : 'BZD')
    }

    TransactionApi.getEnvelope(data)
      .then(res => {
        this.setState({
          transactionEnvelope: res.envelope,
          isTransactionReviewLoading: false
        })
      }).catch(error => {
        // TODO: handle this

        this.props.navigation.setParams({
          header: undefined
        });
        
        this.setState({
          isTransactionReviewModalOpen: false,
        });

        Alert.alert('Something went wrong', 'It looks like something went wrong. Try again.')
      })
  }

  setUserFunds(funds){
    let allFunds = _.reject(funds.funds, (item) => {
      return item.asset.asset_code == 'XLM'
    });

    allFunds = allFunds.map((fund) => {
        return {
          value: fund.balance,
          title: fund.asset.asset_name,
          subtitle: fund.balance,
          code: fund.asset.asset_code
        }  
    });

    let selectedFund = allFunds[0]

    if(this.extraData && this.extraData.asset) {
      selected = _.find(allFunds, (item) => {
        return item.code == this.extraData.asset
      });

      if (selected) {
        selectedFund = selected;
      } else {
        this.setState({
          isTransactionReviewModalOpen: false,
        });

        Alert.alert('Invalid Asset', `${this.extraData.asset} is an invalid asset. Please select a valid fund.`)
      }

      this.setState({
        fundsData: allFunds,
        currentFund: selectedFund
      });


    } else {
      this.setState({
        fundsData: allFunds,
        currentFund: selectedFund
      });
    }
  }


  renderSelectFundModal = () => {
    return(
      <SelectItemModal
        isOpen={this.state.isSelectFundOpen}
        onClosed={() => { this.onSelectFundModalClosed() }}
        data={this.state.fundsData}
        onItemSelected={(item) => {
          this.setState({
            currentFund: item,
            isSelectFundOpen: false
          })
        }}
        currentItem={this.state.currentFund}
        title={'Select Fund'} />
    )
  }


  _addValueToAmount = (value) => {
    let currentValue = this.state.amount.toString()
    let formattedAmount = accounting.formatNumber(currentValue, { precision: 0 })

    // don't allow more than one dot 
    if (value == '.' && currentValue.includes('.')) { return; }

    // only allow two values for cents.
    if(currentValue.includes('.')) {
      let cents = currentValue.split('.')[1]
      if(cents.length == 2) { return; }
    }

    // limit the user from entering more than 6 digits (not including cents)
    if(currentValue.split('.')[0].length >= 6 && value != '.' && (!currentValue.includes('.'))) {  
      Vibration.vibrate(800); this.amountContainer.shake(500)
      return; 
    }

    if(currentValue == '0') {
      if(value == '.') {
        let newValue = currentValue + '' + value
        this.setState({amount:  newValue})
      } else {
        this.setState({amount: value});
      }
    } else {
      let newValue = currentValue + '' + value
      this.setState({amount:  newValue})
    }

    // this.amountText.fadeInDownBig(500)
  
  }

  _removeValueFromAmount = () => {
    let currentValue = this.state.amount.toString()
    let formattedAmount = accounting.formatNumber(currentValue, { precision: 0 })
    
    if(currentValue.length > 1) {
      let amountToRemove = 1

      if (currentValue.endsWith('.')) {
         amountToRemove = 2 
         this.amountTextList[formattedAmount.length - 1].fadeOutDownBig(500)
      }
      
      if(!currentValue.includes('.')) {
        this.amountTextList[formattedAmount.length - 1].fadeOutDownBig(500)
      }
      
      setTimeout(() => {
        this.setState({amount: currentValue.substr(0, currentValue.length - amountToRemove)})
      }, 200)
    } else {
      this.setState({amount: '0'})
    }

  }
  
  _renderAmountTexts = (amountString) => {
    let amountFontSize = initialAmountFontSize - (6 * (amountString.length - 1))
    allText = _.map(amountString, (value, i) => {
      return (
        <Animatable.Text
          key={i}
          animation={'fadeInDownBig'}
          duration={500}
          ref={(ref) => {
            this.amountTextList[i] = ref
          }}
          style={[styles.amountText, { fontSize: amountFontSize}]}>
            {value}
        </Animatable.Text>
      )
    })

    return allText
  }

  renderFormattedAmount = () => {
    let currentValue = this.state.amount.toString()
    let values = currentValue.split('.')

    let amount = ''
    let cents = ''
    amount = values[0]
    
    if (values.length > 1) { cents = values[1] }

    // let amountFontSize = initialAmountFontSize - (6 * (amount.length - 1))
    let centFontSize = initialCentFontSize - (3 * (amount.length - 1))

    formattedAmount = accounting.formatNumber(amount, { precision: 0 })
    return (
      <Animatable.View ref={(ref) => this.amountContainer = ref} useNativeDriver={true} style={styles.amountWrapper}>
        <Text style={styles.amountCurrencySymbol}>$</Text>

        {this._renderAmountTexts(formattedAmount)}
        {/* <Animatable.Text 
          ref={(ref) => this.amountText = ref} 
          animation={'fadeInDown'}
          style={[styles.amountText, { fontSize: amountFontSize}]}>
            {accounting.formatNumber(amount, { precision: 0 })}
        </Animatable.Text> */}
        <Text style={[styles.amountCentsText, {fontSize: centFontSize}]}>{cents}</Text>
      </Animatable.View>
    )
  }

  onTransactionReviewModalClosed = () => {
    if(!this.state.showLoadingModal) {
      this.props.navigation.setParams({
        header: undefined
      });
    }

    this.setState({
      isTransactionReviewModalOpen: false,
      isTransactionReviewLoading: true
    })
  }

  onSelectFundModalOpen = () => {
    this.props.navigation.setParams({
      header: null
    });
    
    this.setState({
      isSelectFundOpen: true
    })
  }

  onSelectFundModalClosed(){
    this.props.navigation.setParams({
      header: undefined
    });
    this.setState({isSelectFundOpen: false})
  }

  onPayButtonPressed = () => {
    this.setTransactionEnvelope();

    this.props.navigation.setParams({
      header: null
    });

    this.setState({
      isTransactionReviewModalOpen: true
    });

  }

  onConfirmation = () => {
    this.props.navigation.setParams({
      header: null
    });

    this.setState({showLoadingModal: true, isTransactionReviewModalOpen: false, isTransactionReviewLoading: true});

    let data = {
      envelope: this.state.transactionEnvelope,
      type: this.state.type,
      message: this.state.message,
      identifier: this.state.identifier
    }

    TransactionApi.create(data)
      .then(res => {
         Toast.show('Processing', Toast.LONG);
      }).catch(error => {
        this.props.navigation.setParams({
          header: undefined
        });

        this.setState({showLoadingModal: false})
      })

   
    
    // this.props.navigation.navigate('TransactionResultScreen', { extraData: extraData });
  }
  onRequestHeaderOpen = () => {
    this.props.navigation.setParams({
      header: null
    });
    
    this.setState({isSelectFundOpen: true})
  }


  renderHeader = () => {
    let asset = this.state.currentFund != undefined ?  +parseFloat(this.state.currentFund.value).toFixed(2).toString() + ' ' + this.state.currentFund.code : 'loading...'
    return(
      <View>
      <Text style={styles.fundSubtitle}>SELECTED FUND</Text>
        <TouchableOpacity
          onPress={() => { this.onRequestHeaderOpen() }}
          style={[styles.button, {elevation: this.state.isConfirmationModalOpen || this.state.isSelectFundOpen  || this.state.isTransactionReviewModalOpen  || this.state.showLoadingModal? 0 : buttonElevation}]}>
          <Text style={styles.AssetSelectText}>{asset}</Text>
          <View style={styles.centerContent}>
            <Image
              source={require('../../Assets/Images/arrow_drop_down_icon.png')}
              resizeMode={'contain'}
              style={styles.drop_down_icon2} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }


  render () {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={'white'}
          translucent={false}
          barStyle={'dark-content'}
        /> 

        <LoadingModal showLoading={this.state.showLoadingModal} />

        <TransactionReviewModal
          isOpen={this.state.isTransactionReviewModalOpen} 
          extraData={{
            name: 'reggie',
            asset: this.state.currentFund ? this.state.currentFund.code : '',
            amount: this.state.amount,
            type: this.state.type,
            message: this.state.message,
            account: this.state.account,
          }}
          onMessageChanged={(message) => {
            this.setState({message: message})
          }}
          isLoading={this.state.isTransactionReviewLoading}
          onConfirmation={this.onConfirmation}
          onClosed={() => { this.onTransactionReviewModalClosed() }}
          />
        { this.renderSelectFundModal() }
        <View style={styles.fundContainer}>
          { this.renderHeader() }
        </View>

      

        <View style={styles.amountContainer}>
          {this.renderFormattedAmount()}
        </View>

        {/* TODO: Refactor this; use loop? */}
        <View style={keyboardStyles.container}>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('1')}>
              <Text style={keyboardStyles.keyText}>1</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('2')}>
              <Text style={keyboardStyles.keyText}>2</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('3')}>
              <Text style={keyboardStyles.keyText}>3</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('4')}>
              <Text style={keyboardStyles.keyText}>4</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('5')}>
              <Text style={keyboardStyles.keyText}>5</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('6')}>
              <Text style={keyboardStyles.keyText}>6</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('7')}>
              <Text style={keyboardStyles.keyText}>7</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('8')}>
              <Text style={keyboardStyles.keyText}>8</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('9')}>
              <Text style={keyboardStyles.keyText}>9</Text>
            </TouchableOpacity>
          </View>
          <View style={keyboardStyles.keyRow}>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('.')}>
              <Text style={[keyboardStyles.keyText]}>·</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._addValueToAmount('0')}>
              <Text style={keyboardStyles.keyText}>0</Text>
            </TouchableOpacity>
            <TouchableOpacity style={keyboardStyles.keyButton} onPress={() => this._removeValueFromAmount()}>
              <Image
                source={require('../../Assets/Images/ios_back_icon.png')}
                resizeMode={'contain'}
                style={keyboardStyles.deleteIcon}
                />
            </TouchableOpacity>
          </View>
        </View>

        <View style={[styles.buttonContainer, {elevation: this.state.isTransactionReviewModalOpen || this.state.showLoadingModal || this.state.isSelectFundOpen ? 0 : buttonElevation}]}>
          <TouchableOpacity
            onPress={() => this.onPayButtonPressed()}
            style={[styles.button, styles.generalButtonPrimary, styles.payButton]}>  
            <Text style={styles.buttonPrimaryTitle}>Pay @{this.extraData.username}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
    }

}

const keyboardStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    marginTop: 5,
    paddingLeft: 20,
    paddingRight: 20
  },

  deleteIcon: {
    width: 12,
    height: 21
  },
  keyButton: {
    // borderColor: 'red', borderWidth: 1,
    flex: 1,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
    padding: 10
  },
  keyText: {
    fontSize: 27,
    color: 'rgba(0,0,0,0.5)',
    fontWeight:  Platform.OS == 'ios' ? '400' : '300'
  },
  keyRow: {
    flexDirection: 'row',
    width: '100%',
    paddingBottom: 10,
    // borderColor: 'blue', borderWidth: 1,
    justifyContent: 'space-around', 
    alignContent: 'center', alignItems:'center',
  }
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  generalButtonTitlePrimary: {
    color: 'white'
  },
  buttonAvatar: {
    width: 24,
    height: 24,
    marginRight: 10
  },
  generalButtonPrimary: {
    backgroundColor: '#0084FF',
  },
  amountContainer: {
    marginTop: 10,
    marginBottom: 10,
    height: 120,
    // borderColor: 'purple', borderWidth: 2,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  amountWrapper: {
    flexDirection: 'row',
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  amountCurrencySymbol: {
    fontSize: 30,
    marginTop: 20,
    color: 'black',
    fontWeight: '100',
    alignSelf: 'flex-start',
  },

  amountCentsText: {
    fontSize: initialCentFontSize,
    color: 'black',
    fontWeight: '300',
    alignSelf: 'flex-start'
  },
  amountText: {
    fontSize: initialAmountFontSize,
    color: 'black'
  },
  buttonContainer: {
    height: 75,
    marginBottom: 20,
    alignContent: 'center', alignItems:'center',  justifyContent: 'center',
  },

  buttonPrimaryTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: 'white'
  },

  
  payButton: {
    paddingLeft: 50, paddingRight: 60,
    height: 55,
    minWidth: 100, 
  },

  fundContainer: {
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },

  drop_down_icon2: {
    width: 10, height: 5,
    tintColor: 'rgba(0,0,0,0.54)',
    position: 'absolute',
    right: -22,
    top: -3
  },

  AssetSelectText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'black'
  },

  fundSelectText: {
    fontSize: 12, 
    left: 12,
    fontWeight: 'bold',
    color: 'black'
  },
  arrowDropDown: {
    height: 18, 
    // fontSize: 14,
    width:18,
    marginLeft:5,
    left: 9,
    // tintColor: '#727272'
    tintColor: 'black'
  },
  fundsButton: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 35,
    minWidth: 50, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    paddingRight: Platform.OS == 'ios' ? 20 : 15,
    paddingLeft: Platform.OS == 'ios' ? 20 : 15,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
    
  },

  button: {
    alignItems: 'center',
    flexDirection: 'row',
    minHeight: 45,
    minWidth: 50, 
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 27,
    paddingRight: Platform.OS == 'ios' ? 50 : 40,
    paddingLeft: Platform.OS == 'ios' ? 50 : 40,
    shadowColor: "rgba(0,0,0,0.1)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 6,
    elevation: 2,
    
  },
  fundSubtitle: {
    color: 'rgba(0,0,0,0.5)',
    fontSize: 8,
    textAlign: 'center',
  }

})