import React, { Component } from 'react';
import {  
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  Platform,
  Keyboard,
  Alert,
  KeyboardAvoidingView
} from 'react-native'
import Text from '../../Components/globalTextStyle';

import CountryPicker, {
  getAllCountries
} from 'react-native-country-picker-modal'
import { parsePhoneNumberFromString } from 'libphonenumber-js'

import CarrierInfo from 'react-native-carrier-info';
import NormalButton from '../../Components/NormalButton'
import LoadingModal from '../../Components/Modals/LoadingModal'
import DataStorage from '../../Libs/DataStorage'
import ClientApi from '../../Services/ClientApi'
import Crypto from '../../Libs/Crypto'
import Toast from 'react-native-simple-toast';
import SecureDataStorage from '../../Libs/SecureDataStorage';
import PushNotification from '../../Libs/PushNotification';


// const COUNTRIES_AVAILABLE = ['TW', 'BZ', 'US', 'CA', 'MX']
const COUNTRIES_AVAILABLE = getAllCountries().map((country) => country.cca2)

export default class WelcomeScreen extends Component {

  constructor (props) {
    super(props)

    this.state = {
      cca2: 'BZ',
      // callingCode: '501',
      phoneNumber: '',
      showLoading: false
    }
    
  }

  _handleClientIDCheck = (createCallBack) => {
    DataStorage.getOblipClientID()
      .then(clientID => {
        // do nothing
        console.log('Device has client record already')
        console.log('Client ID: ' + clientID)
      }).catch(error => {
        console.log(error.message)
        console.log('We are going to start call back')
        // handle client registration
        createCallBack()
      }) 
  }

  createClient = (token) => {
    let fcm_token = token;
    let verifierKeys = Crypto.generateSigKeypair()
    let wrapperKeys = Crypto.generatePublicAuthKeypair()
    
    let keys = {
      verifierKeys: verifierKeys,
      wrapperKeys: wrapperKeys
    }

    SecureDataStorage.setOblipKeyPairs(keys)
      .then(r => {
        ClientApi.create(keys.verifierKeys.publicKey,
          keys.wrapperKeys.publicKey, fcm_token)
          .then(res => {
            console.log('We are finished with creating client')
          }).catch(error => {
            console.log('Could not create a new client :/')
            Toast.show('Something went wrong.', Toast.SHORT)
          })
      }).catch(error => {
        console.log(error.message)
        Toast.show('Something went wrong.', Toast.SHORT)
      })
  }

  componentDidMount () {

    this._handleClientIDCheck(() => {
      PushNotification.register((token) => {
        console.log(token)
        this.createClient(token)
      });
    }) 

    CarrierInfo.isoCountryCode()
      .then((result) => {
        localeCountry = result ? result.toUpperCase() : 'BZ' // default is BZD :v 
        const userCountryData = getAllCountries()
          .filter(country => country.cca2 === localeCountry)
          .pop()
        this.setState({
          cca2: userCountryData.cca2,
          phoneNumber: userCountryData.callingCode.toString()
          // callingCode: userCountryData.callingCode
        })
      }).catch(error => {
        
      })
  }

  onNextPressed = () => {
    if (this.state.phoneNumber.trim() == '') {
      Alert.alert('Code required', 'Please enter the code before proceeding.');
      return;
    }

    Keyboard.dismiss()
    // if (Platform.OS == 'ios') {
    //   this.setState({showLoading: true})
    // } else {
    //   setTimeout(() => {
    //     this.setState({showLoading: true})
    //   }, 100)
    // }

    this.setState({showLoading: true})

    // let phoneNumber = this.state.callingCode + this.state.phoneNumber
    let phoneNumber = this.state.phoneNumber

    ClientApi.sendVerificationCode(phoneNumber)
      .then(res => {
        this.props.navigation.navigate('CodeConfirmationScreen', { phoneNumber: phoneNumber })
      }).catch(error => {
        console.log('There was an error')
        this.setState({showLoading: false})
        Toast.show(error.message, Toast.SHORT);
      })

    // setTimeout(() => {
    //   this.props.navigation.navigate('CodeConfirmationScreen')
    // }, 2000)
  }

  onPhoneNumberChange = (text) => {
    formattedPhoneNumber = `+${text}`
    const phoneNumber = parsePhoneNumberFromString(formattedPhoneNumber)
    console.log(`Country: ${phoneNumber != undefined ? phoneNumber.country : 'Invalid phone number'}`)

    if(phoneNumber) {
      if (phoneNumber.country != undefined) {
        this.setState({cca2: phoneNumber.country})
      }
    }

    this.setState({phoneNumber: text})
  }
  
  render () {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'padding' : null} enabled={Platform.OS == 'ios'} keyboardVerticalOffset={0}  style={styles.container}>
        <StatusBar
          barStyle={'dark-content'}
          backgroundColor={'white'} />

        <LoadingModal showLoading={this.state.showLoading} />

        <View style={styles.content}>
          <Text style={styles.title}>Welcome to Oblip! {'\n'} Enter your mobile number to continue</Text>
          <View style={styles.customInputContainer}>
            <View style={styles.countryPickerContainer}>
              <CountryPicker
              countryList={COUNTRIES_AVAILABLE}
              closeable={true}
              showCallingCode={true}
              onChange={value => {
                this.setState({ cca2: value.cca2, phoneNumber:  value.callingCode.toString() })
              }}
              cca2={this.state.cca2}
              
              />
              {/* <Text style={styles.countryPhoneCode}>+{this.state.callingCode}</Text> */}
            </View>
            <TextInput
                style={styles.input}
                keyboardType={'numeric'}
                autoFocus={true}
                onSubmitEditing={() => {
                  this.onNextPressed();
                }}
                autoCorrect={false}
                autoCompleteType={'off'}
                importantForAutofill={'no'}
                placeholderTextColor={'#D8D8D8'}
                underlineColorAndroid='transparent'
                value={this.state.phoneNumber}
                onChangeText={(text) => this.onPhoneNumberChange(text)}
                placeholder='Phone Number'
              />
          </View>
        </View>
        <View style={styles.buttonContainer}>
            <NormalButton
              title={'Next'}
              onPress={() => this.onNextPressed()}
            />
          </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  countryPhoneCode: {
    marginLeft: 5, marginRight: 5, color: '#162842'
  },

  countryPickerContainer: {
    flexDirection: 'row', alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  input: {
    flex: 1, fontSize: 16, color: 'black',
    padding: 10
  },
  customInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 10,
    paddingLeft: 10, 
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
  },
  buttonContainer: {
    bottom: 0,
    width: '100%',
    height: 100,
    // zIndex: 100,
    paddingBottom: 10,
    flexDirection: 'row', 
    alignContent: 'center', 
    alignSelf: 'flex-end',
    alignItems:'flex-end',  justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    color: '#232228',
    marginBottom: 5
  },
  content: {
    flex: 1,
    paddingLeft: '15%',
    paddingRight: '15%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  }
})