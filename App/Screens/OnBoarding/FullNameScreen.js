import React, { Component } from 'react';
import {  
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  Platform,
  Alert,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native'
import Text from '../../Components/globalTextStyle';

import NormalButton from '../../Components/NormalButton'
import { BackHandler } from "react-native";
import LoadingModal from '../../Components/Modals/LoadingModal'
import Toast from 'react-native-simple-toast'

export default class FullNameScreen extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor (props) {
    super(props)

    this.state = {
      fullname: '',
      showLoading: false
    }

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    try {
      this.phoneNumber  = this.props.navigation.state.params.phoneNumber
    } catch (e) {
      Toast.show('Something went wrong. Try again!')
      this.props.navigation.navigate('PhoneNumberScreen'); 
    }
    
  }

  componentDidMount () {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    return true; // we handled the back button; no need to pop the screen
  };

  getName = () => {
    let fullName = this.state.fullname;
    // let cleanString = fullName.replace(/\s+/g,' ').trim();
    let isCleanString = /^([a-zA-Z0-9]+|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{1,}|[a-zA-Z0-9]+\s{1}[a-zA-Z0-9]{3,}\s{1}[a-zA-Z0-9]{1,})$/g.test(fullName)
    if(!isCleanString) { throw new Error ('Sorry, You entered an invalid name. Please try again.')}
    let names = fullName.split(' ');
    // console.log(names)
    if (names.length < 2) { throw new Error('Please enter your full name.') }

    return {
      firstName: names[0],
      middleName: names.length > 2 ? names[1] : '',
      lastName: names.length > 2 ? names[2] : names[1]
    }
  }

  onNextPressed = () => {
    Keyboard.dismiss()
    // if (Platform.OS == 'ios') {
    //   this.setState({showLoading: true})
    // } else {
    //   setTimeout(() => {
    //     this.setState({showLoading: true})
    //   }, 100)
    // }

    try {
      let names = this.getName();
      this.props.navigation.navigate('UsernameScreen', { phoneNumber: this.phoneNumber, names: names })
    } catch (e) {
      Alert.alert('Error', e.message)
    }
    

    // setTimeout(() => {
    //   this.props.navigation.navigate('UsernameScreen')
    // }, 2000)
  }
  
  render () {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'padding' : null} enabled={Platform.OS == 'ios'} keyboardVerticalOffset={0}  style={styles.container}>
        <StatusBar
          barStyle={'dark-content'}
          backgroundColor={'white'} />
        <LoadingModal showLoading={this.state.showLoading} />

        <View style={styles.content}>
          <Text style={styles.title}>Please enter your full {'\n'} name</Text>
          <View style={styles.customInputContainer}>
            <TextInput
                style={styles.input}
                autoFocus={true}
                onSubmitEditing={() => {
                  this.onNextPressed()
                }}
                autoCorrect={false}
                placeholderTextColor={'#D8D8D8'}
                underlineColorAndroid='transparent'
                onChangeText={(text) => this.setState({fullname: text})}
                placeholder='Full Name'
              />
          </View>
        </View>
        <View style={styles.buttonContainer}>
            <NormalButton
              title={'Next'}
              onPress={() => this.onNextPressed()}
            />
          </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  countryPhoneCode: {
    marginLeft: 5, marginRight: 5, color: '#162842'
  },

  countryPickerContainer: {
    flexDirection: 'row', alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  input: {
    flex: 1, fontSize: 16, color: 'black',
    padding: 10
  },

  customInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 10,
    paddingLeft: 10, 
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    // height: 50
  },
  buttonContainer: {
    bottom: 0,
    width: '100%',
    height: 100,
    // zIndex: 100,
    paddingBottom: 10,
    flexDirection: 'row', 
    alignContent: 'center', 
    alignSelf: 'flex-end',
    alignItems:'flex-end',  justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    color: '#232228',
    marginBottom: 20
  },
  content: {
    flex: 1,
    paddingLeft: '15%',
    paddingRight: '15%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  }
})