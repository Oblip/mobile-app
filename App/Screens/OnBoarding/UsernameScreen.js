import React, { Component } from 'react';
import {  
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  Platform,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native'
import Text from '../../Components/globalTextStyle';

import NormalButton from '../../Components/NormalButton'
import LoadingModal from '../../Components/Modals/LoadingModal'
import AccountApi from '../../Services/AccountApi'
import Toast from 'react-native-simple-toast'

export default class UsernameScreen extends Component {

  constructor (props) {
    super(props)

    this.state = {
      username: '',
      showLoading: false
    }

    try {
      this.phoneNumber  = this.props.navigation.state.params.phoneNumber
      this.names = this.props.navigation.state.params.names
    } catch (e) {
      Toast.show('Something went wrong. Try again!')
      this.props.navigation.navigate('PhoneNumberScreen'); 
    }
  }

  onNextPressed = () => {
    Keyboard.dismiss()
    this.setState({showLoading: true});
    // if (Platform.OS == 'ios') {
    //   this.setState({showLoading: true})
    // } else {
    //   setTimeout(() => {
    //     this.setState({showLoading: true})
    //   }, 100)
    // }

    let username = this.state.username;

    let data = {
      username: username,
      phone_number: this.phoneNumber,
      first_name: this.names['firstName'],
      middle_name: this.names['middleName'],
      last_name: this.names['lastName'],
    }
    console.log(data)
    AccountApi.create(data)
      .then(res => {
        this.props.navigation.navigate('WelcomeScreen')
      }).catch(e => {
        this.setState({ showLoading: false })
        Toast.show(e.message, Toast.LONG)
      })
  }
  
  render () {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'padding' : null} enabled={Platform.OS == 'ios'} keyboardVerticalOffset={0}  style={styles.container}>
        <StatusBar
          barStyle={'dark-content'}
          backgroundColor={'white'} />

        <LoadingModal showLoading={this.state.showLoading} />

        <View style={styles.content}>
          <Text style={styles.title}>Choose your unique name for {'\n'} getting paid by anyone</Text>
          <View style={styles.customInputContainer}>
            <TextInput
                style={styles.input}
                autoFocus={true}
                onSubmitEditing={() => {

                }}
                autoCorrect={false}
                autoCapitalize={'none'}
                placeholderTextColor={'#D8D8D8'}
                underlineColorAndroid='transparent'
                onChangeText={(text) => this.setState({username: text})}
                placeholder='Username'
              />
          </View>
          <Text style={styles.inputSecondaryText}>oblip.me/{this.state.username != '' ? this.state.username : '[Your_Username]'}</Text>
        </View>
        <View style={styles.buttonContainer}>
            <NormalButton
              title={'Next'}
              onPress={() => this.onNextPressed()}
            />
          </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  countryPhoneCode: {
    marginLeft: 5, marginRight: 5, color: '#162842'
  },

  countryPickerContainer: {
    flexDirection: 'row', alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  input: {
    flex: 1, fontSize: 16, color: 'black',
    padding: 10
  },
  inputSecondaryText: {
    textAlign: 'center',
    marginTop: 2,
    color: '#D8D8D8',
    fontSize: 16
  },
  customInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 10,
    paddingLeft: 10, 
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
  },
  buttonContainer: {
    bottom: 0,
    width: '100%',
    height: 100,
    // zIndex: 100,
    paddingBottom: 10,
    flexDirection: 'row', 
    alignContent: 'center', 
    alignSelf: 'flex-end',
    alignItems:'flex-end',  justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    color: '#232228',
    marginBottom: 20
  },
  content: {
    flex: 1,
    paddingLeft: '15%',
    paddingRight: '15%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  }
})