import React, { Component } from 'react';
import {  
  View,
  StyleSheet,
  Image,
  StatusBar,
  Keyboard,
  Dimensions,
  Platform,
  TouchableOpacity
} from 'react-native'
import Text from '../../Components/globalTextStyle';

import { BackHandler } from "react-native";
import Permissions from 'react-native-permissions';
import CheckingAccountApi from '../../Services/CheckingAccountApi';
import Animation from 'lottie-react-native';
import FayeServices from '../../Services/FayeService';
const viewport = Dimensions.get('window')

export default class WelcomeScreen extends Component {
  animation;
  _didFocusSubscription;
  _willBlurSubscription;

  constructor (props) {
    super(props)

    this.state = {
      fullname: ''
    }

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
    
  }

  _requestCameraPermission = () => {
    Permissions.request('camera').then(response => {
      if (response == 'authorized') {
       // TODO: handle some shit
      } else {
        // TODO: handle user didn't accept
        // setTimeout(() => {
        //   this.props.navigation.navigate('AppNav')
        // }, 3000)
      }

      // setTimeout(() => {
      //   this.props.navigation.navigate('AppNav')
      // }, 3000)

    });
  }

  _handlePermissions = () => {
    return new Promise((resolve, reject) => {
      Permissions.request('camera').then((cameraStatus) => {
        Permissions.request('location').then((locationStatus) => {
          resolve({cameraStatus, locationStatus})
        })
      })
    })
  }

  componentDidMount () {
    if(this.animation) {
      this.animation.play();
    }
    Keyboard.dismiss()
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    FayeServices.init();

    CheckingAccountApi.create()
    .then(res => {
      console.log('We created the blockchain')
      this._handlePermissions().then(statuses => {
        this.props.navigation.navigate('AppNav')
      }).catch(error => {
        console.log('Error getting permissions')
        console.log(error)
      })

      // if(this.animation) {
      //     this.animation.play()
      //   }
    }).catch(error => {
      console.log(error)
    })
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    return true; // we handled the back button; no need to pop the screen
  };

  render () {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle={'dark-content'}
          backgroundColor={'white'} />

          <View style={styles.content}>
            <Image
              source={require('../../Assets/Images/success_large_icon.png')}
              resizeMode={'cover'}
              style={styles.successIcon}
            />
            <Text style={styles.title}>Welcome to Oblip!</Text>
          </View>
          
          <View style={styles.loadingContainer}>
            <Animation
              ref={animation => {
                this.animation = animation;
              }}
              loop={true}
              style={styles.loading}
              source={require('../../Assets/Animations/loading_1.json')}
            />
          </View>
          <Text style={styles.loadingText}>We are creating your checking account</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  },

  successIcon: {
    width: 62,
    height: 62
  },

  title: {
    textAlign: 'center',
    fontSize: 26,
    color: '#232228',
    marginTop: 10
  },
  content: {
    height: 280,
    paddingLeft: '15%',
    paddingRight: '15%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  },
    loadingContainer: {  
    marginBottom: 80,
    alignContent: 'center',
    justifyContent: 'center',
    width: Platform.OS == 'ios' ? '100%' :  viewport.width / 3,
    height: Platform.OS == 'ios' ? viewport.width / 3 :   viewport.width / 3,
  },
  loading: { 
    width: '100%',
    height: '100%',
    alignSelf: 'center'
  },
  loadingText: {
    textAlign: 'center',
    position: 'absolute',
    bottom: '15%',
    width: '100%',
    fontSize: 13
  }
})