import React, { Component } from 'react';
import {  
  View,
  StyleSheet,
  TextInput,
  StatusBar,
  Platform,
  Keyboard,
  Alert,
  KeyboardAvoidingView
} from 'react-native'
import Text from '../../Components/globalTextStyle';

import NormalButton from '../../Components/NormalButton';
import { BackHandler } from "react-native";
import LoadingModal from '../../Components/Modals/LoadingModal'
import Toast from 'react-native-simple-toast'
import ClientApi from '../../Services/ClientApi'

export default class CodeConfirmationScreen extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor (props) {
    super(props)

    this.state = {
      code: '',
      showLoading: false
    }

    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );

    try {
      this.phoneNumber  = this.props.navigation.state.params.phoneNumber
    } catch (e) {
      Toast.show('Something went wrong. Try again!')
      this.props.navigation.navigate('PhoneNumberScreen'); 
    }
    
  }

  componentDidMount () {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    return true; // we handled the back button; no need to pop the screen
  };

  onNextPressed = () => {
    Keyboard.dismiss();
    let code = this.state.code;

    if (this.state.code.trim() == '') {
      Alert.alert('Code required', 'Please enter the code before proceeding.');
      return;
    }

    // hack to make the modal work
    // if (Platform.OS == 'ios') {
    //   this.setState({showLoading: true})
    // } else {
    //   setTimeout(() => {
    //     this.setState({showLoading: true})
    //   }, 100)
    // }
    this.setState({showLoading: true})

    ClientApi.verifyCode(code)
      .then(res => {
        this.props.navigation.navigate('FullNameScreen', { phoneNumber: this.phoneNumber })
      }).catch(error => {
        console.log(error.message)
        Toast.show(error.message, Toast.SHORT);
        this.setState({showLoading: false});
      })
    // setTimeout(() => {
    //   this.props.navigation.navigate('FullNameScreen')
    // }, 2000)
  }

  onHelpPressed = () => {
    // TODO
    Alert.alert('Not available', 'Sorry this feature is not available right now.')
  }
  
  render () {
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? 'padding' : null} enabled={Platform.OS == 'ios'} keyboardVerticalOffset={0}  style={styles.container}>
        <StatusBar
          barStyle={'dark-content'}
          backgroundColor={'white'} />

        <LoadingModal showLoading={this.state.showLoading} />

        <View style={styles.content}>
          <Text style={styles.title}>Please enter the code sent to {'\n'} {this.phoneNumber}</Text>
          <View style={styles.customInputContainer}>
            <TextInput
                style={styles.input}
                keyboardType={'numeric'}
                autoFocus={true}
                onSubmitEditing={() => {

                }}
                autoCorrect={false}
                placeholderTextColor={'#D8D8D8'}
                underlineColorAndroid='transparent'
                onChangeText={(text) => this.setState({code: text})}
                placeholder='Confirmation Code'
              />
          </View>
        </View>
        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <NormalButton
              title={'Help'}
              onPress={() => this.onHelpPressed()}
            />
          </View>
          <View style={styles.buttonContainer}>
            <NormalButton
              title={'Next'}
              onPress={() => this.onNextPressed()}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  countryPhoneCode: {
    marginLeft: 5, marginRight: 5, color: '#162842'
  },

  countryPickerContainer: {
    flexDirection: 'row', alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  input: {
    flex: 1, fontSize: 16, color: 'black',
    padding: 10
  },
  customInputContainer: {
    borderRadius: 8,
    backgroundColor: 'white',
    borderRadius: 7,
    borderColor: '#EEEEEE',
    borderWidth: 1,
    marginTop: 10,
    paddingLeft: 10, 
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
  },
  buttonsContainer: {
    bottom: 0,
    width: '100%',
    height: 100,
    // zIndex: 100,
    paddingBottom: 10,
    // borderColor: 'red', borderWidth: 1,
    flexDirection: 'row', 
    alignContent: 'center', 
    alignSelf: 'flex-end',
    alignItems:'flex-end',  justifyContent: 'center',
  },

  buttonContainer: {
    flex: 1, 
    alignContent: 'center', alignItems:'center',  justifyContent: 'center'
  },
  title: {
    textAlign: 'center',
    fontSize: 18,
    color: '#232228',
    marginBottom: 20
  },
  content: {
    flex: 1,
    paddingLeft: '15%',
    paddingRight: '15%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems:'center', 
  }
})