import React, { Component } from 'react';
import {  
  View,
  StyleSheet,
  ActivityIndicator,
  StatusBar
} from 'react-native'
//import Text from '../../Components/globalTextStyle';

import SplashScreen from 'react-native-smart-splash-screen'
import SecureDataStorage from '../Libs/SecureDataStorage'
import codePush from 'react-native-code-push'
import FayeService from '../Services/FayeService'

export default class AuthScreen extends Component {
  
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    codePush.sync();
    SplashScreen.close({
      animationType: SplashScreen.animationType.scale,
      duration: 600,
      delay: 200,
    })

    this.handleAuth()
  }

  handleAuth = () => {

    SecureDataStorage.getAuthAccount().then(authAccount => {
      // this.props.navigation.navigate('WelcomeScreen')

      // Connect to Faye
      FayeService.init();

      this.props.navigation.navigate('AppNav')
      // this.props.navigation.navigate('WelcomeScreen')
    //   let extraData = {
    //     name: 'Dony Escobar',
    //     isSuccess: true,
    //     amount: "100",
    //     asset: "BZD",
    //     transactionId: "0046de18-b7d9-4f9a-937c-dff96af7063a",
    //     date: "03 December 2019, 10:27 PM"
    //   }

    //   this.props.navigation.navigate('TransactionResultScreen', {extraData: extraData})
    }).catch(error => {
      this.props.navigation.navigate('OnBoardingNav');
      // this.props.navigation.navigate('AppNav')
      // this.props.navigation.navigate('FullNameScreen', { phoneNumber: '6644584' });
    })
  }

  render () {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={'white'}
          barStyle={'dark-content'}
        />
        <ActivityIndicator size="large" color="#0084FF" />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  }
});


const codePushOptions = { 
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME, 
  installMode: codePush.InstallMode.ON_NEXT_RESUME 
};

module.exports = codePush(codePushOptions)(AuthScreen)

