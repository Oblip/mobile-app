import {
	SwitchNavigator
} from 'react-navigation'
import React from 'react';

import OnBoardingNav from './OnBoardingNav'
import AppNav from './AppNav'
import AuthScreen from '../Screens/AuthScreen'

const RootNav = SwitchNavigator({
  AuthScreen: AuthScreen,
  OnBoardingNav: OnBoardingNav,
  AppNav: {
    screen: AppNav,
    path: '/'
  }
}, {
	initialRouteName: 'AuthScreen',
});

const prefix = 'oblip://';

const MainApp = () => <RootNav uriPrefix={prefix} />;

export default MainApp ;