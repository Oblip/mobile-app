import {
	StackNavigator
} from 'react-navigation'

import MainScreen from '../Screens/App/MainScreen'
import LogsScreen from '../Screens/App/LogsScreen'
import ProfileScreen from '../Screens/App/ProfileScreen'
import TransactionScreen from '../Screens/App/TransactionScreen'
import TransactionResultScreen from '../Screens/App/TransactionResultScreen'
import AccountScreen from '../Screens/App/AccountScreen'
import BankScreen from '../Screens/App/BankScreen'
// import BankRequestDetailsScreen from '../Screens/App/BankRequestDetailsScreen'
import TopupWithdrawScreen from '../Screens/App/TopupWithdrawScreen'
import PersonalDetailsScreen from '../Screens/App/PersonalDetailsScreen'
import AgentScreen from '../Screens/App/AgentScreen' 
// KYC Screens
import CaptureDocumentScreen from '../Screens/App/KYC/CaptureDocumentScreen'
import CaptureSelfieScreen from '../Screens/App/KYC/CaptureSelfieScreen'
import DocumentSubmittedScreen from '../Screens/App/KYC/DocumentSubmittedScreen'

// Blockchain Screens
// import BlockchainIntro from '../Screens/App/Blockchain/BlockchainIntro'

const AppNav = StackNavigator({
  MainScreen: MainScreen,
  LogsScreen: LogsScreen,
  ProfileScreen: ProfileScreen,
  TransactionScreen: {
    screen: TransactionScreen,
    path: 'transaction/:username'
  },
  TransactionResultScreen: TransactionResultScreen,
  AccountScreen: AccountScreen,
  BankScreen: BankScreen,
  // BankRequestDetailsScreen: BankRequestDetailsScreen,
  TopupWithdrawScreen: TopupWithdrawScreen,
  PersonalDetailsScreen: PersonalDetailsScreen,
  CaptureDocumentScreen: CaptureDocumentScreen,
  CaptureSelfieScreen: CaptureSelfieScreen,
  DocumentSubmittedScreen: DocumentSubmittedScreen,
  AgentScreen: AgentScreen
}, {
  initialRouteName: 'MainScreen'
})

export default AppNav;