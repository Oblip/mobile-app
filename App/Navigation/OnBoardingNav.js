import {
	StackNavigator
} from 'react-navigation'

import {
	Easing,
	Animated
} from 'react-native'

import PhoneNumberScreen from '../Screens/OnBoarding/PhoneNumberScreen'
import CodeConfirmationScreen from '../Screens/OnBoarding/CodeConfirmationScreen'
import FullNameScreen from '../Screens/OnBoarding/FullNameScreen'
import UsernameScreen from '../Screens/OnBoarding/UsernameScreen'
import WelcomeScreen from '../Screens/OnBoarding/WelcomeScreen'

// applies right animated transition to all screens
const transitionConfig = () => {
  return {
    transitionSpec: {
      duration: 750,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: sceneProps => {      
      const { layout, position, scene } = sceneProps

      const thisSceneIndex = scene.index
      const width = layout.initWidth

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      })

      return { transform: [ { translateX } ] }
    },
  }
}

const OnBoardingNav = StackNavigator({
	PhoneNumberScreen: PhoneNumberScreen,
	CodeConfirmationScreen: CodeConfirmationScreen,
	FullNameScreen: FullNameScreen,
	UsernameScreen: UsernameScreen,
	WelcomeScreen: WelcomeScreen
}, {
	initialRouteName: 'PhoneNumberScreen',
	transitionConfig,
	navigationOptions: {
		header: null
	}
});

export default OnBoardingNav;