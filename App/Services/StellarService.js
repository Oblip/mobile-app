import Config from 'react-native-config'
const StellarSdk = require('stellar-sdk')

export default class StellarService {
  constructor() {
    this.server = new StellarSdk.Server(Config.STELLAR_API_URL);

    if (Config.ENVIRONMENT == 'Production') {
      console.log('USING Blockchain Public Network')
      StellarSdk.Network.usePublicNetwork();
    } else {
      console.log('USING Blockchian Test Network')
      StellarSdk.Network.useTestNetwork();
    }
  }

  signTransactionEnvelope(unsignedTxEnvelope, secretKey) {
    const transaction = new StellarSdk.Transaction(unsignedTxEnvelope)
    const keypair = StellarSdk.Keypair.fromSecret(secretKey)
    transaction.sign(keypair)

    return transaction.toEnvelope().toXDR('base64')
  }

}