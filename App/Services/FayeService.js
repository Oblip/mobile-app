import Config from 'react-native-config'
import Faye from 'faye'

export default class FayeService {
  static init = () => {
    this.client = new Faye.Client(`${Config.APP_URL}/faye`);
  }

  static getClient = () => {
    return this.client;
  }
}