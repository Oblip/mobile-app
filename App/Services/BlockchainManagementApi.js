import Request from './Request'

export default class BlockchainManagementApi {  
  
  static fetchAvailableAssets() {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'available_assets',
        method: 'GET',
        requiresAuth: true
      }).then(res => {
        resolve(res)
      }).catch(error => {
        console.log(error);
        reject(error)
      })
    })
  }
  
  static fetchUserFunds() {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'account/checking_account',
        method: 'GET',
        requiresAuth: true
      }).then(res => {
        resolve(res)
      }).catch(error => {
        console.log(error);
        reject(error)
      })
    })
  }
  
}