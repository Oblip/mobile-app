import Request from './Request' 

export default class BusinessApi {  

  static filter(isAgent, isNearBy, lat, lng) {
    return new Promise((resolve, reject) => {
    let path = `businesses?is_agent=${isAgent}&nearby=${isNearBy}&lat=${lat}&lng=${lng}`
  
      Request.create({
        resource: path,
        method: 'GET',
        requiresAuth: true
      }).then(res => { 
        resolve(res.businesses)
      }).catch(error => { 
        reject(error)
      })
    })
  }

  // returns only the logged in users businesses
  static fetch() {
    return new Promise((resolve, reject) => {
  
      Request.create({
        resource: 'businesses',
        method: 'GET',
        requiresAuth: true
      }).then(res => { 
        resolve(res.businesses)
      }).catch(error => { 
        reject(error)
      })
    })
  }
}