import Request from './Request'

export default class BankManagementApi {  

  static createBankAccount(data) {
    return new Promise((resolve, reject) => {   
          
      let payload = {
        registered_bank_id: data.id,
        account_number: data.accountNumber,
        account_name: data.accountName,
        is_partner: data.isPartner
      }
      
      // makes the request to the server
      Request.create({
        resource: 'bank_accounts',
        method: 'POST',
        data: payload,
        requiresAuth: true
      }).then(res => {                
          console.log(res)
          resolve(res)
      }).catch(error => {
        //some http error
        reject(error)
      })
    })
  }

  static fetchRegisteredBanks() {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'registered_banks',
        method: 'GET',
        requiresAuth: true
      }).then(res => {
        resolve(res)
      }).catch(error => {
        console.log(error);
        reject(error)
      })
    })
  }

  static fetchBankAccountRequest(bankAccountID, requestID){
    return new Promise((resolve, reject) => {
      let res = 'bank_accounts/' + bankAccountID + '/requests/' + requestID

      Request.create({
        resource: res,
        method: 'GET',
        requiresAuth: true
      }).then(res => {
        resolve(res)
      }).catch(error => {
        console.log(error);
        reject(error)
      })
    })
  }


  static fetchBankAccounts() {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'bank_accounts',
        method: 'GET',
        requiresAuth: true
      }).then(res => {
        resolve(res)
      }).catch(error => {
        console.log(error);
        reject(error)
      })
    })
  }

  static fetchBankingRequests(bankAccountID, offset = 0){
    return new Promise((resolve, reject) => {
      let res = `bank_accounts/${bankAccountID}/requests?offset=${offset}`
      Request.create({
        resource: res,
        method: 'GET',
        requiresAuth: true
      }).then(res => {
        resolve(res.bank_account_requests)
      }).catch(error => {
        console.log(error);
        reject(error)
      })
    })
  }

  static confirmTopupRequest(bankAccountID, requestID, newStatus) {
    return new Promise((resolve, reject) => {   
          
      let payload = {
        status: newStatus
      }
      let res = 'bank_accounts/' + bankAccountID + '/requests/' + requestID + '/status'
      console.log(res)
      // makes the request to the server
      Request.create({
        resource: res,
        method: 'PUT',
        data: payload,
        requiresAuth: true
      }).then(res => {                
          console.log(res)
          resolve(res)
      }).catch(error => {
        //some http error
        reject(error)
      })
    })
  }

  static createTopupRequest(data) {
    return new Promise((resolve, reject) => {   
          
      let payload = {
        amount: data.amount,
        registered_bank_id: data.registered_bank_id,
        asset: data.asset
      }
      let res = 'bank_accounts/' + data.bank_account_id + '/deposits'
      console.log(res)
      // makes the request to the server
      Request.create({
        resource: res,
        method: 'POST',
        data: payload,
        requiresAuth: true
      }).then(res => {                
          console.log(res)
          resolve(res)
      }).catch(error => {
        //some http error
        reject(error)
      })
    })
  }
}