import Request from './Request' 
import StellarService from './StellarService'
import SecureDataStorage from '../Libs/SecureDataStorage'

export default class TransactionApi {  

  /**
   * 
   * @param {
   *  to [string]
   *  amount [float]
   *  asset [string]
   *  type [string]
   * } data 
   */
  static getEnvelope(data) {
    // TODO: do check of required data;
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'transaction_envelope',
        method: 'POST',
        requiresAuth: true,
        data: data
      }).then(res => {
        resolve(res);
      }).catch(error => {
        reject(error);
      })
    })
    
  }


  /**
   * 
   * @param {
    *  envelope [string]
    *  type [string]
    *  message [string]
    *  identifier [string]
    * } data 
    */
  static create(data)  {

    // TODO: do check of required data;
    return new Promise((resolve, reject) => {

      SecureDataStorage.getSignerSeed()
        .then(seed => {
          const Stellar = new StellarService();
          let signedEnvelope = Stellar.signTransactionEnvelope(data.envelope, seed);
          data.envelope = signedEnvelope
          // const data = {
          //   envelope: signedEnvelope,
          //   type: type,
          //   message: message,
          //   identifier: identifier
          // }

          Request.create({
            resource: 'transactions',
            method: 'POST',
            requiresAuth: true,
            data: data
          }).then(res => {
            resolve(res);
          }).catch(error => {
            reject(error);
          })

        }).catch(error => {
          // TODO: handle
        })
    })
  }
}