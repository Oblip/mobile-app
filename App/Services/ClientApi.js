import Request from './Request'
import DataStorage from '../Libs/DataStorage'
import SecureDataStorage from '../Libs/SecureDataStorage'
import Crypto from '../Libs/Crypto'


export default class ClientApi {
  static create (verifier_key, wrapper_key, fcm_token) {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'clients',
        method: 'POST',
        data: { verifier_key: verifier_key, wrapper_key: wrapper_key, fcm_token: fcm_token },
      }).then(res => {
        console.log('we got our client res here: ')
        console.log(res)
        // we gotta save our client ID here
        DataStorage.setOblipClientID(res.id)
          .then(r => {
            console.log('We saved the device ID in storage')
            resolve(res)
          }).catch(error =>{
            // TODO: I don't think this works?
            reject('Something went wrong saving device ID.')
          })
      }).catch(error => {
        // no internet?
        console.log('WTF happened')
        console.log(error)
        reject (error)
      })
    })
  }

  static updateFCM(fcmToken) {
    let data = {fcm_token: fcmToken };

    return new Promise((resolve, reject) => {
      SecureDataStorage.getOblipKeyPairs()
          .then(keys => {
            signature = Crypto.signData(data, keys.verifierKeys.secretKey)
            DataStorage.getOblipClientID()
              .then(clientID => {
                payload = {
                  data: data,
                  signature: signature,
                }

                Request.create({
                  resource: `clients/${clientID}`,
                  method: 'PUT',
                  data: payload,
                  requiresAuth: true
                }).then(res => {
                  resolve(res)
                }).catch(error => {
                  //some http error
                  reject(error)
                })

              }).catch(error => {
                reject(error)
              });
          }).catch(error => {
            reject(error)
          });
    });
  }

  static sendVerificationCode(phoneNumber) {
    let data = { phone_number: phoneNumber }

    return new Promise((resolve, reject) => {
        SecureDataStorage.getOblipKeyPairs()
          .then(keys => {
            signature = Crypto.signData(data, keys.verifierKeys.secretKey)
            DataStorage.getOblipClientID()
              .then(clientID => {
                payload = {
                  data: data,
                  signature: signature,
                }
            
                Request.create({
                  resource: `clients/${clientID}/send_verification`,
                  method: 'POST',
                  data: payload
                }).then(res => {
                  resolve(res)
                }).catch(error => {
                  //some http error
                  reject(error)
                })

            }).catch(error => {
              console.log(error)
              reject(error)
            })  
        }).catch(error => {
          // we don't have a keypair? (Impossible)
          reject(error)
        })
    })
  }

  static verifyCode(code) {
    let data = { code: code }
    
    return new Promise((resolve, reject) => {
      SecureDataStorage.getOblipKeyPairs()
        .then(keys => {
          signature = Crypto.signData(data, keys.verifierKeys.secretKey)
          DataStorage.getOblipClientID()
            .then(clientID => {
              payload = {
                data: data,
                signature: signature,
              }
          
              Request.create({
                resource: `clients/${clientID}/verify`,
                method: 'POST',
                data: payload
              }).then(res => {
                resolve(res)
              }).catch(error => {
                //some http error
                reject(error)
              })

          }).catch(error => {
            console.log(error)
            reject(error)
          })
      }).catch(error => {
        // we don't have a keypair? (Impossible)
        reject(error)
      })
    });
  }
}