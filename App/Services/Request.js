import Config from 'react-native-config'
import Crypto from '../Libs/Crypto'
import SecureDataStorage from '../Libs/SecureDataStorage'
import moment from 'moment'
import _ from 'underscore'


export default class Request {

  static create(opts) {
    request = new Request();
    return request.start(opts)
  }

  start = (opts) => {
    return new Promise((resolve, reject) => {
      if(opts['requiresAuth']) {
        SecureDataStorage.getAuthAccount()
          .then(authAccount => {
            accessData = authAccount.tokens.access
            console.log('Token Expired?: '+ this.isAccessTokenExpired(accessData.expires_on))
            if (!this.isAccessTokenExpired(accessData.expires_on)) {
              console.log('Not expired')
              opts['authToken'] = accessData.token
              Request.makeRequest(opts)
                .then(res => { resolve(res) })
                .catch(error => { reject(error) })
            } else {
              console.log('We are expired :P')

              // let opts = null;
              let account = authAccount.account

              if(account.type == 'business') {
                opts['type'] = 'business';
                opts['businessId'] = account.id
              }

              Request.fetchNewToken(authAccount, opts)
                .then(tokenData => {
                  opts['authToken'] = tokenData.token
                  Request.makeRequest(opts)
                    .then(res => { resolve(res) })
                    .catch(error => { reject(error) })
                }).catch(error => {
                  reject(error)
                })
            }
          }).catch(error => {
            // TODO: handle this
            reject(error)
          })
      } else {
        Request.makeRequest(opts)
          .then(res => { resolve(res) })
          .catch(error => { reject(error) })
      }
    })

  }

  isAccessTokenExpired (expiresOn) {
    currentTime = moment().unix()   
    return currentTime > expiresOn
  }

  static fetchNewToken = (authAccount, opts) => {
    return new Promise((resolve, reject) => {
      let prePayload = {}

      if(opts && opts['type'] && opts['type'] == 'business'){
        if(opts['businessId']) {
          prePayload = { 
            refresh_token: authAccount.tokens.refresh.token,
            type: opts['type'],
            business_id: opts['businessId']
          }
        } else {
          reject('Business ID is required for type business')
        }
      } else {
        prePayload = { 
          refresh_token: authAccount.tokens.refresh.token
        }
      }

      SecureDataStorage.getOblipKeyPairs()
        .then(keys => {
          // sign data sending
          signature = Crypto.signData(prePayload, keys.verifierKeys.secretKey)
          payload = {
            signature: signature,
            data: prePayload
          }

          Request.makeRequest({
            resource: 'token',
            method: 'POST',
            data: payload,
          }).then(tokenData => {

            // we should save the new data :p 
            authAccount.tokens.access = tokenData

            // get account data to save
            Request.makeRequest({
              resource: 'account',
              method: 'GET',
              authToken: tokenData.token
            }).then(account => {
              authAccount.account = account

              SecureDataStorage.setAuthAccount(authAccount)
                .then(saved => {
                  resolve(tokenData)
                })
            }).catch(error => {
              SecureDataStorage.setAuthAccount(authAccount)
                .then(saved => {
                  resolve(tokenData)
                })
            })

            
          }).catch(error => {
            reject(error)
          })

        }).catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }

  /*
    Creates a request
    returns: success? promise
    options: {
      resource: [string] eg. account,
      method: ['GET', POST' etc..],
      # authToken: [string],
      requiresAuth: [bool]
      abortSignal: [bool]
      data: [jsonObject]
    }
  */
  static makeRequest = (opts) => {
    return new Promise((resolve, reject) => {
      return fetch(`${Config.API_URL}/${opts['resource']}`, {
        method: `${opts['method']}`,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': opts['authToken'] !== undefined ? `Bearer ${opts['authToken']}` : ''
        },
        body: JSON.stringify(opts['data'])
      }).then(response => {
        if(response.status < 300) {
          response.json().then(data => resolve(data))
        } else {
          response.json().then(data => reject(data))
        }
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    })
  }
  
}
