import Request from './Request'
import DataStorage from '../Libs/DataStorage'
import SecureDataStorage from '../Libs/SecureDataStorage'
import Crypto from '../Libs/Crypto'

export default class AccountApi {

  static fetchBasic(username) {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: `account_availability/${username}`,
        method: 'GET',
        requiresAuth: false
      }).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error);
      })
    })
    
  }

  static fetchFederatedAccounts(checkingAccounts) {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'account_federation',
        method: 'POST',
        data: { checking_accounts: checkingAccounts },
        requiresAuth: true
      }).then(res => {
        resolve(res.accounts)
      }).catch(error => {
        reject(error);
      })
    })
  }

  /*
    data:
      first_name (String)
      middle_name (String)[*Optional]
      last_name (String)
      username (String)
      phone_number (String)
  */
  static create(data) {
    return new Promise((resolve, reject) => {
      SecureDataStorage.getOblipKeyPairs()
        .then(keys => {
          signature = Crypto.signData(data, keys.verifierKeys.secretKey)
          DataStorage.getOblipClientID()
            .then(clientID => {
              payload = {
                client_id: clientID,
                data: data,
                signature: signature,
              }
              
              // makes the request to the server
              Request.create({
                resource: 'accounts',
                method: 'POST',
                data: payload
              }).then(res => {
                
                // save auth account in local persistent storage
                SecureDataStorage.setAuthAccount(res)
                  .then(saved => {
                    resolve(res)
                  }).catch(error => {
                    reject(error)
                  })

              }).catch(error => {
                //some http error
                reject(error)
              })

          }).catch(error => {
            // no client id?
            console.log(error)
            reject(error)
          })  
      }).catch(error => {
        // we don't have a keys? (Impossible)
        reject(error)
      })
    })
  }

  static fetchSingleAccount(id) {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: `accounts/${id}`,
        method: 'GET',
        requiresAuth: true
      }).then(res => {
        resolve(res)
      }).catch(error => {
        reject(error);
      })
    });
  }
}
