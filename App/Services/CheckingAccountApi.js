import Request from './Request'
import SecureDataStorage from '../Libs/SecureDataStorage'
import Crypto from '../Libs/Crypto'

export default class CheckingAccountApi {
  static create() {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'account/checking_account',
        method: 'POST',
        requiresAuth: true,
        data: null
      }).then(res => {

        SecureDataStorage.getAuthAccount()
          .then(authAccount => {
            authAccount.account = res.account
            // save new account info with checking account id
            SecureDataStorage.setAuthAccount(authAccount)
              .then(xxx => {

                SecureDataStorage.getOblipKeyPairs()
                  .then(keys => {
                    let secretKey = keys.wrapperKeys.secretKey
                    let data = res.signer_seed.split('.')
                    console.log("Secret Key: "+ secretKey)
                    let seed = data[0]
                    let nonce = data[1]

                   Crypto.decryptAsymmetric(seed, nonce, res.wrapper_public_key, secretKey)
                    .then(msg => {
                      SecureDataStorage.setSignerSeed(msg)
                        .then(xx => {
                          SecureDataStorage.getCheckingAccounts()
                            .then(cks => {
                              // Exists so we just add to it.
                              cks[res.account.checking_account_id] = msg

                               // Save to CheckingAccounts
                               SecureDataStorage.setCheckingAccounts(cks)
                               .then(xxx => {
                                 resolve(res)
                               }).catch(error => {
                                 reject(error) // TODO: handle
                               })

                            }).catch(ckError => {
                              // Does not exist so we create it
                              let checkingAccounts = {}
                              checkingAccounts[res.account.checking_account_id] = msg

                              // Save to CheckingAccounts
                              SecureDataStorage.setCheckingAccounts(checkingAccounts)
                                .then(xxx => {
                                  resolve(res)
                                }).catch(error => {
                                  reject(error) // TODO: handle
                                })
                            });
                        })
                    }).catch(error => {
                      console.log(error.message)
                      reject(error) // TODO: handle
                    })
                  })
              }).catch(error => {
                console.log(error)
                reject(error) // TODO: handle
              })

          }).catch(error => {
            console.log(error)
            reject(error)
          })

      }).catch(error => {
        reject(error)
      })
    })
  }

  static fetch() {
    return new Promise((resolve, reject) => {
      Request.create({
        resource: 'account/checking_account',
        method: 'GET',
        requiresAuth: true
      }).then(res => {
        resolve(res)
      }).catch(error => {
        console.log(error);
        reject(error)
      })
    })
  }
}