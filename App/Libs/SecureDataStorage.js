import RNSecureKeyStore from 'react-native-secure-key-store';
import Config from 'react-native-config';

export default class SecureDataStorage {
  // static setRefreshToken (tokenData) {
  //   return RNSecureKeyStore.set(`${Config.STORAGE_KEYWORD}_jeU95VHtRvzlwjBKZVnGqed`, JSON.stringify(tokenData))
  // }

  // static getRefreshToken () {
  //   return new Promise((resolve,reject) => {
  //     RNSecureKeyStore.get(`${Config.STORAGE_KEYWORD}_jeU95VHtRvzlwjBKZVnGqed`)
  //       .then((savedContent) => {
  //         resolve(JSON.parse(savedContent))
  //       }).catch((error) => {
  //         reject(error)
  //       })
  //   })
  // }

  // static getRefreshToken () {
  //   return RNSecureKeyStore.get(`${Config.STORAGE_KEYWORD}_jeU95VHtRvzlwjBKZVnGqed`)
  // }

  static setOblipKeyPairs (keypair) {
    return RNSecureKeyStore.set(`${Config.STORAGE_KEYWORD}_OblipKeyPair`, JSON.stringify(keypair) )
  }

  static getOblipKeyPairs () {
    return new Promise((resolve,reject) => {
      RNSecureKeyStore.get(`${Config.STORAGE_KEYWORD}_OblipKeyPair`)
        .then((savedContent) => {
          resolve(JSON.parse(savedContent))
        }).catch((error) => {
          reject(error)
        })
    })
  }

  static setCheckingAccounts (checkingAccounts) {
    return RNSecureKeyStore.set(`${Config.STORAGE_KEYWORD}_CheckingAccounts`, JSON.stringify(checkingAccounts))
  }

  static removeCheckingAccounts () {
    return RNSecureKeyStore.remove(`${Config.STORAGE_KEYWORD}_CheckingAccounts`)
  }

  static getCheckingAccounts () {
    return new Promise((resolve,reject) => {
      RNSecureKeyStore.get(`${Config.STORAGE_KEYWORD}_CheckingAccounts`)
        .then((savedContent) => {
          resolve(JSON.parse(savedContent))
        }).catch((error) => {
          reject(error)
        })
    })
  }

  static setAuthAccount (authAccount) {
    return RNSecureKeyStore.set(`${Config.STORAGE_KEYWORD}_AuthAccount`, JSON.stringify(authAccount) )
  }

  static getAuthAccount () {
    return new Promise((resolve,reject) => {
      RNSecureKeyStore.get(`${Config.STORAGE_KEYWORD}_AuthAccount`)
        .then((savedContent) => {
          resolve(JSON.parse(savedContent))
        }).catch((error) => {
          reject(error)
        })
    })
  }
  
  

  static setBankAccount (bankAccount) {
    return RNSecureKeyStore.set(`${Config.STORAGE_KEYWORD}_BankAccount`, JSON.stringify(bankAccount) )
  }

  static getBankAccount () {
    return new Promise((resolve,reject) => {
      RNSecureKeyStore.get(`${Config.STORAGE_KEYWORD}_BankAccount`)
        .then((savedContent) => {
          resolve(JSON.parse(savedContent))
        }).catch((error) => {
          reject(error)
        })
    })
  }

  static setSignerSeed (seed) {
    return RNSecureKeyStore.set(`${Config.STORAGE_KEYWORD}_kqeU95VHtRvzKZVnGqed`, seed)
  }

  static removeSignerSeed (seed) {
    return RNSecureKeyStore.remove(`${Config.STORAGE_KEYWORD}_kqeU95VHtRvzKZVnGqed`)
  }

  static getSignerSeed () {
    return RNSecureKeyStore.get(`${Config.STORAGE_KEYWORD}_kqeU95VHtRvzKZVnGqed`)
  }

  

}