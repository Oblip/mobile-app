import firebase from 'react-native-firebase';
import type { Notification } from 'react-native-firebase';
import { Platform } from 'react-native'

export default class PushNotification {
  static askPermission() {
    return new Promise ((resolve, reject) => {
      firebase.messaging().hasPermission()
        .then(enabled => {
          if (enabled) {
            // user has permissions
            // do nothing.
            resolve(true)
          } else {
            // user doesn't have permission
            firebase.messaging().requestPermission() // asks user for permission
              .then(() => {
                // User has authorised  
                resolve(true)
              })
              .catch(error => {
                // User has rejected permissions  
                reject(false)
              });
          } 
        });
    })
  }

  static onNotificationOpened (callback) {
    firebase.notifications().onNotificationOpened((notificationOpen) => {
      firebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId)
      callback(notificationOpen.notification, false)
    });
    
    firebase.notifications().getInitialNotification()
      .then(notificationOpen => {
        firebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId)
        callback(notificationOpen.notification, true)
      })

  }

  static handleNotification(notification: Notification) {
    console.log('GOt notification')
    console.log(notification)

    // return;
    
    const options= new firebase.notifications.Notification()
      .setNotificationId(notification._notificationId)
      .setTitle(notification._title) 
      .setSound("default")
      .setBody(notification._body)
      .setData(JSON.parse(notification._data.extras))

    // if android
    if(Platform.OS == 'android') {
      options
      .android.setChannelId("oblip")
      .android.setSmallIcon('notification_icon_small')
      .android.setPriority(firebase.notifications.Android.Priority.High)
      .android.setColor('#000000')
      .android.setVibrate(500)
      // .android.setLargeIcon('notification_icon_large')
    }
    
    // show notification
    firebase.notifications()
      .displayNotification(options)
      .then(x => {
          console.log('Notification Displayed')
       })
      .catch(err => {
        console.log(err)
      });
  }

  static listenToNotifications () {
    PushNotification.notificationListener = firebase.notifications()
      .onNotification(PushNotification.handleNotification);

    // PushNotification.notificationDisplayedListener = firebase.notifications()
    //   .onNotificationDisplayed(PushNotification.handleNotification);
 
  }

  static unlistenToNotifications () {
    PushNotification.notificationListener();
    // PushNotification.notificationDisplayedListener()
  }

  static register (callback) {
    if(Platform.OS == 'android') {
      firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          console.log(fcmToken)
          // user has a device token
          callback(fcmToken)
        } else {
          // user doesn't have a device token yet
          // TODO: handle this? Don't know when this happens!
        } 
      });
    } else if (Platform.OS == 'ios') {
      firebase.messaging().ios.registerForRemoteNotifications()
        .then(() => {
          firebase.messaging().getToken()
            .then(fcmToken => {
              if (fcmToken) {
                console.log(fcmToken)
                // user has a device token
                callback(fcmToken)
              } else {
                // user doesn't have a device token yet
                // TODO: handle this? Don't know when this happens!
              } 
            });

            firebase
              .messaging()
              .ios.getAPNSToken()
              .then(token => {
                console.log(
                  'APNS TOKEN AFTER REGISTRATION FOR BACKGROUND',
                  token,
                );
              })
        });
    }
  }

  static setup() {
    PushNotification.askPermission()
      .then(r => {
        console.log('Authorized')

        const channel = new firebase.notifications.Android.Channel(
          "oblip",
          "Oblip",
          firebase.notifications.Android.Importance.Max
        ).setDescription('Recieve transactions notifications');
        
        firebase.notifications().android.createChannel(channel);

      }).catch(e => {
        // user didn't authorize
        // TODO: show alert about the importance of this.
      })
  }
}
