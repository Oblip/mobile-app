const scrypt = require('scrypt-async');
const nacl = require('tweetnacl');
nacl.util = require('tweetnacl-util');

// Crypto helper functions
export default class Crypto {
  static scrypt = (text,encoding, salt) => {
    return new Promise((resolve, reject) => {
      try {
        /*
        * TODO: audit these parameters again :p 
        */
        const dkLen = 32
        const eSalt =  salt == undefined ? nacl.util.encodeBase64(nacl.randomBytes(32)) : salt
        const opslimit = 8
        const memlimit = 16;

        scrypt(text, eSalt, {
          r: opslimit,
          N: memlimit,
          dkLen: dkLen,
          encoding: encoding
        }, function(derivedKey) {
          resolve({ derivedKey: derivedKey, salt: eSalt })
        });
      }

      catch(e) {
        reject(e)
      }

    })
  } 

  static encryptSecretKey = (password, secretKey) => {
    return new Promise((resolve,reject) => {
      const nonce = new Uint8Array(24);
      Crypto.scrypt(password, 'binary')
        .then(output => {
          const encryptedSecretKey = nacl.util.encodeBase64(
            nacl.secretbox(nacl.util.decodeUTF8(secretKey), nonce, output.derivedKey)
          )
          resolve({encryptedSecretKey: encryptedSecretKey, salt: output.salt })
        }).catch(e => {
          reject(e)
        })
    }) 
  }

  static decryptSecretKey = (password, encryptedSecretKeyBundle) => {
    return new Promise((resolve, reject) => {
      const nonce = new Uint8Array(24)
      Crypto.scrypt(password, 'binary',encryptedSecretKeyBundle.salt)
        .then(output => {
          key = nacl.util.decodeBase64(encryptedSecretKeyBundle.encryptedSecretKey)
          const secretKey = nacl.util.encodeUTF8(nacl.secretbox.open(key,
            nonce, output.derivedKey))
          secretKey ? resolve(secretKey) : reject('Decryption failed!')
        }).catch(e => {
          reject(e)
        })
    })
  }

  static decryptAsymmetric(message, nonce, someones_public_key, our_private_key) {
    return new Promise((resolve, reject) => {
      let public_key = nacl.util.decodeBase64(someones_public_key)
      let private_key = nacl.util.decodeBase64(our_private_key)
      let decoded_message = nacl.util.decodeBase64(message)
      let decoded_nonce = nacl.util.decodeBase64(nonce)

      message = nacl.box.open(decoded_message, decoded_nonce, public_key, private_key)
      message = nacl.util.encodeUTF8(message)

      resolve(message)
    })
  }

  static generateSigKeypair = () => {
    let keypair = nacl.sign.keyPair()
    verifyKey = nacl.util.encodeBase64(keypair.publicKey)
    signingKey = nacl.util.encodeBase64(keypair.secretKey)
    
    return {
      publicKey: verifyKey,
      secretKey: signingKey
    }
  }

  static generatePublicAuthKeypair = () => {
    let keypair = nacl.box.keyPair()
    console.log(keypair)
    verifyKey = nacl.util.encodeBase64(keypair.publicKey)
    signingKey = nacl.util.encodeBase64(keypair.secretKey)
    
    return {
      publicKey: verifyKey,
      secretKey: signingKey
    }
  }

  static signData(data, signingKey) {
    keypair =  nacl.sign.keyPair.fromSecretKey(nacl.util.decodeBase64(signingKey))
    // message = new TextEncoder().encode(JSON.stringify(data))
    message = nacl.util.decodeUTF8(JSON.stringify(data))
   
    signature = nacl.sign.detached(message, keypair.secretKey)
    signature = nacl.util.encodeBase64(signature)
 
    return signature
  }

  static generateKeyPairFromSeed = (seed) => {
    return new Promise ((resolve, reject) => {
      salt = 'OBLIP_DEVICE_SALT_THAT_IS_NOT_A_SECRET_AT_ALL_OR_IS_IT'
      seed64 = null
      Crypto.scrypt(seed, 'binary', salt)
        .then(output => {
          seed64 = output.derivedKey
          let keypair = nacl.sign.keyPair.fromSeed(seed64)
          resolve(keypair)
        }).catch(e => {
          reject(e)
        })
    })
  }

}
