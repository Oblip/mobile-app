import { AsyncStorage } from "react-native"
import Config from 'react-native-config';

export default class DataStorage {
  static async setOblipClientID(id) {
    try {
      const jsonItem = await AsyncStorage.setItem(`${Config.STORAGE_KEYWORD}_ClientID`, id)
      return jsonItem
    } catch (error) {
      throw new Error(error.message)
    }
   return
  }

  static async getOblipClientID (){
    const clientID = await AsyncStorage.getItem(`${Config.STORAGE_KEYWORD}_ClientID`)
    
    if(clientID == null) {
      throw new Error('Client ID is not set.')
    }

    return clientID
  }

  static async saveIdentities(list) {
    try {
      const jsonItem = await AsyncStorage.setItem(`${Config.STORAGE_KEYWORD}_7k37aIGwLU3x76B/t9hsSD0`,
        JSON.stringify(list))
      return jsonItem
    } catch (error) {
      throw new Error(error.message)
    }
  }

  static async removeIdentities() {
    try {
      const jsonItem = await AsyncStorage.removeItem(`${Config.STORAGE_KEYWORD}_7k37aIGwLU3x76B/t9hsSD0`)
      return jsonItem
    } catch (error) {
      throw new Error(error.message)
    }
  }



  static async getIdentities (){
    const directory = await AsyncStorage.getItem(`${Config.STORAGE_KEYWORD}_7k37aIGwLU3x76B/t9hsSD0`)
    
    if(directory == null) {
      throw new Error('People Directory does not exist.')
    }

    return JSON.parse(directory);
  }

  // static async setAuthAccount (authData) {
  //   try {
  //     const jsonItem = await AsyncStorage.setItem(`${Config.STORAGE_KEYWORD}_AuthAccount`, JSON.stringify(authData))
  //     return jsonItem
  //   } catch (error) {
  //     throw new Error(error.message)
  //   }
  // }

  // static async getAccountData(){
  //   try {
  //     const data = await AsyncStorage.getItem(`${Config.STORAGE_KEYWORD}_AuthAccount`)
  //     return JSON.parse(data)
  //   } catch (error) {
  //     throw new Error(error.message)
  //   }
  // }

}
