#!/usr/bin/env bash

if [ "$APPCENTER_BRANCH" == "staging" ] || [ "$1" == "staging" ];
then
  echo "Switching to Firebase Staging environment"
  cp -rf firebase_environments/staging/google-services.json android
  echo "copied to android dir"
  cp -rf firebase_environments/staging/google-services.json android/app
  echo "copied to android/app dir"
  cp -rf firebase_environments/staging/GoogleService-Info.plist ios
  echo "copied to ios dir"
  echo "Switching to Firebase Staging environment - DONE"
else
  echo "Switching to Firebase Dev environment"
  cp -rf firebase_environments/development/google-services.json android
  echo "copied to android dir"
  cp -rf firebase_environments/development/google-services.json android/app
  echo "copied to android/app dir"
  cp -rf firebase_environments/development/GoogleService-Info.plist ios
  echo "copied to ios dir"
  echo "Switching to Firebase Dev environment - DONE"
fi